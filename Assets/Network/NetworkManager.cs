﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class NetworkManager
{
    public bool Running { get; private set; }

    Authentication _auth = new Authentication();
    public TownHandler BuildingHandler = new TownHandler(); 
    public BattleHandler BattleHandler = new BattleHandler();
	public RaidHandler RaidHandler = new RaidHandler();
	public AdminHandler adminHandler = new AdminHandler ();
	public SettingsHandler settingsHandler = new SettingsHandler ();

    static byte _fragmentedChannelID;

    public NetworkManager(int serverPort)
    {
        ConnectionConfig config = new ConnectionConfig();
        config.AddChannel(QosType.Reliable);
        config.AddChannel(QosType.Unreliable);
        _fragmentedChannelID = config.AddChannel(QosType.ReliableFragmented);
        HostTopology topology = new HostTopology(config, 10000);
        NetworkServer.Configure(topology);

        NetworkServer.Listen(serverPort);
        RegisterHandlers();
        Running = true;
    }   

    void RegisterHandlers()
    {    
        // Client Connection
        NetworkServer.RegisterHandler(MsgType.Connect, OnClientConnected);
        NetworkServer.RegisterHandler(MsgType.Disconnect, OnClientDisconnected);

        // Authentication
        NetworkServer.RegisterHandler((short)RequestCode.Login, _auth.Login);
        NetworkServer.RegisterHandler((short)RequestCode.Register,  _auth.Register);    

    
        // Town Handlers
        var codes = (RequestCode[])System.Enum.GetValues(typeof(RequestCode));
        foreach (var code in codes)
        {
            short c = (short)code;
            if( c >= 1100 && c < 1400)  
                NetworkServer.RegisterHandler(c, BuildingHandler.RequestRecieved);    
            if(c >= 1400 && c < 1500 )
				NetworkServer.RegisterHandler(c, BattleHandler.RequestRecieved);
			if(c >= 1500 && c < 1600 )
				NetworkServer.RegisterHandler(c, RaidHandler.RequestRecieved);
			if(c >= 1600 && c < 1610)
				NetworkServer.RegisterHandler(c, adminHandler.RequestRecieved);
			if(c >= 1700 && c < 1710)
				NetworkServer.RegisterHandler(c, settingsHandler.RequestRecieved);
            
        }
    }

    void OnClientConnected(NetworkMessage netMsg)
    {
		SimpleMessage msg = new SimpleMessage();
		msg.Add(ParameterCode.LastAssetUpdate, (PlayerPrefs.HasKey("LastAssetUpdate")) ? PlayerPrefs.GetString("LastAssetUpdate") : "");
		
		SendMessage(netMsg.conn, MsgType.Connect, msg);
        Debug.Log("Client connected from " + netMsg.conn.address);
    }

    void OnClientDisconnected(NetworkMessage netMsg)
    {
        ServerLogic.Instance.DisconnectPlayer(netMsg.conn.connectionId);
    }



    // Send Response
    public static void SendMessage(Player player, RequestCode code, MessageBase message)
    {
        var client = ServerLogic.Instance.ConnectedPlayers.GetConnectionInfo(player.ConnectionID);
        SendMessage(client, (short)code, message);
    }
    public static void SendMessage(NetworkConnection client, RequestCode code, MessageBase message)
    {
        SendMessage(client, (short)code, message);
    }


    // Send Event
    public static void SendMessage(Player player, EventCode code, MessageBase message)
    {
        var client = ServerLogic.Instance.ConnectedPlayers.GetConnectionInfo(player.ConnectionID);
        SendMessage(client, (short)code, message);
    }
    public static void SendMessage(NetworkConnection client, EventCode code, MessageBase message)
    {
        SendMessage(client, (short)code, message);
    }



    static void SendMessage(NetworkConnection client, short code, MessageBase message)
    {
        string log = "Sending Message(" + code + ") " + message.GetType();
        if (message is IBigMessage  && ((IBigMessage)message).Size(ref log) > 1400)
            client.SendByChannel(code, message, _fragmentedChannelID);              // MAX: 63900 bytes
        else
            client.Send(code, message);
    }


    public static void SendErrorMessage(Player player, ErrorMessage m = null)
    {
        var client = ServerLogic.Instance.ConnectedPlayers.GetConnectionInfo(player.ConnectionID);
        SendMessage(client, (short)EventCode.Error, m);
    }
    public static void SendErrorMessage(NetworkConnection client, ErrorMessage m = null )
    {
        if (m == null)
            m = new ErrorMessage(){ ErrorCode = ErrorCode.UnknownError };
        
       client.Send((short)EventCode.Error, m);
    }

    void OnApplicationQuit()
    {
        NetworkServer.Shutdown();
    }

}