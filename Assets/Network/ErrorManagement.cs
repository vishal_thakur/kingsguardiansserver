﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;


public class ErrorManagement
{
    public static void Send(NetworkConnection client, ErrorCode code, RequestCode request, string contentID, string instanceID = "" )
    {
        ErrorMessage error = new ErrorMessage()
            { 
                ErrorCode = code,
                Message = "",
                requestCode = request,
                ContentID = contentID,
                InstanceID = instanceID,
            };

        NetworkManager.SendErrorMessage(client, error);

        Debug.LogWarningFormat("ERROR [{0}]: {1} - Client: {2}\n RequestCode: {3}\n ContentID: {4}\n InstanceID: {5}", 
            (short)code, code.ToString(), client.hostId, request.ToString(), contentID, instanceID);
    }

    public static void Send(Player player, ErrorCode code, RequestCode request, string contentID, string instanceID = "" )
    {
        ErrorMessage error = new ErrorMessage()
            { 
                ErrorCode = code,
                Message = "",
                requestCode = request,
                ContentID = contentID,
                InstanceID = instanceID,
            };

        NetworkManager.SendErrorMessage(player, error);

        Debug.LogWarningFormat("Implemented ERROR: \nPlayer: {0} \n Error Details ({2}): {3} ", 
            player.Username, (short)code, code.ToString());
    }
   
}
