﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsHandler : BaseHandler {

	List<Player> AllPlayers = new List<Player>();


	protected override void OnRequestRecieved(Message message)
	{
		
		AllPlayers = ServerLogic.Instance.PlayerDatabase.GetPlayers();
		ErrorInfo error = null;

		//Grab the simple Message from the recieved Client Message
		SimpleMessage simpleMessage = message.GetMessage<SimpleMessage> ();

		bool flag = simpleMessage.GetValue (ParameterCode.StringValue).ToLower() == "true" ? true : false;


		switch (flag)
		{
		case true: 
			OnPushNotificationsToggled (message.Player , true);
			break;      

		case false: 
			OnPushNotificationsToggled (message.Player , false);
			break;      
		}

		if(error != null)
			ErrorManagement.Send(message.Client, error.Code, message.Code, error.ContentID, error.ItemInstanceID );
	}





	public void OnPushNotificationsToggled(Player player, bool flag){
		//Toggle PushNotif Flag
		player.IsPushNotificationsEnabled = flag;

		Debug.LogError ("Push Notif Toggle Successfull\t" + player.Username + "PushNotifStatus:\t" + flag);

		var msg = new AdminControlsMessage();

		//Send Confirmation to Client
		NetworkManager.SendMessage(player, EventCode.PushNotificationsToggled , msg);  

		//Save TO DB
		ServerLogic.Instance.PlayerDatabase.SavePlayer (player.Username);
	}
}
