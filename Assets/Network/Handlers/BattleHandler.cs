﻿using UnityEngine;
using System.Collections;

public class BattleHandler : BaseHandler
{
    protected override void OnRequestRecieved(Message message)
    {
        ErrorInfo error = null;
        switch (message.Code)
        {
		case RequestCode.StartBattle:
			error = BattleLogic.Start(message.Player, message.GetMessage<SimpleMessage>());
                break;
            case RequestCode.TurnRequest:
                error = BattleLogic.OnPlayerRequest(message.Player, message.GetMessage<SimpleMessage>());
                break;

            case RequestCode.ForceWin:
                error = BattleLogic.OnPlayerForceWin(message.Player);
                break;
        }

        if(error != null)
            ErrorManagement.Send(message.Client, error.Code, message.Code, error.ContentID, error.ItemInstanceID );
    }
	
}
