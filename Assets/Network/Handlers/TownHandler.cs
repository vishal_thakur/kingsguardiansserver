﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TownHandler : BaseHandler
{



    protected override void OnRequestRecieved(Message message)
    {
        ErrorInfo error = null;

//		Debug.LogError (message.Code.ToString());
        switch (message.Code)
        {
			case RequestCode.ConstructWall: 
				error = TownLogic.Construct(message.Player, message.GetMessage<WallConstructRequest>());
				break;

            case RequestCode.Construct: 
                error = TownLogic.Construct(message.Player, message.GetMessage<ConstructRequest>());
                break;

			//When We move Wall to a new position
			case RequestCode.ReplaceWall: 
				error = TownLogic.Move(message.Player, message.GetMessage<WallConstructRequest>());
				break;

			//When we move any unit (Except wall) to a new position
            case RequestCode.Replace:
                error = TownLogic.Move(message.Player, message.GetMessage<ConstructRequest>());
                break;

            case RequestCode.Upgrade:
                error = TownLogic.Upgrade(message.Player, message.GetMessage<SimpleMessage>());
                break;

            case RequestCode.Repair: 
                error = TownLogic.Repair(message.Player, message.GetMessage<SimpleMessage>());
                break;

            case RequestCode.Mine: 
                error = TownLogic.Mine(message.Player, message.GetMessage<SimpleMessage>());
                break;

            case RequestCode.Craft: 
                error = TownLogic.Craft(message.Player, message.GetMessage<SimpleMessage>());
                break;

			case RequestCode.Collect:
				error = TownLogic.Collect(message.Player, message.GetMessage<SimpleMessage>());
				break;

            case RequestCode.CancelBuilding:
            case RequestCode.CancelWorker:    
                error = TownLogic.Cancel(message.Player, message.GetMessage<SimpleMessage>(), message.Code);
                break;


            case RequestCode.CreateWorker:
            case RequestCode.CreateWarlock:
                error = TownLogic.CreateWorker(message.Player, message.Code);
                break;

			case RequestCode.AttachWarlock:
				error = TownLogic.AttachWarlock(message.Player, message.GetMessage<SimpleMessage>());
				break;

			case RequestCode.DetachWarlock:
				error = TownLogic.DetachWarlock(message.Player, message.GetMessage<SimpleMessage>());
				break;

			case RequestCode.GetOpponents:
				error = TownLogic.GetOpponents(message.Player);
				break;

			case RequestCode.GetOtherPlayer:
				error = TownLogic.GetOtherPlayer(message.Player, message.GetMessage<SimpleMessage>());
				break;

			case RequestCode.ChangeSquad:
				error = TownLogic.ChangeSquad(message.Player, message.GetMessage<SimpleMessage>());
				break;

			case RequestCode.UpgradeCharacter :
				error = TownLogic.UpgradeCharacter(message.Player, message.GetMessage<SimpleMessage>());
				break;

			case RequestCode.Equip: 
			case RequestCode.Unequip:                 
				error = TownLogic.EquipItem(message.Player, message.GetMessage<SimpleMessage>(), message.Code);
				break;      
        }

        if(error != null)
            ErrorManagement.Send(message.Client, error.Code, message.Code, error.ContentID, error.ItemInstanceID );
    }



	public static void RefreshMaterialEvent(Player player)
	{            
		NetworkManager.SendMessage(player, EventCode.RefreshMaterial, new MaterialUpdateMessage()
			{
				Materials = player.Materials.Save()
			});       
	}


    public static void WorkerJobDone(Player player, Building building, Worker worker)
	{
		Debug.LogError ("Worker Job done for player\t" + player.Username + "\t and building = \t" + building.ItemCategory.ToString());
        NetworkManager.SendMessage(player, EventCode.WorkerJobDone, new BuildingResponse()
            {
                Building = building.ToClient<BuildingInfo>(),
                Worker = worker.ToClient<WorkerInfo>()
			});            
		Debug.LogError ("Worker Job done for player\t" + player.Username + "\t and building = \t" + building.ItemCategory.ToString());
    }

    public static void MineDone(Player player, Building building)
    {
        NetworkManager.SendMessage(player, EventCode.MineProgressDone, new BuildingResponse()
            {
                Building = building.ToClient<BuildingInfo>(),
                Materials = player.MaterialsToClient()
			});            
		Debug.LogError ("Mine Done for player\t" + player.Username + "\t and building = \t" + building.ItemCategory.ToString());
    }

    public static void CraftDone(Player player, Building building)
    {
        var m = new PlayerMessage();
        m.Buildings = new List<BuildingInfo>(){ building.ToClient<BuildingInfo>() };

        if (building.Category == BuildingCategory.CharacterCreator)
            m.Characters = player.CharactersToClient();
        else
            m.Inventory = player.InventoryToClient();

        NetworkManager.SendMessage(player, EventCode.CraftProgressDone, m);   
		Debug.LogError ("Craft Done for player\t" + player.Username + "\t and building = \t" + building.ItemCategory.ToString());
    }
 
}
