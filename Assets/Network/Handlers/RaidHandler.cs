﻿using UnityEngine;
using System.Collections;

public class RaidHandler : BaseHandler
{
    protected override void OnRequestRecieved(Message message)
    {
        ErrorInfo error = null;

        switch (message.Code)
        {
            case RequestCode.StartRaid:
                error = RaidLogic.Start(message.Player, message.GetMessage<SimpleMessage>());
                break;
            case RequestCode.SpawnCharacter:
               error = RaidLogic.SpawnCharacter(message.Player, message.GetMessage<SimpleMessage>());
               break;
            case RequestCode.TargetBuilding:
                error = RaidLogic.TargetBuilding(message.Player, message.GetMessage<SimpleMessage>());
                break;

            case RequestCode.GetAllRewards:
                error = RaidLogic.GetAllRewards(message.Player);
                break;

            case RequestCode.ActivateRaidAbility:
                error = RaidLogic.ActivateRaidAbility(message.Player, message.GetMessage<SimpleMessage>());
                break;

			case RequestCode.DestinationReached:
				error = RaidLogic.OnDestinationReached(message.Player, message.GetMessage<SimpleMessage>());
				break;
        }

        if(error != null)
            ErrorManagement.Send(message.Client, error.Code, message.Code, error.ContentID, error.ItemInstanceID );
    }
	
}
