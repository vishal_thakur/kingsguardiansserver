﻿using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;
using System.Collections.Generic;

public class Authentication 
{

    public void Login(NetworkMessage netMsg)
    {
		Debug.LogWarning("Login request" + Time.time);

        var client = netMsg.conn;
        var msg = netMsg.ReadMessage<LoginRequest>();

		string[] usernameStrings = msg.ID.Split ('.');

		var userName = usernameStrings [0];

		var firebaseToken = usernameStrings [1];

        Player player = ( msg.method == LoginMethod.Username ) ? 
			ServerLogic.Instance.PlayerDatabase.GetPlayer(userName, msg.Password) : 
			ServerLogic.Instance.PlayerDatabase.GetPlayer(userName);

		//If Player Is Banned
		if (player.IsBanned) {
			ErrorManagement.Send(client, ErrorCode.PlayerBanned, (RequestCode)netMsg.msgType, "", ""); 
			return;
		}
		//Successful Login
        if (player != null)
        {
            ServerLogic.Instance.ConnectedPlayers.PlayerConnected(client, player);


            // Send Content Database event to the client
            NetworkManager.SendMessage(client, EventCode.GameContent, CollectGameContent(1));
            NetworkManager.SendMessage(client, EventCode.GameContent, CollectGameContent(2));
            NetworkManager.SendMessage(client, EventCode.GameContent, CollectGameContent(3));


			//Set Freshly Recieved GCM Token and save it
			player.GCMFirebaseToken = firebaseToken;
			player.Save ();

            // Send Response
			var m = new PlayerMessage()
				{
					Account = player.AccountToClient(),
					Buildings = player.BuildingsToClient(),
					Characters = player.CharactersToClient(),
					Inventory = player.InventoryToClient(),
					Materials = player.MaterialsToClient(),
					Squad = player.SquadToClient(),
					Workers = player.WorkersToClient(),
					Warlocks = player.Warlocks
				};
            NetworkManager.SendMessage(client, RequestCode.Login, m);      
        }
		//Invalid UserName/Password
        else
            ErrorManagement.Send(client, ErrorCode.InvalidUsernameOrPassword, (RequestCode)netMsg.msgType, "", ""); 
    }

    public void Register(NetworkMessage netMsg)
    {
        var client = netMsg.conn;
        var msg = netMsg.ReadMessage<LoginRequest>();


		//Error! Empty/Null
        if (msg.ID == "")
        {
            ErrorManagement.Send(client, ErrorCode.RegistrationError, (RequestCode)netMsg.msgType, "", "");
            return;
        }

		//Error! Invalid User Name / Password
        if (!ValidateRegistration(msg.ID, msg.method))
        {
            ErrorManagement.Send(client, ErrorCode.InvalidUsernameOrPassword, (RequestCode)netMsg.msgType, "", "");
            return;
        }

		//All Good ! Register player in DB
        Player p = new Player();

		//Save GCM Token For Push notif
		p.GCMFirebaseToken = msg.GCMFirebaseToken;
		Debug.LogError ("GCM Token = \t" + msg.GCMFirebaseToken);

        p.SetUsername("" + Random.Range(0, 10000000));
        switch (msg.method)
        {
            case LoginMethod.Username:
                p.SetUsername(msg.ID);
                p.Password = msg.Password;
                break;
            case LoginMethod.DeviceID:
                p.DeviceID = msg.ID;
                break;
            case LoginMethod.FacebookToken:
                p.FacebookToken = msg.ID;
                break;
            case LoginMethod.GoogleToken:
                p.GoogleToken = msg.ID;
                break;
        }

        foreach( var m in Settings.STARTING_MATERIALS)
            p.Materials.Add(m.Key, m.Value, true);
 
        ServerLogic.Instance.PlayerDatabase.CreateNewPlayer(p);
        NetworkManager.SendMessage(client, RequestCode.Register, new EmptyMessage());
    }

    bool ValidateRegistration(string id, LoginMethod method)
    {
        var player = ServerLogic.Instance.PlayerDatabase.GetPlayer(id, method);
        return player == null;
    }




    /// <summary>
    /// Collects the content of the game.
    /// Part parameter is not givem means collect all.
    /// </summary>
    /// <returns>The game content.</returns>
    /// <param name="part">Part.</param>
    GameContentMessage CollectGameContent(int part = 0)
    {
        GameContentMessage m = new GameContentMessage();
        var content = ServerLogic.Instance.GameContent;

        if (part == 0 || part == 1)
        {
            m.Characters = new List<CharacterContent>();
            foreach (var c in content.Characters)
                m.Characters.Add(c.Value);

            m.Items = new List<ItemContent>();
            foreach (var i in content.Items)
                m.Items.Add(i.Value);
        }
        if (part == 0 || part == 2)
        {
            m.Weapons = new List<ItemContent>();
            foreach (var i in content.Weapons)
                m.Weapons.Add(i.Value);
        }

        if (part == 0 || part == 3)
        {
            m.Buildings = new List<BuildingContent>();
            foreach (var b in content.Buildings)
                m.Buildings.Add(b.Value);         
        }

		m.TileSize = GridSystem.GridHandler.Instance.TileSize;
        return m;
    }
}
