﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdminHandler : BaseHandler {

	List<Player> AllPlayers = new List<Player>();


	protected override void OnRequestRecieved(Message message)
	{
		//Grab Player list Everytime as Players can be updated
//		AllPlayers.Clear();
		AllPlayers = ServerLogic.Instance.PlayerDatabase.GetPlayers();
		ErrorInfo error = null;
		//Grab the simple Message from the recieved Client Message
		SimpleMessage simpleMessage = message.GetMessage<SimpleMessage> ();
//		Debug.LogError ("Aa fad pyo apna\t\t\t" + simpleMessage.GetValue (ParameterCode.Username));
//		Debug.LogError ("Admin vala" + message.NetMsg.ToString());
		switch (message.Code)
		{
		case RequestCode.BanPlayer: 
			//Set The Specified Player As banned and update the same in DB
			OnPlayerBanToggleRequest (message.Player , simpleMessage.GetValue (ParameterCode.Username) , true);
			break;      

		case RequestCode.UnbanPlayer: 
			OnPlayerBanToggleRequest (message.Player ,simpleMessage.GetValue (ParameterCode.Username), false);
			break;      

		case RequestCode.RewardPlayer: 
			OnPlayerRewarded (message.Player ,simpleMessage.GetValue(ParameterCode.StringValue));
			break;      
		}

		if(error != null)
			ErrorManagement.Send(message.Client, error.Code, message.Code, error.ContentID, error.ItemInstanceID );
	}





	public void OnPlayerBanToggleRequest(Player adminPlayer, string playerName, bool banflag){
		for(int i=0;i<AllPlayers.Count;i++){
			//Find the player to be banned/Unbanned amongst all players
			if (AllPlayers [i].Username== playerName)
				AllPlayers [i].IsBanned = banflag;
		}
//		Debug.LogError ("Player Ban Toggle Successfull\t" + player.Username + "Ban Status" + banflag);

		var msg = new AdminControlsMessage();
//		player.IsBanned = banflag;

		if(banflag)
			NetworkManager.SendMessage(adminPlayer, EventCode.PlayerBanned , msg);  
		else
			NetworkManager.SendMessage(adminPlayer, EventCode.PlayerUnBanned, msg);  

		ServerLogic.Instance.PlayerDatabase.SavePlayer (playerName);
	}

	public void OnPlayerRewarded(Player adminPlayer , string message){
		string[] allStrings= message.Split('.');

		string rewardedPlayerName = allStrings [0];
		Currency rewardCurrency = StringToCurrency(allStrings [1]);
		int rewardAmmount = int.Parse(allStrings [2]);


		//Grab Rewarded Player
		Player rewardedPlayer = ServerLogic.Instance.PlayerDatabase.GetPlayer(rewardedPlayerName);  


		//Add Reward Material
		rewardedPlayer.Materials.Add (rewardCurrency , rewardAmmount);
		//Save its Progress TO DB
		rewardedPlayer.Save ();

		var msg = new AdminControlsMessage();

		//Message To The Reward Giver
		NetworkManager.SendMessage(adminPlayer, EventCode.PlayerRewarded, msg);  

		FirebaseNotificationManager.Instance.SendPushNotification (rewardedPlayer , "Hey " + rewardedPlayer.Username , "You recieved " + rewardAmmount + " " + rewardCurrency + "from " + adminPlayer.Username);
//		Debug.LogError ("Admin PLayer :\t" + adminPlayer.Username + "\tCurrency :\t" + rewardCurrency  + "\tAmmount:\t" + rewardAmmount + "\tTo : " + rewardedPlayerName);
	}


	/// <summary>
	/// Strings to currency.
	/// </summary>
	/// <returns>The to currency.</returns>
	/// <param name="stringValue">String value.</param>
	Currency StringToCurrency(string stringValue){
		for(int i=0;i<(int)Currency.Ether;i++){
			if (stringValue.ToLower () == ((Currency)i).ToString ().ToLower ())
				return (Currency)i;
		}
		return Currency.Wood;
	}
}
