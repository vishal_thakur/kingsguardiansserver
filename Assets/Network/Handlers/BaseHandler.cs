﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BaseHandler
{
    public void RequestRecieved(NetworkMessage netMsg)
    {
        OnRequestRecieved(new Message(netMsg));
    }

    protected virtual void OnRequestRecieved(Message message)
    {
        
    }
}

public class ErrorInfo
{
    public ErrorCode Code;
    public string ItemInstanceID;
    public string ContentID;
	public string Message;
}

public class Message
{
    public readonly NetworkMessage NetMsg;

    public NetworkConnection Client { get; private set; }
    public Player Player { get; private set; }
    public RequestCode Code { get; private set; }

    public Message(NetworkMessage netMsg)
    {
        NetMsg = netMsg;
        Client = netMsg.conn;
        Code = (RequestCode)netMsg.msgType;
        Player = ServerLogic.Instance.ConnectedPlayers.GetPlayer(Client.connectionId);
    }

    public T GetMessage<T>() where T : MessageBase, new() { return NetMsg.ReadMessage<T>(); } 
}
