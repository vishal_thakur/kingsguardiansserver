﻿using UnityEngine;
using UnityEngine.Networking;
using System;

public class CharacterUpgradeMessage : MessageBase {

    /// <summary>
    /// The building ID.
    /// </summary>
    public string CharacterID;

    /// <summary>
    /// The stat.
    /// </summary>
    public CharacterStats Stat;

    public override void Deserialize(NetworkReader reader)
    {
        CharacterID = reader.ReadString(); 
        Stat = (CharacterStats)Enum.Parse(typeof(CharacterStats), reader.ReadString());
    }

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(CharacterID);
        writer.Write(Stat.ToString());
    }
}
