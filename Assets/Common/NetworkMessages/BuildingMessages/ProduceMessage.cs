﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class ProduceMessage : StringMessage
{
    /// <summary>
    /// The item content ID.This item will be crafted.[optional]
    /// </summary>
    public string ItemContentID;

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);
        ItemContentID = reader.ReadString();
    }

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        writer.Write(ItemContentID);
    }
}
