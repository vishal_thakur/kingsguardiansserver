﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CharacterInfo : BaseInfo
{
    public int MaxHP;
    public int CurrentHP;
    public float Rage; 
    public int Shield;

    public int Exp;
    public int ExpStart;
    public int ExpEnd;
    public int Level;

    public List<SCharacterStat> Stats;

    public WeaponInfo Weapon;

    public int MaxDefences;
    public List<DefenceInfo> Defences;

    public int MaxItems;
    public List<CombatCardInfo> Inventory;
}
