﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class RaidUnitClientInfo
{
	public string InstanceID;
	public RaidAction Action = RaidAction.Idle;

    public int Health;
    public int Shield;
    public string position;
    public string lookatPos;

    public Vector3 Position 
    {
        get { return Vector3Extended.Parse(position);}
        set { position = value.ToString(); }
    }

    public Vector3 LookatPos 
    {
        get { return Vector3Extended.Parse(lookatPos);}
        set { lookatPos = value.ToString(); }
    }

}