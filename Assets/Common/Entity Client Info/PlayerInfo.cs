﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerInfo
{
    public string Username;
    public string DeviceID;
    public string FacebookToken;
    public string GoogleToken;
	public string GCMFirebaseToken;

	public bool IsBanned;

	public bool IsPushNotificationsEnabled;

    public string AvatarID;
    public int Exp;
    public int ExpStart;
    public int ExpEnd;
    public int Level;

    public int MaxBuildings;
    public int MaxItems;

    // Optional parameters, used only for player lists.
    public List<SquadInfo> Squad;

	public List<RemainsInfo> Remains = new List<RemainsInfo>();

    public override string ToString()
    {
        return Username + " (Level: " + Level + " (" + Exp + " exp)";
    }

    public string GridTileSize = "1,1";

}

[System.Serializable]
public class SquadInfo
{
    public string InstanceID = "";
    public string ContentID = "";
    public int Level = 0;
    public int MaxHP = 0;
    public int HP = 0;

    public float Readiness;
}