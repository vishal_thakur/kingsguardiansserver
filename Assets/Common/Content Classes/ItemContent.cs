﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ItemContent : GameItemContent
{
    public ItemCategory Category;

	/// <summary>
	/// Maximum stack of the item in the character inventory.
	/// </summary>
    public int MaxStacks;  

	/// <summary>
	/// This building level needs to be able to craft.
	/// </summary>
    public int BuildingLevelReq;
}
