﻿public enum Currency 
{
    Wood = 0,
    Stone,
    Iron, 
    BloodCrystal,
    Gold,
    Diamond,
    Ether,   
}

public enum WorkerType { Worker, Warlock }