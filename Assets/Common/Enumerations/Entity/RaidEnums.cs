﻿public enum UnitType 
{ 
	None,
	Character,
	Building, 
	BreedingPit 
}

public enum TraitType
{
	DamageIncrease,
	DamageResistance,
	Miss,
	Explosion,
	AlternateDamage,
	WallJump,
	StatIncrease,
	Necromancy,

	DOT,
	Stun,
}

public enum AuraType
{
	HPAdjust,
	TraitGranting,
	Combined,
}

public enum AttackResult
{
	Hit,
	Miss,
}