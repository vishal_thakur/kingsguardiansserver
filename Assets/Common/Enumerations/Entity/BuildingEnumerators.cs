﻿public enum BuildingCategory
{
    Resource_mine,
    Resource_storageOnly,
    Resource_generator,

    CardCreator,
    CharacterCreator,

    WorkerCreator,          // Not implemented
    Defense,                // Not implemented
}

public enum Rotation : int { D0 = 0, D90 = 1, D180 = 2, D270 = 3 }