﻿
public enum ItemCategory : int 
{
    Weapon = 0,
    Defence,
    Magic,
    Elemental,
    MixedElemental
}
public enum ActionID : int
{
    None = 0,
    BasicAttack = 1,
    Buff = 2,
    Offensive = 3,

    MoveForward = 4,
    MoveBackward = 5,	
	Jump = 6,
}

public enum TargetType
{
    Friend,
    Foe,
	Self,
}

public enum Element : int
{
    All,
    Water,
    Wind,
    Electric,
    Stone,
    Ice,
    Fire,
    Untyped,
}

