﻿
public enum CharacterStats : int
{
    Health,
    Attack,
    MagicAttack,
    Defence,
    Speed,
}
