﻿public enum LoginMethod 
{ 
    Username,
    DeviceID, 
    FacebookToken, 
    GoogleToken 
}