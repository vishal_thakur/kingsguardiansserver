﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class BattleMessage : MessageBase, IBigMessage
{
    public string NextPlayerCharacter = "";
    public List<CommandResult> CommandResults = new List<CommandResult>();
    public List<TurnOrder> TurnOrder = new List<TurnOrder>();
    public BattleEndMessage BattleEnd = null;

    public int Size(ref string log)
    {
        string s = "";
        int size = Utils.Add(out s, 
            SerializationBinary.CalculateSize(CommandResults),
            SerializationBinary.CalculateSize(TurnOrder), 
            SerializationBinary.CalculateSize(BattleEnd));
        log += s;
        return size;
    }      

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);  
        writer.Write(NextPlayerCharacter);
        writer.WriteBytesFull(SerializationBinary.Serialize(CommandResults));
        writer.WriteBytesFull(SerializationBinary.Serialize(TurnOrder));
        writer.WriteBytesFull(SerializationBinary.Serialize(BattleEnd));
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);
        NextPlayerCharacter = reader.ReadString();
        CommandResults = SerializationBinary.Deserialize<List<CommandResult>>(reader.ReadBytesAndSize());
        TurnOrder = SerializationBinary.Deserialize<List<TurnOrder>>(reader.ReadBytesAndSize());
        BattleEnd = SerializationBinary.Deserialize<BattleEndMessage>(reader.ReadBytesAndSize());
    }
}


[System.Serializable]
public class ActionLog
{
    public string CharacterID;
    public string CardID;
    public string TargetID;
}

[System.Serializable]
public class CharacterBuffs
{
    public string CharacterID;
    public string ID;
    public int Rounds;
}

[System.Serializable]
public class CommandResult
{ 
    // From Battle Manager
    public List<TurnOrder> TurnOrder = new List<TurnOrder>();
    public List<ActionLog> Log = new List<ActionLog>();
    public List<CharacterBuffs> Buffs = new List<CharacterBuffs>();

    // From AI
    public string Caster = "";
    public int TargetHP = -1;  
    public int TargetShield = 0;
   
    // If player character
    public List<SCharacterStat> Stats = new List<SCharacterStat>();

    // From AI Action
    public string UsedCardContentID = "";
    public string Target;

    // From Card
    public string Message = "";
    public CommandMessageColor MessageType;   
}
[System.Serializable]
public class TurnOrder
{
    public string CharacterID;
    public float Readiness;
}

public enum CommandMessageColor : int
{
    White,
    Red,
    Green,
    Yellow,
}

[System.Serializable]
public class BattleEndMessage
{
    public bool Win;
    public PlayerInfo Account = null;
    public List<SMaterial> Materials = new List<SMaterial>();
    public List<CharacterInfo> Characters = new List<CharacterInfo>();       

}