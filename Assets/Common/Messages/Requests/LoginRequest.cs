﻿using UnityEngine.Networking;

public class LoginRequest : MessageBase
{
    public LoginMethod method;

	//Can be Simple Username , FacebookToken , Google Token 
	public string ID;

	// Only used with Username method.
	public string Password; 

	//Is Sent In Each Case , Used for Push notifications
	public string GCMFirebaseToken;

    public override void Deserialize(NetworkReader reader)
    {
		ID = reader.ReadString();
		Password = reader.ReadString();
		GCMFirebaseToken = reader.ReadString();
    }

    public override void Serialize(NetworkWriter writer)
    {
		writer.Write(ID);
		writer.Write(Password);
		writer.Write(GCMFirebaseToken);
    }
}