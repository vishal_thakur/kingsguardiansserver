﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using GridSystem;
using System.Collections.Generic;

public class ConstructRequest : MessageBase
{
	public string InstanceID;
    public string ContentID;
    public Point Position;
    public Rotation Rotation;

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);
		InstanceID = reader.ReadString();
        ContentID = reader.ReadString();
        Position = Point.Parse(reader.ReadString());
        Rotation = (Rotation)reader.ReadInt32();
    }

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        writer.Write(InstanceID);
        writer.Write(ContentID);
        writer.Write(Position.ToString());
        writer.Write((int)Rotation);
    }
}
