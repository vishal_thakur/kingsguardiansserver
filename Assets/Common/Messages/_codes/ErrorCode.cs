﻿public enum ErrorCode : short 
{
    None = 3000,
    UnknownError = 3001,

    RegistrationError = 3002,
    InvalidUsernameOrPassword = 3003,
	PlayerBanned = 3004,

    MissingGameContent = 3099,

    NoFreeWorker = 3100,
    NotEnoughMaterial = 3101,
    ItemNotOwned = 3102,
    WrongItemType = 3103,

    AlreadyExists = 3104,
    LockedItem = 3105,

    NoFreeWarlock = 3106,

    BuildingLimit = 3107,

    // BATTLE
    AlreadyInBattle = 3400,
    EnemyDoesNotExists = 3401,
    PlayerNotInBattle = 3402,

    // RAID
    AlreadyInRaid = 3500,
    PlayerNotInRaid = 3501,
    CharacterIsDead = 3502,
    CharacterAlreadySpawned = 3503,
    CharacterNotSpawned = 3504,
    InvalidTarget = 3505,

	CantUseAbility = 3506,
	NotEnoughActionPoint = 3507,

}