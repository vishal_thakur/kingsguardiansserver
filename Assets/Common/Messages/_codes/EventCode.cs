﻿public enum EventCode
{
	Error = 2000,
    GameContent = 2001,
    RefreshMaterial = 2002,
	AssetLastUpdateInfo = 2003,

    WorkerJobDone = 2100,
    MineProgressDone = 2101,
    CraftProgressDone = 2102,

	// RAID
	UnitUpdate = 2501,			// Client update: pos, lookat, action
	Attack = 2502,				// TargetsID, Result, Damage, Damage Type
	DamageTaken = 2503,
	AbilityPointUpdate = 2504,
	TargetUnit = 2505,

    RaidFinished = 2510,
	RaidAbilityEvent = 2511,

	CustomRaidEvent = 2549,
	CustomCharacterSpawn = 2550,

	//Admin Controls
	PlayerBanned = 3001,
	PlayerUnBanned = 3002,

	PlayerRewarded = 3004,
		
	//Settings
	PushNotificationsToggled = 4001
		


}