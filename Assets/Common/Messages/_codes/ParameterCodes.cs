﻿public enum ParameterCode
{
	LastAssetUpdate,

    Username,
    ContentID,
    InstanceID,

    CharacterInstanceID,
    ItemInstanceID,

    CharacterStat,
    SquadSlot,

    TargetCharacterInstanceID,

    ItemSlot,
    Position,
	LookAtPosition,

    Health,
    Shield,
    Action,
    KamikazeAttack,

    RaidReward,
    BuildingCount,
    AbilityNumber,

	AbilityPoints,

	EventName,

	BoolValue,
	FloatValue,
	StringValue,
	IntValue,
}

public enum RaidAction { Idle, Move, Attack }