﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class MaterialUpdateMessage : MessageBase, IBigMessage
{
    public List<SMaterial> Materials = new List<SMaterial>();

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        writer.WriteBytesFull(SerializationBinary.Serialize(Materials));
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);   
        Materials = SerializationBinary.Deserialize<List<SMaterial>>(reader.ReadBytesAndSize());       
    }

    public int Size(ref string log)
    {
        int size = SerializationBinary.CalculateSize(Materials);
        log += " " + size;
        return size; 
    }

    public bool Check(int maxBytes)
    {
        int m = SerializationBinary.CalculateSize(Materials);
        Debug.Log("Check message size: " + m + " (max: " + maxBytes + ")");
        if (m > maxBytes)
            return true;
        return false;
    }
}
