﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class BuildingResponse : MessageBase, IBigMessage
{
    public string TemporaryInstanceID;
    public BuildingInfo Building;
    public List<SMaterial> Materials = new List<SMaterial>();
    public WorkerInfo Worker; 

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        writer.Write(TemporaryInstanceID);
        if (Building == null) Building = new BuildingInfo();       
        if (Worker == null) Worker = new WorkerInfo();

        writer.WriteBytesFull(SerializationBinary.Serialize(Building));
        writer.WriteBytesFull(SerializationBinary.Serialize(Worker));
        writer.WriteBytesFull(SerializationBinary.Serialize(Materials));
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);   
        TemporaryInstanceID = reader.ReadString();
        Building = SerializationBinary.Deserialize<BuildingInfo>(reader.ReadBytesAndSize());
        Worker = SerializationBinary.Deserialize<WorkerInfo>(reader.ReadBytesAndSize());
        Materials = SerializationBinary.Deserialize<List<SMaterial>>(reader.ReadBytesAndSize());       
    }


    public int Size(ref string log)
    {
        int b = SerializationBinary.CalculateSize(Building);
        int m = SerializationBinary.CalculateSize(Materials);
        int w = SerializationBinary.CalculateSize(Worker);

        string s = "";
        int size = Utils.Add(out s, b, m, w);

        log += s;
        return size; 
    }
}
