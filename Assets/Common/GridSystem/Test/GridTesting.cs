﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GridSystem;

public class GridTesting : SingletonMono<GridTesting>
{
    public GameObject TilePrefab;
    public GameObject ObstaclePrefab;

    Dictionary<Point, GameObject> Obstacles = new Dictionary<Point, GameObject>();
    Dictionary<Point, GameObject> Tiles = new Dictionary<Point, GameObject>();

    Point _startPoint;
    GameObject _startTile;

    Point _goalPoint;
    GameObject _goalTile;

    public void DrawPath()
    {
        if (_startTile == null || _goalTile == null)
            return;

        Pathfinder pathfinding = new Pathfinder();
        pathfinding.Calculate(_startPoint, _goalPoint);
        if (pathfinding.HasPath)
        {
            foreach (var tile  in pathfinding.GetPath())
                CreateTile(tile);
        }
        else
            Debug.Log("There is no path the the goal point");
    }

    void Update()
    {
        bool left = Input.GetMouseButtonUp(0);
        bool right = Input.GetMouseButtonUp(1);

        if (left || right)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
            if (Physics.Raycast(ray, out hit))
            {
                if (right)
                {
                    Point p = new Point(hit.point);
                    CreateObstacle(p);
                    GridHandler.Instance.Show(p, hit.point);  
                }
                else
                {
                    Point p = new Point(hit.point);
                    if (_startTile == null)
                    {
                        _startPoint = p;
                        _startTile = CreateTile(p);
                    }
                    else
                    {
                        _goalPoint = p;
                        _goalTile = CreateTile(p);
                    }
                }      
            }            
        }       
    }



    GameObject CreateTile(Point p)
    {
        if (!Tiles.ContainsKey(p))
        {
            GameObject go = (GameObject)Instantiate(TilePrefab, p.vector3, Quaternion.identity) as GameObject;
            go.transform.localScale = new Vector3(go.transform.localScale.x * GridHandler.Instance.TileSize.x, go.transform.localScale.y, go.transform.localScale.z *  GridHandler.Instance.TileSize.y);
            Tiles.Add(p, go);
            return go;
        }           
        return null;
    }

    public void ClearTiles()
    {
        foreach (var pair in Tiles)
            Destroy(pair.Value);
        Tiles.Clear();

        _startTile = null;
        _goalTile = null;
    }



    void CreateObstacle(Point p)
    {
        if (!GridHandler.Instance.Obstacles.ContainsKey(p))
        {
            GameObject go = (GameObject)Instantiate(ObstaclePrefab, p.vector3, Quaternion.identity) as GameObject;
            GridHandler.Instance.AddObstacle(go.name + p.ToString(), p);
        }
        else
        {
            Destroy(Obstacles[p]);
            GridHandler.Instance.RemoveObstacle(p);
        }        
    }
}
