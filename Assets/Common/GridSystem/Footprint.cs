﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridSystem
{  
	 /// <summary>
    /// This class defines the size of the buildings and other obstacles on the grid.
    /// The footprint is a rectangle, measured in grid points.
    /// Every object on the grid should have this component.
    /// </summary>
    public class Footprint
    {
		public Point TopLeft { get { return Tile(Position, Size, Corner.TopLeft); }}
		public Point BottomLeft { get { return Tile(Position, Size, Corner.BottomLeft); }}
		public Point TopRight { get { return Tile(Position, Size, Corner.TopRight); }}
		public Point BottomRight { get { return Tile(Position, Size, Corner.BottomRight); }}


		public Point Size { get; set; }
		public Point Position { get; set; }


		#region Static Methods

		public static Point Tile(Point position, Point size, Corner corner)
		{
			if (corner == Corner.Center)
				return position;

			int dxLeft = (size.x % 2 == 0) ? (size.x / 2) - 1 : Mathf.FloorToInt((float)(size.x - 1) / 2f);
			int dyTop = (size.y % 2 == 0) ? (size.y / 2) - 1 : Mathf.FloorToInt((float)(size.y - 1) / 2f);


			// Calculate top-left corner
			Point tile = new Point(position.x - dxLeft, position.y + dyTop);

			switch (corner)
			{
				case Corner.TopRight: tile.x += size.x - 1; break;
				case Corner.BottomLeft: tile.y -= size.y - 1; break;
				case Corner.BottomRight: 
					tile.x += size.x - 1;
					tile.y -= size.y - 1;
					break;	
			}
			return tile;
		}


		public static Vector3 WorldPosition (Point position, Point size, Corner corner)
		{
			Vector3 centerOffset = Vector3.zero;
			if (size.x % 2 == 0)
				centerOffset.x = GridHandler.Instance.TileSize.x / 2;
			if (size.y % 2 == 0)
				centerOffset.z = GridHandler.Instance.TileSize.y / 2;
			
			var tile = Tile(position, size, corner);
			return GridHandler.PointToVector3(tile, corner) ;
		}

		public static bool ValidFootprint(Point position, Point size)
		{
			return GridHandler.ValidGridPoint(Tile(position, size, Corner.TopLeft)) && 
				GridHandler.ValidGridPoint(Tile(position, size, Corner.BottomRight)) &&
				GridHandler.ValidGridPoint(Tile(position, size, Corner.TopRight)) && 
				GridHandler.ValidGridPoint(Tile(position, size, Corner.BottomLeft));
		}

		public static Point ClosestCorner (Point from, Point position, Point size)
		{
			var topLeft = Tile(position, size, Corner.TopLeft);
			var bottomLeft = Tile(position, size, Corner.BottomLeft);
			var topRight = Tile(position, size, Corner.TopRight);
			var bottomRight = Tile(position, size, Corner.BottomRight);

			int dTL = GridHandler.ManhattanDistance (topLeft, from);
			int dBR = GridHandler.ManhattanDistance (bottomRight, from);
			int dTR = GridHandler.ManhattanDistance (topRight, from);
			int dBL = GridHandler.ManhattanDistance (bottomLeft, from);

			if (dTL < dBR && dTL < dTR && dTL < dBL)
				return topLeft;			
			if (dBR < dTR && dBR < dBL)
				return bottomRight;			
			if (dTR < dBL)
				return topRight;			
			return bottomLeft;
		}

		public static List<Point> GetPoints(Point position, Point size)
		{
			Point topLeft = Tile(position, size, Corner.TopLeft);

			List<Point> points = new List<Point>();
			for (int x = topLeft.x; x <= topLeft.x + size.x - 1; x++)
				for (int y = topLeft.y; y <= topLeft.y - size.y + 1; y--)
					points.Add(new Point(x, y));
			return points;
		}


		#endregion     
               
      
        

    }
}