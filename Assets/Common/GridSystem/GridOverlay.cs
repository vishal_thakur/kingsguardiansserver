﻿using UnityEngine;
using System.Collections;

namespace GridSystem
{
    [AddComponentMenu("Kings Guardians/GridSystem/CameraOverlay")]
    public class GridOverlay : SingletonMono<GridOverlay>
    {
        public bool ShowOverlay = false;

        public Material DarkGreenMaterial;

        /// <summary>
        /// The grid overlay line material.
        /// </summary>
        public Material GreenMaterial;

        /// <summary>
        /// The invalid footprint material.
        /// </summary>
        public Material RedMaterial;

        /// <summary>
        /// Raises the post render event.
        /// </summary>
        public void OnPostRender()
        {      
            if (!ShowOverlay) return;
                
            Vector3 pos = GridHandler.Instance.Origo + GridHandler.Instance.Offset;

            float horizontalLength = GridHandler.Instance.HorizontalTiles * GridHandler.Instance.TileSize.x;
            float verticalLength = GridHandler.Instance.VerticalTiles * GridHandler.Instance.TileSize.y;

            Material lineMaterial = GreenMaterial;
            lineMaterial.SetPass (0);

            GL.Begin( GL.LINES );   

			// Vertical lines
            for (int x = 0; x < GridHandler.Instance.HorizontalTiles; x++)
            {        
                float xPos = pos.x + x * GridHandler.Instance.TileSize.x;
                GL.Vertex3 (xPos, 0.2f, pos.z);
                GL.Vertex3 (xPos, 0.2f, pos.z + verticalLength);
            }

			// Horizontal lines
            for (int y = 0; y < GridHandler.Instance.VerticalTiles; y++) 
            {       
                float zPos = pos.z + y * GridHandler.Instance.TileSize.y;
                GL.Vertex3 (pos.x, 0.2f, zPos);
                GL.Vertex3 (pos.x + horizontalLength, 0.2f, zPos);
            }
            GL.End ();
        }
    }

}