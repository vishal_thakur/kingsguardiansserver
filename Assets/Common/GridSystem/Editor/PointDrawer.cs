﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace GridSystem
{
	[CustomPropertyDrawer(typeof(Point))]
	public class PointDrawer : PropertyDrawer 
	{
		float propertyWidth;

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty (position, label, property);

			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

			// Don't make child fields be indented
			int indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			propertyWidth = position.width;

			float x = (propertyWidth < 200 ) ? position.x - 105f : position.x;
			float y = (propertyWidth < 200 ) ? position.y + position.height/2 : position.y;
			float h = (propertyWidth < 200 ) ? position.height /2 : position.height;
			float w = (propertyWidth < 200 ) ? (propertyWidth+105 - 12*2 - 2)/2 :(position.width * 0.66f - 12*2 - 2)/2;

			EditorGUI.LabelField (new Rect (x, y, 12, h), "X");
			EditorGUI.PropertyField(new Rect( x+12, y, w, h), property.FindPropertyRelative ("x"), GUIContent.none);

			EditorGUI.LabelField (new Rect (x+w+14, y, 12, h), "Y");
			EditorGUI.PropertyField(new Rect( x + 24 + w, y, w, h), property.FindPropertyRelative ("y"), GUIContent.none);

			// Set indent back to what it was
			EditorGUI.indentLevel = indent;

			EditorGUI.EndProperty ();
		}

		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			return base.GetPropertyHeight (property, label) * ((propertyWidth < 200) ? 2 : 1);
		}
	}
}
