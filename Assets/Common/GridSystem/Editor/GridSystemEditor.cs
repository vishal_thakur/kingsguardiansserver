﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace GridSystem
{
    [CustomEditor(typeof(GridHandler))]
    public class GridSystemEditor : Editor 
    {
        public override void OnInspectorGUI()
        {            
            GridHandler myTarget = (GridHandler)target;

            if (myTarget.TestMode && Application.isPlaying)
            {
                GridOverlay.Instance.ShowOverlay = true;
                EditorGUILayout.LabelField("TEST MODE");

                if (GUILayout.Button("Create Path"))
                {
                    GridTesting.Instance.DrawPath();
                }

                if (GUILayout.Button("Clear Path"))
                {
                    GridTesting.Instance.ClearTiles();
                }
            }

            DrawDefaultInspector();
        }
    }

}


	/*

		MeshRenderer _tile;
		GridHandler _myTarget;

		public override void OnInspectorGUI()
		{
			

			_tile = (MeshRenderer)EditorGUILayout.ObjectField ("TopLeft Tile", _tile, typeof(MeshRenderer), true);
			if(GUILayout.Button("Calculate"))
			{
				Calculate();
			}

			GUILayout.Space (20f);

			DrawDefaultInspector ();
		}

		void Calculate()
		{
			Vector3 size = _tile.bounds.size;
			_myTarget.TileSize = new Vector2(size.x, size.z);
			_myTarget.TopLeftPosition = _tile.transform.position;
			_myTarget.GridSize.x = 0;
			_myTarget.GridSize.y = 0;

			_myTarget.Tiles = _myTarget.GetComponentsInChildren<GridTile> ().ToList();

			foreach(GridTile tile in _myTarget.Tiles)
			{
				Vector3 pos = tile.transform.position;

				float distanceX = pos.x - _myTarget.TopLeftPosition.x;
				float distanceZ = _myTarget.TopLeftPosition.z - pos.z;

				int x = Mathf.RoundToInt(distanceX / _myTarget.TileSize.x);
				int y = Mathf.RoundToInt(distanceZ / _myTarget.TileSize.y);

				tile.Coordinate = new Point(x, y);

				if(_myTarget.GridSize.x < x+1)
					_myTarget.GridSize.x = x+1;
				if(_myTarget.GridSize.y < y+1)
					_myTarget.GridSize.y = y+1;

				EditorUtility.SetDirty (tile);
			}

			BoxCollider collider = _myTarget.GetComponent<BoxCollider> ();

			float topLeftCornerX = _myTarget.TopLeftPosition.x - _myTarget.TileSize.x / 2;
			float topLeftCornerZ = _myTarget.TopLeftPosition.z + _myTarget.TileSize.y / 2;		

			float centerX = _myTarget.GridSize.x * _myTarget.TileSize.x / 2 + topLeftCornerX;
			float centerZ = _myTarget.GridSize.y * _myTarget.TileSize.y / 2 - topLeftCornerZ;

			float sizeX = (centerX - topLeftCornerX) * 2;
			float sizeZ = (centerZ + topLeftCornerZ) * 2;

			collider.size = new Vector3(sizeX, 0.1f, sizeZ);
			collider.center = new Vector3 (centerX - _myTarget.transform.position.x, 0 , -(centerZ + _myTarget.transform.position.z));

			EditorUtility.SetDirty (_myTarget);
		}
	}*/


