﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "NewCharacter", menuName = "GameContent/Character", order = 1)]
public class CharacterAsset : GameItemAsset
{
    public CombatCardAsset NativeCard;
    public List<SCharacterStat> Stats = new List<SCharacterStat>()
        {
            new SCharacterStat(){ Stat = CharacterStats.Health, Value = 0 },
            new SCharacterStat(){ Stat = CharacterStats.Attack, Value = 0 },
            new SCharacterStat(){ Stat = CharacterStats.Defence, Value = 0 },
            new SCharacterStat(){ Stat = CharacterStats.MagicAttack, Value = 0 },
            new SCharacterStat(){ Stat = CharacterStats.Speed, Value = 0 },
        };

    //Custom Inspector Properties
    public bool BattleFoldout = true;
	public bool RaidFoldout = true;
    public int BuildingLevelReq = 0;

    // Raid Scene Stats
	public bool Flying;
    public float MovementSpeed = 1;
    public int AttackRange = 1;

	public bool LandTarget;
	public bool AirTarget;


	public List<TraitData> Traits = new List<TraitData>();
	public List<ActiveAbility> ActiveAbilities = new List<ActiveAbility>();

    public CharacterContent ToContent()
    {
        var content = base.ToContent<CharacterContent>();
        content.NativeCardContentID = (NativeCard != null) ? NativeCard.ID : "";
        content.BaseStats = Stats;
        content.BuildingLevelReq = BuildingLevelReq;

		content.MovementSpeed = MovementSpeed;
		content.Flying = Flying;

        content.AttackRange = AttackRange;
		content.LandTarget = LandTarget;
		content.AirTarget = AirTarget;

		content.Traits = new List<TraitBase>();
		foreach (var t in Traits)
			content.Traits.Add(Trait.CreateTrait(t));

		content.ActiveAbilities = new List<ActiveAbility>();
		foreach (var a in ActiveAbilities)
		{
			a.Trait = Trait.CreateTrait(a.TraitData);
			content.ActiveAbilities.Add(a);
		}

        return content;
    }
}


