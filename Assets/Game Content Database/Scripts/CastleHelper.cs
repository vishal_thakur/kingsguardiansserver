﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
// REMOVE castle helper
/* 
public class CastleHelper : Singleton<CastleHelper> 
{

  

    public int GetBuildingLimit(string contentID, int castleUpgrade)
    {
        if (BuildingLimits.ContainsKey(castleUpgrade) && BuildingLimits[castleUpgrade].ContainsKey(contentID))
            return BuildingLimits[castleUpgrade][contentID];   
        return 0;
    }




    public Dictionary<int, Dictionary<string, int>> BuildingLimits = new Dictionary<int, Dictionary<string, int>>()
    {
        { 1, new Dictionary<string, int>()
            {
                {"Woodcutter",1},

                {"Treasury",1},

                {"House",1},
            }
        },

        {2, new Dictionary<string, int>()
            {
                {"Woodcutter",2},
                {"BloodCrystalMine",1},

                {"Treasury",2},
                {"BloodCrystalTreasury",1},

                {"Tavern",1},               
                {"House",2},

                {"Landmine",1},
                {"Canon",1},
                {"Wall",20},
            }
        },

        {3, new Dictionary<string, int>()
            {
                {"Woodcutter",3},
                {"BloodCrystalMine",2},
                {"StoneQuarry",1},

                {"Treasury",3},
                {"BloodCrystalTreasury",1},

                {"Tavern",1},  
                {"House",4},

                {"Academy",1},
                {"Blacksmith",1},

                {"Landmine",2},
                {"Canon",2},
                {"Wall",40},
            }
        },

        {4, new Dictionary<string, int>()
            {
                {"Woodcutter",3},
                {"BloodCrystalMine",3},
                {"StoneQuarry",2},
                {"IronMine",1},

                {"Treasury",4},
                {"BloodCrystalTreasury",2},

                {"Tavern",1},
                {"House",6},

                {"Academy",2},
                {"AltarofElements",1},
                {"Blacksmith",1},

                {"Landmine",3},
                {"Canon",2},
                {"Ballista",1},
                {"Flamethrower",1},
                {"Wall",60},
            }
        },

        {5, new Dictionary<string, int>()
            {
                {"Woodcutter",3},
                {"BloodCrystalMine",3},
                {"StoneQuarry",3},
                {"IronMine",2},
                {"DiamondMine",1},

                {"Treasury",5},
                {"BloodCrystalTreasury",2},

                {"Tavern",1},
                {"House",8},

                {"Academy",3},
                {"AltarofElements",1},
                {"Blacksmith",1},
                {"DefenseTower",1},

                {"Landmine",4},
                {"Canon",3},
                {"Ballista",2},
                {"Flamethrower",1},
                {"Wall",80},
            }
        },

        {6, new Dictionary<string, int>()
            {
                {"Woodcutter",3},
                {"BloodCrystalMine",3},
                {"StoneQuarry",3},
                {"IronMine",3},
                {"DiamondMine",2},

                {"Treasury",6},
                {"BloodCrystalTreasury",2},

                {"Tavern",1},
                {"House",10},

                {"Academy",3},
                {"AltarofElements",2},           
                {"Blacksmith",1},
                {"DefenseTower",2},
                {"SummonTower",1},  

                {"Landmine",6},
                {"Canon",3},
                {"Ballista",2},
                {"Flamethrower",1},
                {"Catapult",1},
                {"Breedingpit",1},
                {"Wall",100},
            }
        },

        {7, new Dictionary<string, int>()
            {
                {"Woodcutter",3},
                {"BloodCrystalMine",3},
                {"StoneQuarry",3},
                {"IronMine",3},
                {"DiamondMine",3},

                {"Treasury",7},
                {"BloodCrystalTreasury",2},

                {"Tavern",1},
                {"House",12},

                {"Academy",4},
                {"AltarofElements",3},   
                {"Blacksmith",1},
                {"DefenseTower",2},
                {"SummonTower",2},  

                {"Landmine",8},
                {"Canon",4},
                {"Ballista",3},
                {"Flamethrower",1},
                {"Catapult",1},
                {"Breedingpit",2},
                {"Wall",120},
            }
        },

        {8, new Dictionary<string, int>()
            {
                {"Woodcutter",3},
                {"BloodCrystalMine",3},
                {"StoneQuarry",3},
                {"IronMine",3},
                {"DiamondMine",3},

                {"Treasury",8},
                {"BloodCrystalTreasury",2},

                {"Tavern",1},
                {"House",14},

                {"Academy",4},
                {"AltarofElements",3},  
                {"Blacksmith",1},
                {"DefenseTower",2},
                {"SummonTower",2},  

                {"Landmine",11},
                {"Canon",5},
                {"Ballista",3},
                {"Flamethrower",2},
                {"Catapult",2},
                {"Breedingpit",2},
                {"Wall",140},
            }
        },

        {9, new Dictionary<string, int>()
            {
                {"Woodcutter",3},
                {"BloodCrystalMine",3},
                {"StoneQuarry",3},
                {"IronMine",3},
                {"DiamondMine",3},

                {"Treasury",9},
                {"BloodCrystalTreasury",2},

                {"Tavern",1},
                {"House",16},

                {"Academy",4},
                {"AltarofElements",3},   
                {"Blacksmith",1},
                {"DefenseTower",2},
                {"SummonTower",2},
                {"ElementalCauldron",1},

                {"Landmine",14},
                {"Canon",6},
                {"Ballista",4},
                {"Flamethrower",3},
                {"Catapult",3},
                {"Breedingpit",2},
                {"Wall",160},
            }
        },
    };

   


    public void TestDB()
    {
        string test = "";
        foreach (var pair in BuildingLimits)
        {
            foreach (var p in pair.Value)
            {
                var b = ServerLogic.Instance.GameContent.GetContent<BuildingContent>(p.Key);
                if (b == null)
                    test += "-"+p.Key + " (in castle "+pair.Key+" upgrade)\n";
            }
        }

        if (test != "")
            Debug.LogError("Castle Helper Database Test - Testing if there is a node in the helper, that doesn't exists in the game content:\n"+test);
        else
            Debug.Log("Castle Helper Database Test - Done");        
    }


    public void ReverseTestDB()
    {
        string test = "";
        foreach (var c in ServerLogic.Instance.GameContent.Buildings)
        {
            if (c.Key == "Castle")
                continue;
            
            bool b = false;
            foreach (var p in BuildingLimits)
            {
                if (p.Value.ContainsKey(c.Key))
                    b = true;
            }
            if(!b)
                test += "-" + c.Key+"\n";
        }
        if (test != "")
            Debug.LogError("Castle Helper Database Reverse Test - Building Limits doesn't contains information about:\n" + test);
        else
            Debug.Log("Castle Helper Database Reverse Test - Done");
    }
	
}
*/