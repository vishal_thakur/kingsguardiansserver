﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(DefenseCardAsset))]
public class DefenseCardAssetEditor : GameItemAssetEditor
{	
    ReorderableList types;

    protected void OnEnable()
    {
        base.OnEnable();

        types = new ReorderableList(serializedObject, serializedObject.FindProperty("DefenceTypes"), true, true, true, true);
        types.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Type of Defences");
        };

        types.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = types.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            float x = rect.x;

            EditorGUI.LabelField(
                new Rect(x, rect.y, 80, EditorGUIUtility.singleLineHeight), "Defense "+element.FindPropertyRelative("Amount").intValue +"%");
            x += 90;

            EditorGUI.PropertyField(
                new Rect(x, rect.y, 100, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("BloodCrystalCost"), GUIContent.none);
            x += 100;

            EditorGUI.LabelField(
                new Rect(x, rect.y, 80, EditorGUIUtility.singleLineHeight), "Blood Crystal");
            x += 100;

            EditorGUI.PropertyField(
                new Rect(x, rect.y, 50, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("CraftTime"), GUIContent.none);
            x += 60;

            EditorGUI.LabelField(
                new Rect(x, rect.y, 80, EditorGUIUtility.singleLineHeight), "seconds");
        };
    }


    public override void OnInspectorGUI()
    {
        DefenseCardAsset myTarget = (DefenseCardAsset)target;
        base.OnInspectorGUI();

        myTarget.DefenseFoldout = EditorGUILayout.Foldout(myTarget.DefenseFoldout, "Defence Card Informations");

        EditorGUI.BeginChangeCheck();

        if (myTarget.DefenseFoldout)
        {
            if (types != null)
            {
                serializedObject.Update();
                types.DoLayoutList();
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}
