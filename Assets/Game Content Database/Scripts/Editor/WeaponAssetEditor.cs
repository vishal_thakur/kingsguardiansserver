﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(WeaponAsset))]
public class WeaponAssetEditor : GameItemAssetEditor
{	
    ReorderableList stats;
    ReorderableList defenses;


    protected void OnEnable()
    {
        base.OnEnable();

        stats = new ReorderableList(serializedObject, serializedObject.FindProperty("StatBonuses"), true, true, true, true);
        stats.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Stat bonuses");
        };

        stats.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = stats.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            var statproperty = element.FindPropertyRelative("Stat");

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width/2, 
                    EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("Stat"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + rect.width/2, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Value"), GUIContent.none);
        };



        defenses = new ReorderableList(serializedObject, serializedObject.FindProperty("DefenceBonuses"), true, true, true, true);
        defenses.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Elemental Defenses");
        };

        defenses.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = defenses.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            var statproperty = element.FindPropertyRelative("Element");

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width/2, 
                    EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("Element"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + rect.width/2, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Amount"), GUIContent.none);
        };
    }


    public override void OnInspectorGUI()
    {
        WeaponAsset myTarget = (WeaponAsset)target;
        base.OnInspectorGUI();

        myTarget.WeaponFoldout = EditorGUILayout.Foldout(myTarget.WeaponFoldout, "Weapon Informations");

        if (myTarget.WeaponFoldout)
        {
            if (defenses != null)
            {
                serializedObject.Update();
                defenses.DoLayoutList();
                serializedObject.ApplyModifiedProperties();
            }
            else
                GUILayout.Label("Defenses are null");
            
            if (stats != null)
            {
                serializedObject.Update();
                stats.DoLayoutList();
                serializedObject.ApplyModifiedProperties();
            }
            else
                GUILayout.Label("Stats are null");
        }
    }
}
