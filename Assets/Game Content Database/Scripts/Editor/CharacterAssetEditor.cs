﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Linq;

[CustomEditor(typeof(CharacterAsset))]
public class CharacterAssetEditor : GameItemAssetEditor
{
    ReorderableList stats;

    protected void OnEnable()
    {
        base.OnEnable();

        CharacterAsset myTarget = (CharacterAsset)target;
        var statEnums = (CharacterStats[])Enum.GetValues(typeof(CharacterStats));

        if (statEnums.Length > myTarget.Stats.Count)
        {
            foreach (var s in statEnums)
            {
                if (myTarget.Stats.Find(x => x.Stat == s) == null)
                    myTarget.Stats.Add(new SCharacterStat(){ Stat = s, Value = 0 });            
            }
            myTarget.Stats.OrderBy(x => x.Stat);
        }


        stats = new ReorderableList(serializedObject, serializedObject.FindProperty("Stats"), false, true, false, false);
        stats.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Statistics");
        };

        stats.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = stats.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            var statproperty = element.FindPropertyRelative("Stat");

            EditorGUI.LabelField(
                new Rect(rect.x, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight), statproperty.enumDisplayNames[statproperty.enumValueIndex]);
            EditorGUI.PropertyField(
                new Rect(rect.x + rect.width/2, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Value"), GUIContent.none);
        };
    }


    public override void OnInspectorGUI()
    {
		GUIStyle title = new GUIStyle(GUI.skin.label);
		title.fontStyle = FontStyle.Bold;

        CharacterAsset myTarget = (CharacterAsset)target;
        base.OnInspectorGUI();

		myTarget.BattleFoldout = EditorGUILayout.Foldout(myTarget.BattleFoldout, "Battle Stats");

        EditorGUI.BeginChangeCheck();

		if (myTarget.BattleFoldout)
		{
			myTarget.NativeCard = (CombatCardAsset)EditorGUILayout.ObjectField("Native Card", myTarget.NativeCard, typeof(CombatCardAsset), false);

			if (stats != null)
			{
				serializedObject.Update();
				stats.DoLayoutList();
				serializedObject.ApplyModifiedProperties();
			}
			else
				GUILayout.Label("Stats are null");
		}

		myTarget.RaidFoldout =  EditorGUILayout.Foldout( myTarget.RaidFoldout, "Raid Stats");  
		if (myTarget.RaidFoldout)
		{
			myTarget.Flying = EditorGUILayout.Toggle("Flying", myTarget.Flying);
			myTarget.MovementSpeed = EditorGUILayout.Slider("Movement Speed", myTarget.MovementSpeed, 0.5f, 1.5f);
			myTarget.AttackRange = EditorGUILayout.IntField("Attack Range", myTarget.AttackRange);
			myTarget.LandTarget = EditorGUILayout.Toggle("Target Land Units", myTarget.LandTarget);
			myTarget.AirTarget = EditorGUILayout.Toggle("Target Air Units", myTarget.AirTarget);


			EditorGUILayout.Space();
			EditorGUILayout.Space(); 

			// PASSIVE ABILITIES
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Passive Abilities", title);
			if (GUILayout.Button("+"))
				myTarget.Traits.Add(new TraitData());
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			HorizontalLine();

			foreach (var a in myTarget.Traits)
			{
				var remove = DrawPassiveAbility(a);
				if (remove != null)
				{
					myTarget.Traits.Remove(remove);
					return;
				}
			}
			EditorGUI.indentLevel--;

			EditorGUILayout.Space();
			EditorGUILayout.Space(); 

			// ACTIVE ABILITIES
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Active Abilities", title);
			if (GUILayout.Button("+"))
				myTarget.ActiveAbilities.Add(new ActiveAbility());
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			HorizontalLine();
			foreach (var a in myTarget.ActiveAbilities)
			{
				var remove = DrawActiveAbility(a);
				if (remove != null)
				{
					myTarget.ActiveAbilities.Remove(remove);
					return;
				}
			}
			EditorGUI.indentLevel--;
		}

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(myTarget);
	}

	ActiveAbility DrawActiveAbility(ActiveAbility a)
	{
		EditorGUILayout.BeginHorizontal();
		a.Name = EditorGUILayout.TextField("Name", a.Name);
		if (GUILayout.Button("X"))
			return a;
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();

		EditorGUILayout.LabelField("Cost", GUILayout.Width(60));
		a.Cost = EditorGUILayout.IntField(a.Cost, GUILayout.Width(60));

		EditorGUILayout.LabelField("Radius", GUILayout.Width(60));
		a.Radius = EditorGUILayout.IntField(a.Radius, GUILayout.Width(60));

		EditorGUILayout.LabelField("Duration", GUILayout.Width(60));
		a.Duration = EditorGUILayout.IntField(a.Duration, GUILayout.Width(60));
		EditorGUILayout.EndHorizontal();

		a.Target = (TargetType)EditorGUILayout.EnumPopup("Target", a.Target);

		EditorGUILayout.Space();

		if (a.Target == TargetType.Self)
		{
			//traits
			a.TraitData = DrawPassiveAbility(a.TraitData, false);
		}
		else
		{
			a.AuraType = (AuraType)EditorGUILayout.EnumPopup("Aura", a.AuraType);
			EditorGUI.indentLevel++;
			switch (a.AuraType)
			{
				case AuraType.Combined:
					a.Amount = EditorGUILayout.FloatField("HP Adjust Amount", a.Amount);
					EditorGUILayout.Space();
					a.TraitData = DrawPassiveAbility(a.TraitData, false);
					break;


				case AuraType.HPAdjust:
					a.Amount = EditorGUILayout.FloatField("Amount", a.Amount);
					break;
				case AuraType.TraitGranting: 
					a.TraitData = DrawPassiveAbility(a.TraitData, false);
					break;
			}
			EditorGUI.indentLevel--;
		}	
		HorizontalLine();
		return null;
	}


	TraitData DrawPassiveAbility(TraitData t, bool removeButton = true)
	{
		if (t == null)
			t = new TraitData();


		EditorGUILayout.BeginHorizontal();
		t.Type = (TraitType)EditorGUILayout.EnumPopup(t.Type);
		if (removeButton && GUILayout.Button("X")) return t;
		EditorGUILayout.EndHorizontal();

		switch (t.Type)
		{
			case TraitType.DamageIncrease:
				t.FloatParameter = EditorGUILayout.FloatField("Multiplier", t.FloatParameter);
				t.Unit = (UnitType)EditorGUILayout.EnumPopup("Against", t.Unit);
				t.ContentID = EditorGUILayout.TextField("ContentID: ", t.ContentID);
				break;

			case TraitType.DamageResistance:
				t.FloatParameter = EditorGUILayout.FloatField("Percentage", t.FloatParameter);
				break;

			case TraitType.Miss:
				t.FloatParameter = EditorGUILayout.FloatField("Chance", t.FloatParameter);
				t.Unit = (UnitType)EditorGUILayout.EnumPopup("Against", t.Unit);
				t.ContentID = EditorGUILayout.TextField("ContentID: ", t.ContentID);
				break;

			case TraitType.Explosion:
				t.FloatParameter = EditorGUILayout.FloatField("Multiplier", t.FloatParameter);
				break;

			case TraitType.AlternateDamage:
				t.DamageType = (Element)EditorGUILayout.EnumPopup("DamageType", t.DamageType);
				break;

			case TraitType.StatIncrease:
				t.Stat = (CharacterStats)EditorGUILayout.EnumPopup("Stat", t.Stat);
				t.FloatParameter = EditorGUILayout.FloatField("Amount", t.FloatParameter);
				break;

			case TraitType.Necromancy:
				t.IntParameter = EditorGUILayout.IntField("Number of summons", t.IntParameter);
				break;

			case TraitType.DOT:
				t.IntParameter = EditorGUILayout.IntField("Damage", t.IntParameter);
				t.DamageType = (Element)EditorGUILayout.EnumPopup("DamageType", t.DamageType);
				break;
		}
		return (removeButton) ? null : t;
	}




	void HorizontalLine()
	{
		GUILayout.Space(10);
		GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));
		GUILayout.Space(10);
	}

}
