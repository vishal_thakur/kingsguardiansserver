﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Linq;
using System.Collections.Generic;

[CustomEditor(typeof(CombatCardAsset))]
public class CombatCardAssetEditor : GameItemAssetEditor
{	
    ReorderableList effects;
    List<Type> _effectClasses = new List<Type>();
    Dictionary<string, Type> _effectClassDict = new Dictionary<string, Type>();

    CombatCardAsset _myTarget;

    protected void OnEnable()
    {
        base.OnEnable();

        System.Type[] types = System.Reflection.Assembly.GetAssembly(typeof(EffectBase)).GetTypes();
        _effectClasses.Clear();
        foreach (var t in types)
        {
            if (t.IsSubclassOf(typeof(EffectBase)))
            {
                _effectClasses.Add(t);          
                _effectClassDict.Add(t.FullName, t);
            }
        }

        effects = new ReorderableList(serializedObject, serializedObject.FindProperty("Effects"), true, true, true, true);
        effects.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Effects");
        };

        effects.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = effects.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;

            EditorGUI.LabelField(
                new Rect(rect.x, rect.y, rect.width/2, 
                    EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("Name").stringValue);
        };

        effects.onAddDropdownCallback = (Rect buttonRect, ReorderableList l) => {
            var menu = new GenericMenu();
            foreach( var n in _effectClasses)
            {
                menu.AddItem(new GUIContent(n.Name), false, AddEffect, n);
            }             
            menu.ShowAsContext();
        };
    }

    void AddEffect(object target) 
    {  
        var index = effects.serializedProperty.arraySize;
        effects.serializedProperty.arraySize++;
        effects.index = index;

        Type t = (Type)target;

        EffectContent effect = new EffectContent();
        effect.Name = t.Name;
        effect.Type = t.FullName;

        _myTarget.Effects.Add(effect);
    }


    public override void OnInspectorGUI()
    {
        _myTarget = (CombatCardAsset)target;
        base.OnInspectorGUI();

        _myTarget.CardFoldout = EditorGUILayout.Foldout(_myTarget.CardFoldout, "Card Informations");

        EditorGUI.BeginChangeCheck();

        if (_myTarget.CardFoldout)
        {
            _myTarget.MaxStacks = EditorGUILayout.IntField(new GUIContent("Max Stacks: ", "Zero means unlimited"), _myTarget.MaxStacks);
            _myTarget.Category = (ItemCategory)EditorGUILayout.EnumPopup("Category",_myTarget.Category);
            _myTarget.ActionID = (ActionID)EditorGUILayout.EnumPopup("Action",_myTarget.ActionID);
            _myTarget.TargetType = (TargetType)EditorGUILayout.EnumPopup("Target Type",_myTarget.TargetType);

            _myTarget.Duration = EditorGUILayout.IntField(new GUIContent("Duration", "Zero means unlimited"), _myTarget.Duration);
            _myTarget.UseOnDead = EditorGUILayout.Toggle("Useable on dead", _myTarget.UseOnDead);

            if (effects != null)
            {
                serializedObject.Update();
                effects.DoLayoutList();
                serializedObject.ApplyModifiedProperties();
            }

            // Selected effect
            if (effects.index > -1)
            {
                EffectContent effect = _myTarget.Effects[effects.index];
                GUILayout.Label(effect.Name, EditorStyles.boldLabel);
                EditorGUILayout.LabelField("Class", effect.Type);   

                var sEffect = serializedObject.FindProperty("Effects").GetArrayElementAtIndex(effects.index);
               
                effect.Always = EditorGUILayout.Toggle("Always", effect.Always);
                if (!effect.Always)
                    effect.SetParameter("Chance", EditorGUILayout.Slider("Chance",effect.GetParameter<float>("Chance"), 0, 1));

                foreach (var f in _effectClassDict[effect.Type].GetFields())
                {
                    if (f.IsPublic && !f.IsLiteral)
                    {
                        DrawField(effect, f.Name, f.FieldType);
                    }
                }

                EditorGUILayout.Space();
                GUILayout.Box("", new GUILayoutOption[]{GUILayout.ExpandWidth(true), GUILayout.Height(1)});
                EditorGUILayout.Space();               
            }
        }

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(_myTarget);
    }

    void DrawField(EffectContent effect, string fieldName, Type fieldType)
    {    
        
        if (fieldType.IsEnum)
        {
            var names = Enum.GetNames(fieldType);
            effect.SetParameter(fieldName, EditorGUILayout.Popup(fieldName, effect.GetParameter<int>(fieldName), names));           
            return;
        }

       

        switch (fieldType.ToString())
        {
            case "System.Int32": 
                effect.SetParameter(fieldName, EditorGUILayout.IntField(fieldName, effect.GetParameter<int>(fieldName)));
                break;

            case "System.Single":    
                if (fieldName != "Chance")
                     effect.SetParameter(fieldName, EditorGUILayout.FloatField(fieldName, effect.GetParameter<float>(fieldName)));
                break;

            case "System.String": 
                effect.SetParameter(fieldName, EditorGUILayout.TextField(fieldName, effect.GetParameter<string>(fieldName)));
                break;

            case "System.Boolean": 
                effect.SetParameter(fieldName, EditorGUILayout.Toggle(fieldName, effect.GetParameter<bool>(fieldName)));
                break;

            /*case "System.Collections.Generic.List`1[SCharacterStat]":
                string v = effect.GetParameter<string>("StatModList");
                List<string> values = (v != null) ? v.Split('|').ToList() : new List<string>();

                EditorGUI.BeginChangeCheck();
                for (int i = 0; i < values.Count; i += 2)
                {
                    EditorGUILayout.BeginHorizontal();
                    int a = int.Parse(values[i]);
                    int b = int.Parse(values[i+1]);

                    values[i] = ""+(int)(CharacterStats)EditorGUILayout.EnumPopup((CharacterStats)a);
                    values[i + 1] = EditorGUILayout.IntField(b).ToString();
                    EditorGUILayout.EndHorizontal();
                }

                if (GUILayout.Button("Add"))
                {
                    values.Add("0");
                    values.Add("0");
                }

                if (EditorGUI.EndChangeCheck())
                {
                    effect.SetParameter("StatModList", String.Join("|", values.ToArray()));
                }               
                break;*/

            default :
                EditorGUILayout.TextField(fieldName, " not implemented type ["+fieldType.ToString()+"]");
                break;
        }
    }
   
}
