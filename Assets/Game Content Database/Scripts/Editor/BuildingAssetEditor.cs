﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(BuildingAsset))]
public class BuildingAssetEditor : GameItemAssetEditor {

    protected override bool ShowCraftInfo { get { return false; }}

    ReorderableList upgrades;

    ReorderableList upgradeCosts;
    ReorderableList miningCost;


    int _selectedUpgrade = 0;
    string[] _upgradeStrings;
   
    protected void OnEnable()
    {
        base.OnEnable();

        _upgradeStrings = new string[9];
        for (int i = 0; i < 9; i++)
            _upgradeStrings[i] = (i+1)+"";      

        RefreshUpdateCosts();
        RefreshMiningCosts();


        upgrades = new ReorderableList(serializedObject, serializedObject.FindProperty("Upgrades"), true, true, true, true);
        upgrades.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Upgrades");
        };

        upgrades.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = upgrades.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight), "Upgrade " + (index+1));
        };

        upgrades.onSelectCallback = (ReorderableList l) => { 
            _selectedUpgrade = l.index;

            RefreshUpdateCosts();
        };


        upgrades.onRemoveCallback = (ReorderableList l) => {    
            if (EditorUtility.DisplayDialog("Warning!", "Are you sure you want to delete this upgrade?", "Yes", "No"))
            {
                _selectedUpgrade = l.index-1;
                RefreshUpdateCosts();

                ReorderableList.defaultBehaviours.DoRemoveButton(l);               
            }
        };

        upgrades.index = _selectedUpgrade;
    }

    public override void OnInspectorGUI()
    {
        BuildingAsset myTarget = (BuildingAsset)target;
        base.OnInspectorGUI();

        EditorGUI.BeginChangeCheck();

        myTarget.BuildingFoldout = EditorGUILayout.Foldout(myTarget.BuildingFoldout, "Building Informations");
        if (myTarget.BuildingFoldout)
        {                       
            DrawInspector(myTarget);

            EditorGUILayout.Space();
              
            serializedObject.Update();
            upgrades.DoLayoutList();
            serializedObject.ApplyModifiedProperties();

            DrawUpgradeInspector(myTarget, _selectedUpgrade);
         }

		myTarget.MaxAmountFoldout = EditorGUILayout.Foldout(myTarget.MaxAmountFoldout, "Max Building Amount ");
		if (myTarget.MaxAmountFoldout)
		{    
			if (myTarget.MaxAmount == null || myTarget.MaxAmount.Length == 0)
				myTarget.MaxAmount = new int[9];

			for (int cnt = 0; cnt < myTarget.MaxAmount.Length; cnt++)
			{
				EditorGUILayout.BeginHorizontal();
				myTarget.MaxAmount[cnt] = EditorGUILayout.IntField("Castle " + (cnt + 1), myTarget.MaxAmount[cnt]);
				if (GUILayout.Button("copy below", GUILayout.Width(100)))
				{
					for (int i = cnt + 1; i < myTarget.MaxAmount.Length; i++)
						myTarget.MaxAmount[i] = myTarget.MaxAmount[cnt];
				}
				EditorGUILayout.EndHorizontal();
			}
		}



        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(myTarget);
    }

    protected void DrawInspector(BuildingAsset myTarget)
    {
        myTarget.Category = (BuildingCategory)EditorGUILayout.EnumPopup("Category: ",myTarget.Category);
        myTarget.ModelSize = EditorGUILayout.Vector2Field("Model Size", myTarget.ModelSize);

        switch (myTarget.Category)
        {
            // MINE
            case BuildingCategory.Resource_mine:
                myTarget.Material = (Currency)EditorGUILayout.EnumPopup("Material", myTarget.Material);               
                serializedObject.Update();
                miningCost.DoLayoutList();
                serializedObject.ApplyModifiedProperties();
                break;

            case BuildingCategory.Resource_generator:
            case BuildingCategory.Resource_storageOnly:
                myTarget.Material = (Currency)EditorGUILayout.EnumPopup("Material", myTarget.Material);
                break;

            case BuildingCategory.CardCreator:
                myTarget.ItemCategory = (ItemCategory)EditorGUILayout.EnumPopup("Item Type", myTarget.ItemCategory);
                break;          
                     
        }
    }


    protected void DrawUpgradeInspector(BuildingAsset myTarget, int upgrade)
    {
        myTarget.Upgrades[_selectedUpgrade].UpgradeTime = TimeField("Upgrade time", myTarget.Upgrades[_selectedUpgrade].UpgradeTime);

        EditorGUILayout.Space();

        serializedObject.Update();
        upgradeCosts.DoLayoutList();
        serializedObject.ApplyModifiedProperties();

        EditorGUILayout.Space();

        myTarget.Upgrades[_selectedUpgrade].MaxHealth = EditorGUILayout.IntField("Max Health: ",myTarget.Upgrades[_selectedUpgrade].MaxHealth);


        switch (myTarget.Category)
        {
            case BuildingCategory.Resource_mine:
                myTarget.Upgrades[_selectedUpgrade].Storage = EditorGUILayout.IntField("Storage: ",myTarget.Upgrades[_selectedUpgrade].Storage);
                myTarget.Upgrades[_selectedUpgrade].ProductionAmount = EditorGUILayout.IntField("Amount (per 5 mins): ",myTarget.Upgrades[_selectedUpgrade].ProductionAmount);
                break;

            case BuildingCategory.Resource_generator:
                myTarget.Upgrades[_selectedUpgrade].ProductionAmount = EditorGUILayout.IntField("Amount (per hours): ",myTarget.Upgrades[_selectedUpgrade].ProductionAmount);
                break;

            case BuildingCategory.Resource_storageOnly:
                myTarget.Upgrades[_selectedUpgrade].Storage = EditorGUILayout.IntField("Storage: ",myTarget.Upgrades[_selectedUpgrade].Storage);
                break;


            case BuildingCategory.WorkerCreator:

                // WORKER
                EditorGUILayout.BeginHorizontal();
    
                bool b = EditorGUILayout.Toggle("Worker", myTarget.Upgrades[_selectedUpgrade].WorkerPrice >= 0);
                if (b)
                {
                    if (myTarget.Upgrades[_selectedUpgrade].WorkerPrice < 0) myTarget.Upgrades[_selectedUpgrade].WorkerPrice = 0;
                    myTarget.Upgrades[_selectedUpgrade].WorkerPrice = EditorGUILayout.IntField(myTarget.Upgrades[_selectedUpgrade].WorkerPrice);
                    GUILayout.Label("Ether");
                }
                else
                    myTarget.Upgrades[_selectedUpgrade].WorkerPrice = -1;

            
                EditorGUILayout.EndHorizontal();

                // WARLOCK
                EditorGUILayout.BeginHorizontal();
                bool bw = EditorGUILayout.Toggle("Warlock", myTarget.Upgrades[_selectedUpgrade].WarlockPrice >= 0);
                if (bw)
                {
                    if (myTarget.Upgrades[_selectedUpgrade].WarlockPrice < 0) myTarget.Upgrades[_selectedUpgrade].WarlockPrice = 0;
                    myTarget.Upgrades[_selectedUpgrade].WarlockPrice = EditorGUILayout.IntField(myTarget.Upgrades[_selectedUpgrade].WarlockPrice);
                    GUILayout.Label("Ether");
                }
                else
                    myTarget.Upgrades[_selectedUpgrade].WarlockPrice = -1;
                EditorGUILayout.EndHorizontal();

                break;

            case BuildingCategory.CardCreator:
                
                break;

			case BuildingCategory.Defense:
				myTarget.Upgrades[_selectedUpgrade].Range = EditorGUILayout.IntField("Range (tiles): ", myTarget.Upgrades[_selectedUpgrade].Range);
				myTarget.Upgrades[_selectedUpgrade].DamageRadius = EditorGUILayout.IntField("Radius (tiles): ", myTarget.Upgrades[_selectedUpgrade].DamageRadius);
				myTarget.Upgrades[_selectedUpgrade].AttackSpeed = EditorGUILayout.FloatField("Attack Speed (sec): ", myTarget.Upgrades[_selectedUpgrade].AttackSpeed);
				myTarget.Upgrades[_selectedUpgrade].Damage = EditorGUILayout.IntField("Damage: ", myTarget.Upgrades[_selectedUpgrade].Damage);
               
				EditorGUILayout.Space();
				GUILayout.Label("Target");
				EditorGUI.indentLevel++;
				myTarget.Upgrades[_selectedUpgrade].LandUnit = EditorGUILayout.Toggle("Land Units", myTarget.Upgrades[_selectedUpgrade].LandUnit);
				myTarget.Upgrades[_selectedUpgrade].FlyingUnit = EditorGUILayout.Toggle("Flying Units", myTarget.Upgrades[_selectedUpgrade].FlyingUnit);
				EditorGUI.indentLevel--;

				break;            
        }
    }








    void RefreshUpdateCosts()
    {
        upgradeCosts = new ReorderableList(serializedObject, serializedObject.FindProperty("Upgrades").GetArrayElementAtIndex(_selectedUpgrade).FindPropertyRelative("Cost"), true, true, true, true);
        upgradeCosts.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Upgrade Costs");
        };

        upgradeCosts.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = upgradeCosts.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Material"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + rect.width/2, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Amount"), GUIContent.none);
        };
    }

    void RefreshMiningCosts()
    {
        miningCost = new ReorderableList(serializedObject, serializedObject.FindProperty("MiningPrices"), true, true, true, true);
        miningCost.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Mining Costs");
        };

        miningCost.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = miningCost.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Material"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + rect.width/2, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Amount"), GUIContent.none);
        };
    }
}
