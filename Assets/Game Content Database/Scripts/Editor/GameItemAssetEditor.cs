﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(GameItemAsset))]
public class GameItemAssetEditor : Editor 
{
    protected virtual bool ShowCraftInfo { get { return true; }}

    ReorderableList costs;

    protected void OnEnable()
    {
        costs = new ReorderableList(serializedObject, serializedObject.FindProperty("Cost"), true, true, true, true);
        costs.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Costs");
        };

        costs.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = costs.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Material"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + rect.width/2, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Amount"), GUIContent.none);
        };
    }

    public override void OnInspectorGUI()
    {
        GameItemAsset myTarget = (GameItemAsset)target;

        myTarget.BaseFoldout = EditorGUILayout.Foldout(myTarget.BaseFoldout, "Basic Informations");

        EditorGUI.BeginChangeCheck();

        if (myTarget.BaseFoldout)
        {
            myTarget.ID = EditorGUILayout.TextField("ID",myTarget.ID);
            myTarget.Name = EditorGUILayout.TextField("Name",myTarget.Name);

            GUILayout.Label("Description");
            myTarget.Description = EditorGUILayout.TextArea(myTarget.Description, GUILayout.Height(150) );

            if( Utils.IsInherited(myTarget.GetType(), typeof(ItemAsset)))
            {                
                var item = (ItemAsset)myTarget;
                item.BuildingLevelReq = EditorGUILayout.IntSlider("Required Building Lvl", item.BuildingLevelReq, 1, 9);
            }

            if( Utils.IsInherited(myTarget.GetType(), typeof(CharacterAsset)))
            {                
                var c = (CharacterAsset)myTarget;
                c.BuildingLevelReq = EditorGUILayout.IntSlider("Required Building Lvl", c.BuildingLevelReq, 0, 9);
            }

            if (ShowCraftInfo)
            {
                myTarget.CraftTime = TimeField("Craft time", myTarget.CraftTime);


                serializedObject.Update();
                costs.DoLayoutList();
                serializedObject.ApplyModifiedProperties();
            }
        }

        if (EditorGUI.EndChangeCheck())
        {
            myTarget.name = myTarget.Name;
            EditorUtility.SetDirty(myTarget);
        }
    }



    protected float TimeField(string label, float time)
    {
        int[] t = CalculateTime(time);

        int d = t[0], h = t[1], m = t[2], s = t[3];            

        EditorGUILayout.LabelField(label+" ("+ time+" seconds)");

        EditorGUILayout.BeginHorizontal();      
        d = EditorGUILayout.IntField(d, GUILayout.Width(40));
        EditorGUILayout.LabelField("Days", GUILayout.Width(40));

        h = EditorGUILayout.IntField(h, GUILayout.Width(40));
        EditorGUILayout.LabelField("Hours", GUILayout.Width(40));

        m = EditorGUILayout.IntField(m, GUILayout.Width(40));
        EditorGUILayout.LabelField("Mins", GUILayout.Width(40));

        s = EditorGUILayout.IntField(s, GUILayout.Width(40));
        EditorGUILayout.LabelField("Secs", GUILayout.Width(40));

        EditorGUILayout.EndHorizontal();

        return d * 60 * 60 * 24 + h * 60 * 60 + m * 60 + s;
       
    }


    protected int[] CalculateTime(float seconds)
    {
        int days = 0, hours = 0, minutes = 0;

        days = Mathf.FloorToInt(seconds / (60f * 60f * 24f));
        seconds -= days * (60f * 60f * 24f);

        if (seconds > 0)
        {
            hours = Mathf.FloorToInt(seconds / (60f * 60f));
            seconds -= hours * (60f * 60f);

            if (seconds > 0)
            {
                minutes = Mathf.FloorToInt(seconds / 60f);
                seconds -= minutes * 60f;
            }
        }      

        if (seconds < 0)
            seconds = 0;

        return new int[]{ days, hours, minutes, (int)seconds };
    }
}
