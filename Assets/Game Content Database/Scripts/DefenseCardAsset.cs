﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "NewDefenseCard", menuName = "GameContent/DefenseCard", order = 4)]
public class DefenseCardAsset : ItemAsset
{
    public List<DefenseType> DefenceTypes = new List<DefenseType>()
        {
            new DefenseType(){ Amount = 5 }, 
            new DefenseType(){ Amount = 10 }, 
            new DefenseType(){ Amount = 15 }, 
            new DefenseType(){ Amount = 20 }, 
            new DefenseType(){ Amount = 25 }, 
            new DefenseType(){ Amount = 30 }, 
            new DefenseType(){ Amount = 35 }, 
            new DefenseType(){ Amount = 40 }, 
            new DefenseType(){ Amount = 45 }, 
        };

    [System.Serializable]
    public class DefenseType
    {
        public int Amount;
        public int BloodCrystalCost;
        public float CraftTime;
    }

    //Custom Inspector Properties
    public bool DefenseFoldout = true;  

    public List<DefenseContent> CreateAll(Element element)
    {
        List<DefenseContent> list = new List<DefenseContent>();
        foreach (var t in DefenceTypes)
        {
            DefenseContent content = base.ToContent<DefenseContent>();

            content.ContentID = content.ContentID.Replace("{element}", element.ToString()).Replace("{amount}", t.Amount.ToString());
            content.Name = content.Name.Replace("{element}", element.ToString()).Replace("{amount}", t.Amount.ToString());
            content.Description = content.Description.Replace("{element}", element.ToString()).Replace("{amount}", t.Amount.ToString());

            content.Price = new List<SMaterial>() { new SMaterial(){ Material = Currency.BloodCrystal, Amount = t.BloodCrystalCost } };
            content.CraftTime = t.CraftTime;

            content.Category = ItemCategory.Defence;
            content.MaxStacks = 10;
            content.Element = element;
            content.Amount = t.Amount;
            content.BuildingLevelReq = Mathf.RoundToInt(t.Amount / 5);

            list.Add(content);
        }
        return list;
    }
}
