﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "NewBuilding", menuName = "GameContent/Building", order = 0)]
public class BuildingAsset : GameItemAsset
{
    public Vector2 ModelSize = Vector2.zero;

    public BuildingCategory Category;
    public BuildingUpgrade[] Upgrades = new BuildingUpgrade[9];
	public int[] MaxAmount = new int[9];

    // MINE & STORAGE
    public Currency Material;

    // MINE
    public List<SMaterial> MiningPrices = new List<SMaterial>();
   
    // CRAFT
    public ItemCategory ItemCategory;

    //Custom Inspector Properties
    public bool BuildingFoldout = true;
	public bool MaxAmountFoldout = true;



    public T ToBuildingContent<T>() where T : BuildingContent
    {
        T content = base.ToContent<T>();
        content.ModelSize = new GridSystem.Point((int)ModelSize.x, (int)ModelSize.y);   
        content.Category = Category;
        content.Upgrades = Upgrades;
        content.Material = Material;
        content.MiningPrices = MiningPrices;
        content.ItemCategory = ItemCategory;
		content.MaxAmount = MaxAmount;

        return content as T;
    }



}

