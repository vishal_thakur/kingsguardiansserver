﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameContent 
{
    public Dictionary<string, CharacterContent> Characters { get; private set; }
    public Dictionary<string, CharacterContent> Monsters { get; private set; }
    public Dictionary<string, BuildingContent> Buildings { get; private set; }
    public Dictionary<string, ItemContent> Items { get; private set; } 
    public Dictionary<string, ItemContent> Weapons { get; private set; } 

    public GameContent()
    {
        GameContentAsset db = Resources.Load<GameContentAsset>("GameContentDatabase");

        // Characters
        Characters = new Dictionary<string, CharacterContent>();
        foreach (var c in db.Characters)
        {
            var cc = c.ToContent();
            if( !Characters.ContainsKey(cc.ContentID))
                Characters.Add(cc.ContentID, cc);
        }

        // Monsters
     /*   Monsters = new Dictionary<string, CharacterContent>();
        foreach (var m in db.Monsters)
        {
            var mc = m.ToContent();
            if( !Monsters.ContainsKey(mc.ContentID))
                Monsters.Add(mc.ContentID, mc);
        }*/

        // Buildings
        Buildings = new Dictionary<string, BuildingContent>();
        foreach( var b in db.Buildings)
        {
            var bc = b.ToBuildingContent<BuildingContent>();
            if (!Buildings.ContainsKey(bc.ContentID))
                Buildings.Add(bc.ContentID, bc);
        }

        Items = new Dictionary<string, ItemContent>();
        Weapons = new Dictionary<string, ItemContent>();
        foreach (var i in db.Items)
        {
            // DEFENSE CARDS
            if (i.GetType() == typeof(DefenseCardAsset))
            {
                var def = (DefenseCardAsset)i;
                var elements = (Element[])Enum.GetValues(typeof(Element));
                foreach (var e in elements)
                {
                    if (e == Element.Untyped)
                        continue;
                    
                    foreach (var d in def.CreateAll(e))
                    {
                        Items.Add(d.ContentID, d);
                    }
                }
            }
            // WEAPONS
            else if (i.GetType() == typeof(WeaponAsset))
            {
                var w = ((WeaponAsset)i).ToContent();
                if (!Weapons.ContainsKey(w.ContentID))
                    Weapons.Add(w.ContentID, w);
            }

            // COMBAT CARDS
            else
            {
                var c = ((CombatCardAsset)i).ToContent();
                if (!Items.ContainsKey(c.ContentID))
                    Items.Add(c.ContentID, c);
            }           
        }

    }

    public T GetContent<T>(string contentID) where T : GameItemContent
    {
        if (CompateType(typeof(T), typeof(CharacterContent)) && Characters.ContainsKey(contentID))
            return Characters[contentID] as T;
        else if (CompateType(typeof(T), typeof(BuildingContent)) && Buildings.ContainsKey(contentID))
            return Buildings[contentID] as T;
        else if (CompateType(typeof(T), typeof(ItemContent)) && Items.ContainsKey(contentID))
        {   
            if(Items.ContainsKey(contentID))
                return Items[contentID] as T;   
            else if (Weapons.ContainsKey(contentID))
                return Weapons[contentID] as T;   
        }
 
        return default(T);
    }




    bool CompateType(System.Type type, System.Type baseType)
    {
        return type.IsSubclassOf(baseType) || type == baseType;
    }
}
