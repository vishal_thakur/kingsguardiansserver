﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[CreateAssetMenu(fileName = "NewCard", menuName = "GameContent/CombatCard", order = 5)]
public class CombatCardAsset : ItemAsset 
{
    public ItemCategory Category;
    public ActionID ActionID;
    public TargetType TargetType;

    public int Duration;
    public bool UseOnDead = false;

    public List<EffectContent> Effects = new List<EffectContent>(); 


    //Custom Inspector Properties
    public bool CardFoldout = true;

    public CombatCardContent ToContent()
    {
        CombatCardContent content = base.ToContent<CombatCardContent>();
        content.Action = ActionID;
        content.Category = Category;
        content.Target = TargetType;
        content.Duration = Duration;
        content.UseOnDead = UseOnDead;
        content.MaxStacks = MaxStacks;
        content.BuildingLevelReq = BuildingLevelReq;

        // EFFECTS
        content.Effects = new List<EffectBase>();
        foreach (var e in Effects)
        {
            Type type = System.Reflection.Assembly.GetExecutingAssembly().GetType(e.Type);
            var effect = (EffectBase)Activator.CreateInstance(type, e.Parameters);  
            content.Effects.Add(effect);
        }
        return content;
    }

}

[System.Serializable]
public class EffectContent
{
    public string Name;
    public string Type;
    public bool Always = true;

    public List<Parameter> Parameters = new List<Parameter>();

    public T GetParameter<T>(string key)
    {            
        var p = Parameters.Find(x => x.Key == key);
        if (p != null)
            return (T)p.Value;
        return default(T);
    }

    public void SetParameter(string key, object value)
    {
        var p = Parameters.Find(x => x.Key == key);
        if (p != null)
            p.Set(value);
        else
        {
            var param = new Parameter(){ Key = key};
            param.Set( value );
            Parameters.Add(param);  
        }
    }
}

[System.Serializable]
public class Parameter
{
    public string Key = null;

    public object Value
    {
        get
        {
            object value = null;
            switch (type)
            {
                case "System.Int32":
                    value = intValue;
                    break;
                case "System.Single":
                    value = floatValue;
                    break;
                case "System.String":
                    value = stringValue;
                    break;
                case "System.Boolean":
                    value = boolValue;
                    break;
                default :
                    Debug.Log("Missing field for '" + ((value != null) ? value.GetType().ToString() : "unknown") + "'");
                    break;
            }
            return value;
        }
    }


    public void Set(object value)
    {          
        if (value == null)
            return;

        type = value.GetType().ToString();
        switch (type)
        {
            case "System.Int32": intValue = (int)value; break;
            case "System.Single": floatValue = (float)value; break;
            case "System.String": stringValue = (string)value; break;
            case "System.Boolean": boolValue = (bool)value; break;
            default : Debug.Log("Missing field for '" + value.GetType().ToString()+"'"); break;
        }
    }

    [SerializeField] string type;

    [SerializeField] int intValue;
    [SerializeField] float floatValue;
    [SerializeField] string stringValue;
    [SerializeField] bool boolValue;
}