﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "NewWeapon", menuName = "GameContent/Weapon", order = 5)]
public class WeaponAsset : ItemAsset
{
    public List<SCharacterStatMod> StatBonuses = new List<SCharacterStatMod>();
    public List<SCharacterDefence> DefenceBonuses = new List<SCharacterDefence>();


    //Custom Inspector Properties
    public bool WeaponFoldout = true;

    public WeaponContent ToContent()
    {
        WeaponContent content = base.ToContent<WeaponContent>();
        content.StatBonuses = StatBonuses;
        content.DefenceBonuses = DefenceBonuses;

        return content;
    }
}
