﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "GameContentDatabase", menuName = "GameContent/Database", order = 0)]
public class GameContentAsset : ScriptableObject
{
    public List<CharacterAsset> Characters = new List<CharacterAsset>();
    public List<CharacterAsset> Monsters = new List<CharacterAsset>();
    public List<BuildingAsset> Buildings = new List<BuildingAsset>();
    public List<ItemAsset> Items = new List<ItemAsset>();
}
