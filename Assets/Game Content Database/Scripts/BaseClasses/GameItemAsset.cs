﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameItemAsset : ScriptableObject
{
    public string ID = "";
    public string Name = "";
    public string Description = "";
    public List<SMaterial> Cost = new List<SMaterial>();
    public float CraftTime = 0;

    //Custom Inspector Properties
    public bool BaseFoldout = true;


    public virtual T ToContent<T>() where T : GameItemContent
    {
        T c = (T)System.Activator.CreateInstance(typeof(T));
        c.ContentID = ID;
        c.Name = Name;
        c.Description = Description;
        c.Price = Cost;
        c.CraftTime = CraftTime;
        return c;
    }
}