﻿using UnityEngine;
using System.Collections;

public class ItemAsset : GameItemAsset
{
    public int MaxStacks = 10;
    public int BuildingLevelReq = 1;  
}
