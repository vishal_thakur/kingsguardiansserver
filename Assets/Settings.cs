﻿using System.Collections.Generic;

//Temp
public class Settings 
{


    public static float CreationTimeScale = 0.01f;

    public static int BATTLE_GOLD_REWARD = 100;
    public static int BATTLE_XP_REWARD = 100;

    public static int SQUAD_SIZE = 3;



	public static Dictionary<Currency, int> STARTING_MATERIALS = new Dictionary<Currency, int>()
	{
		{Currency.Gold, 150},          
		{Currency.Wood, 150},          

		{Currency.Stone, 0},
		{Currency.Iron, 0},
		{Currency.Diamond, 0},
		{Currency.BloodCrystal, 0},

		{Currency.Ether, 0},
	};
}
