﻿using UnityEngine;
using System.Collections;

public class DefenseEquipment : Item
{
    public override ItemCategory Category { get { return ItemCategory.Defence; }  }
    new public DefenseContent Data { get { return ServerLogic.Instance.GameContent.GetContent<DefenseContent>(ContentID); } }

    public override void Use(Character target)
    {
        if (CharacterOwner == null)
           CharacterOwner = target;
        else if (CharacterOwner == target)
            CharacterOwner = null;
        else
            Debug.LogError("The weapon already equiped by " + CharacterOwner.Data.Name + "(" + CharacterOwner.InstanceID + ") can't equip by " + target.Data.Name + "(" + target.InstanceID + ")");
    }
}
