﻿using UnityEngine;
using System.Collections;

public interface IEffect
{
    void Apply(Character target, Character caster, int duration);
    string ToString();
}                
