﻿using UnityEngine;
using System.Collections;

public interface ILevelable 
{
    int Exp { get; }
    int Level { get; }
}
