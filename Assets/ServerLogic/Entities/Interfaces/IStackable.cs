﻿using UnityEngine;
using System.Collections;

public interface IStackable
{
    int MaxStacks { get; }
}
