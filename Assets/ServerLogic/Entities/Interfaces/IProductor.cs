﻿using System.Collections.Generic;


public interface IProductor
{
    int ProductionAmount { get; }
    List<BaseProcess> Queue { get; }

    void Create(ItemContent item = null);
    void Cancel();
}
