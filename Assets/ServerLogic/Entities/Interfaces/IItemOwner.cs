﻿
public interface IItemOwner 
{
    string Identifier { get; }
}
