﻿
public interface IStorage
{
    Currency Material { get; }
    int Storage { get; }
    int StoredMaterials { get; }
}
