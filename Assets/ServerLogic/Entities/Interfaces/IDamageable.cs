﻿using UnityEngine;
using System.Collections;

public interface IDamageable 
{
    int MaxHP { get; }
    int CurrentHP { get; }
    bool Dead { get; }

    void AdjustHP(int amount);
}
