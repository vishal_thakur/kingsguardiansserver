﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasicAttack
{
    public static void Use(Character caster, Character target, bool rage = false)
    {
        bool miss = caster.ActiveStatuses.Contains(Status.Miss) && Random.Range(0, 1) <= 0.5f;
        bool concealment = target.ActiveStatuses.Contains(Status.Concealment) && Random.Range(0, 1) <= 0.5f;

        if ((miss || concealment) && Random.Range(0f, 1f) < .5f)
        {
            caster.Manager.SetMessage("Miss", CommandMessageColor.White);
            return;
        }

        int dmg = (100 * caster.Stats.GetStat(CharacterStats.Attack)) / (100 + target.Stats.GetStat(CharacterStats.Defence));
        if (rage)
            dmg = caster.UseRage(dmg);
        target.AdjustHP(-dmg);
    }



}
