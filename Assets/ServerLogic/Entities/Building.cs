﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GridSystem;
using System;

public class Building : GameItem<BuildingContent>
{  
	const Rotation DEFAULT_ROTATION = Rotation.D180;

    public bool ChangeFlag { get; set; }

    public bool Active { get; set; }
    public string WorkerID { get; set; }

    public int Shield { get; protected set; }
    public int CurrentHP { get; protected set; }

    public int Upgrade { get; protected set; }
    public List<BaseProcess> Queue { get; private set; }

	public Point Position { get; private set; }			// if one of the size component is even, the center tile is top-left tile from the center vector3 position.
	public Point Size { get; private set; }
	public Rotation Rotation { get; private set; }



    // DERIVED PROPERTIES   
    public BuildingCategory Category { get { return Data.Category; }}
    public ItemCategory ItemCategory{ get { return Data.ItemCategory; } } 
    public Currency Material { get { return Data.Material; }} 
    public List<SMaterial> MiningPrices { get 
        {
            List<SMaterial> prices = new List<SMaterial>();
            foreach (var p in Data.MiningPrices)
                prices.Add(new SMaterial(){ Material = p.Material, Amount = p.Amount * ProductionAmount });
            return Price;
        }}


    public int MaxHP { 
        get { 
            var u = Data.GetUpgrade(Upgrade);
            return (u != null) ? u.MaxHealth : 0; 
        }}    
    public float UpgradeTime{
        get {
            var u = Data.GetUpgrade(Upgrade);
            return (u != null) ? u.UpgradeTime : 0;
        }}
    public List<SMaterial> UpgradeCost{
        get {
            var u = Data.GetUpgrade(Upgrade);
            return (u != null) ? u.Cost : new List<SMaterial>();
        }}
    public int ProductionAmount { 
        get { 
            var u = Data.GetUpgrade(Upgrade);
            return (u != null) ? u.ProductionAmount : 0;
        }}
    public int Storage { 
        get { 
            var u = Data.GetUpgrade(Upgrade);
			return (u != null) ? u.Storage : 0;
        }}
    public int StoredMaterials { 
        get { 
			if (Category == BuildingCategory.Resource_generator)
				return _storedMaterials;
			
            int total = PlayerOwner.Materials.Get(Material);
            if (total >= PlayerOwner.Materials.GetStorage(Material))
                return Storage;
            return Mathf.FloorToInt(total * StorageRatio);
        }}
    public float StorageRatio { 
        get { 
            int playerStorage =  PlayerOwner.Materials.GetStorage(Material);
            if (playerStorage != 0)
                return Storage / playerStorage;
            return 0f;
        }}


	int _storedMaterials = 0;


    DateTime _lastGeneratorTick = DateTime.MinValue;

    public Building() : base()
    {
        Queue = new List<BaseProcess>();

		Position = Point.zero;
		Size = Point.zero;
		Rotation = DEFAULT_ROTATION;
    }


    #region Basic Building Functionality
    public void Replace(Point position, Rotation rotation )
    {
		Position = position;
		if (rotation != Rotation)
			Rotate(rotation);
    }

	protected void Rotate (Rotation rotation)
	{
		int r = (int)rotation + 1;
		if (r > 3) r = 0;

		if ((int)Rotation % 2 != r % 2)
			Size = new Point (Size.y, Size.x);              

		Rotation = (Rotation)r;
	}

    protected override void OnContentIDSet()
    {
        Upgrade = 1;
        CurrentHP = MaxHP;
		Size = GridHandler.FootprintSize(Data.ModelSize, GridHandler.Instance.TileSize);
    }

    public void OnConstructed()
    {
        if(Category == BuildingCategory.CharacterCreator)
            AutoCreateCharacter();

        if(Category == BuildingCategory.Resource_mine | Category == BuildingCategory.Resource_storageOnly)
            PlayerOwner.Materials.RefreshStorage();
    }

	public void AddUpgrade(int upgrade = 1) 
    { 
        int dmg = MaxHP - CurrentHP;
        Upgrade += upgrade;

        CurrentHP = MaxHP - dmg;

        if( Category == BuildingCategory.Resource_mine || Category == BuildingCategory.Resource_storageOnly)
            PlayerOwner.Materials.RefreshStorage();
       
        OnConstructed();
    }

    public BuildingUpgrade GetUpgrade()
    {
        return Data.GetUpgrade(Upgrade);
    }


    public void AdjustHP(int amount)
    {
        int tempHP = CurrentHP;
        int tempShield = Shield;

        // If the building takes damage and it has active shield
        if (amount < 0 && Shield > 0)
        {
            int diff = amount + Shield;
            if (diff > 0)
            {
                amount = 0;
                Shield = diff;
            }
            else
            {
                Shield = 0;
                amount = diff;
            }
        }
                            
        CurrentHP = Mathf.Clamp(CurrentHP + amount, 0,  MaxHP);

        // Set the change flag to true if the hp or the shield is changed!
        if (tempHP != CurrentHP || tempShield != Shield)
            ChangeFlag = true;
    }

    public virtual void AdjustShield (int amount)
    {
        Shield = amount;
    }
    #endregion

	void AutoCreateCharacter()
    {
        if (Category == BuildingCategory.CharacterCreator)
        {
            foreach (var pair in ServerLogic.Instance.GameContent.Characters)
            {
                if (pair.Value.BuildingLevelReq == Upgrade)
                    Create(pair.Value); 
            }
        }     
    }

    public void Create(GameItemContent item = null)
    {
        if (!Active)
            return;

        switch (Category)
        {
            case BuildingCategory.Resource_mine:              
                if (MiningPrices.Count == 0 || PlayerOwner.Materials.Subtract(MiningPrices))
                {
                    MineProcess mine = new MineProcess(PlayerOwner, this, ((Category == BuildingCategory.Resource_generator) ? new List<SMaterial>() : MiningPrices), ProductionAmount);
                    if (Queue.Count == 0)
                        mine.Start(Data.ProductionTime);
                    Queue.Add(mine);     
                }
                break;

            case BuildingCategory.CardCreator:
                if (item != null)
                {
                    var i = (ItemContent)item;
                    if (i.Category == ItemCategory && i.BuildingLevelReq <= Upgrade && PlayerOwner.Materials.Subtract(item.Price))
                    {
                        CraftProcess craft = new CraftProcess(PlayerOwner, item, ProductionAmount);
                        if (Queue.Count == 0)
                            craft.Start(item.CraftTime);
                        Queue.Add(craft);     
                     }
                }
                break;

            case BuildingCategory.CharacterCreator:
                if (item != null && item.GetType() == typeof(CharacterContent) && Upgrade > PlayerOwner.Characters.Count)
                {
                    CraftProcess craft = new CraftProcess(PlayerOwner, item, ProductionAmount);
                    if (Queue.Count == 0)
                        craft.Start(item.CraftTime);
                    Queue.Add(craft);      
                }
                 
                break;

            case BuildingCategory.WorkerCreator:
                
                break;
        }
    }

    protected override void Update()
    {
        if (PlayerOwner == null || !PlayerOwner.IsConnected) return;

        if (Category == BuildingCategory.Resource_generator)
			HandleGenerator();
        else if (Active && Queue != null && Queue.Count > 0 && Queue[0].TimeLeft == 0 && Queue[0].Finish())
        {    
            Queue.RemoveAt(0);
            switch (Category)
            {             
                case BuildingCategory.Resource_mine:              
                    if (Queue.Count > 0)
                        Queue[0].Start(Data.ProductionTime);
                    TownHandler.MineDone(PlayerOwner, this);
                    break;

                case BuildingCategory.CardCreator:
                case BuildingCategory.CharacterCreator:
                    if (Queue.Count > 0)
                        Queue[0].Start(((CraftProcess)Queue[0]).Item.CraftTime);
                    TownHandler.CraftDone(PlayerOwner, this);
                    break;
            }         
        }
    }

    public void Cancel()
    {
        if (Queue != null && Queue.Count > 0)
        {
            Queue[0].Cancel();
            Queue.RemoveAt(0);

            if (Queue.Count > 0)
            {
                switch (Category)
                {
                    case BuildingCategory.Resource_mine:
                        Queue[0].Start(Data.CraftTime);
                        break;

                    case BuildingCategory.CardCreator:
                    case BuildingCategory.CharacterCreator:
                        Queue[0].Start(((CraftProcess)Queue[0]).Item.CraftTime);
                        break;
                }               
            }               
        }
    } 

	public bool Collect()
	{
		if (PlayerOwner.Materials.Get(Material) == PlayerOwner.Materials.GetStorage(Material))
			return false;			
		_storedMaterials = PlayerOwner.Materials.Add(Material,  _storedMaterials);
		return true;
	}

    bool HandleGenerator()
    {
		// Don't do anything if the storage is full, and reset the _lastGeneratorTick
		if (PlayerOwner.Materials.Get(Material) == PlayerOwner.Materials.GetStorage(Material))
        {
            _lastGeneratorTick = DateTime.MinValue;       
            return false;
        }

        // Grab the current system time.
        var now = System.DateTime.Now;

        // If the building didn't generated in the last time (becouse freshly made, or the storage was full)
        if (_lastGeneratorTick == DateTime.MinValue)
        {
			_storedMaterials += ProductionAmount;
			// PlayerOwner.Materials.Add(Material,  ProductionAmount); // This row directly adds to the player storage.
            _lastGeneratorTick = now;
            return true;
        }
       

        float timePerTick = Data.ProductionTime * Settings.CreationTimeScale;
        var nextTick = _lastGeneratorTick.AddSeconds((double)timePerTick);
        if (now.Ticks > nextTick.Ticks)
        {
            // Calculate seconds
            long elapsedTicks = now.Ticks - _lastGeneratorTick.Ticks;
            System.TimeSpan elapsedSpan = new System.TimeSpan(elapsedTicks);
            float difference = (float)elapsedSpan.TotalSeconds;

            int ticks = Mathf.FloorToInt(difference / timePerTick);
            if (ticks > 0 )
            { 
				_storedMaterials += ProductionAmount * ticks;
				//PlayerOwner.Materials.Add(Material, ProductionAmount * ticks); // This row directly adds to the player storage.
                _lastGeneratorTick = _lastGeneratorTick.AddSeconds((double)((float)ticks * timePerTick));
                return true;                            
            } 
        } 
        return false;
    }


    public override BaseData Save()
    {
        BuildingData data = new BuildingData();
        data.Active = Active;
        data.ContentID = ContentID;
        data.InstanceID = InstanceID;
        data.Position = Position.ToString();
        data.Rotation = ((int)Rotation).ToString();
        data.Upgrade = Upgrade;
        data.HP = CurrentHP;
		data.StoredMaterials = _storedMaterials;

        if (Category == BuildingCategory.Resource_mine || Category == BuildingCategory.CardCreator || Category == BuildingCategory.CharacterCreator)
        {
            foreach (var q in Queue)
                data.Queue.Add(q.Serialize());
        }
        data.LastGeneratorTick = _lastGeneratorTick.ToString(ServerLogic.DATE_TIME_FORMAT);


        return data;
    }

    public override bool Load(BaseData data)
    {
        if (data.GetType() != typeof(BuildingData))
        {
            Debug.LogError("Wrong type of data for Item. " + data.GetType().ToString());
            return false;                
        }
        base.Load(data);

        BuildingData d = (BuildingData)data;
        Active = d.Active;
		Position = Point.Parse(d.Position);
		Rotation = (Rotation)int.Parse(d.Rotation);

        Upgrade = d.Upgrade;
        CurrentHP = d.HP;

        if (Category == BuildingCategory.Resource_mine || Category == BuildingCategory.CardCreator || Category == BuildingCategory.CharacterCreator)
        {
            Queue = new List<BaseProcess>();
            foreach (var q in ((BuildingData)data).Queue)
            {
                BaseProcess p = null;
                if (Category == BuildingCategory.Resource_mine)
                    p = new MineProcess(PlayerOwner, this, null, 0);
                else
                    p = new CraftProcess(PlayerOwner, null, 0);
                
                p.Deserialize(q);
                Queue.Add(p);
            }
        }

		_storedMaterials = d.StoredMaterials;
        _lastGeneratorTick = DateTime.Parse(d.LastGeneratorTick);

        return true;
    }

    public override C ToClient<C>()
    {
        if( !Utils.IsInherited( typeof(C), typeof(BuildingInfo)))
            return null;

       if (Category == BuildingCategory.Resource_generator && Active)
            HandleGenerator();

        BuildingInfo info = base.ToClient<C>() as BuildingInfo;
        info.Active = Active;
        info.CurrentHP = CurrentHP;
        info.Position = Position.ToString();
        info.Rotation = ((int)Rotation).ToString();
        info.FootprintSize = Size.ToString();
        info.Upgrade = Upgrade;

		if (Category == BuildingCategory.Resource_mine || Category == BuildingCategory.CardCreator || Category == BuildingCategory.CharacterCreator)
		{
			info.StoredMaterials = StoredMaterials;
			info.Queue = new List<QueueInfo>();

			foreach (var q in Queue)
				info.Queue.Add(q.ToClient());
		}
		else
			info.StoredMaterials = _storedMaterials;
        return info as C;
    }
}



