﻿using UnityEngine;
using System.Collections;

public class WeaponEquipment : Item
{
    public override ItemCategory Category { get { return ItemCategory.Weapon; }  }
    new public WeaponContent Data { get { return ServerLogic.Instance.GameContent.GetContent<WeaponContent>(ContentID); } }

    public override void Use(Character target)
    {
        if (CharacterOwner == null)
        {
            target.Stats.AddEquipmentModifier(Data.StatBonuses, InstanceID);
            CharacterOwner = target;
        }
        else if (CharacterOwner == target)
        {
            target.Stats.RemoveEquipmentModifier(InstanceID);
            CharacterOwner = null;
        }
        else
            Debug.LogError("The weapon already equiped by " + CharacterOwner.Data.Name + "(" + CharacterOwner.InstanceID + ") can't equip by " + target.Data.Name + "(" + target.InstanceID + ")");
    }
}