﻿using UnityEngine;
using System.Collections;

public class Item : GameItem<ItemContent>, IStackable
{
    public virtual ItemCategory Category { get { return ItemCategory.Weapon; }}
    public Character CharacterOwner { get; protected set; }
    public int MaxStacks { get { return Data.MaxStacks; } }

    public override void SetOwner(IItemOwner owner)
    {
        if (owner.GetType() == typeof(Character))
        {
            Character c = (Character)owner;
            CharacterOwner = c;
        }
        else
            base.SetOwner(owner);
    }

    public override void RemoveOwner()
    {
        CharacterOwner = null;
    }


    public virtual void Use( Character target )
    {
        
    }

    public override BaseData Save()
    {
        ItemData data = new ItemData();
        data.ContentID = ContentID;
        data.InstanceID = InstanceID;
        data.Category = Category;
        return data;
    }

    public override bool Load(BaseData data)
    {
        if (data.GetType() != typeof(ItemData))
        {
            Debug.LogError("Wrong type of data for Item. " + data.GetType().ToString());
            return false;                
        }
        base.Load(data);
        return true;
    }

    public override C ToClient<C>()
    {
        if( !Utils.IsInherited( typeof(C), typeof(ItemInfo)))
            return null;

        ItemInfo info = base.ToClient<C>() as ItemInfo;
        return info as C;
    }
}
