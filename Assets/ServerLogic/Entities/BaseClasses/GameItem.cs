﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameItem<T> : GameItem where T : GameItemContent
{ 
    public T Data { get { return ServerLogic.Instance.GameContent.GetContent<T>(ContentID); } }   
    public List<SMaterial> Price { get { return Data.Price; }}   
}

public class GameItem
{
	protected virtual string InstanceIDSuffix{ get { return ""; } }

    public string InstanceID { get; protected set; }
    public string ContentID { get; private set; }
    public Player PlayerOwner { get; protected set; }

	public string Note { get; set; }

    public GameItem()
    {
        ServerLogic.Instance.UpdateDelegates += Update;     
    }

    public virtual void SetOwner(IItemOwner owner)
    {
        if( owner.GetType() == typeof(Player))
        {
            Player p = (Player)owner;
            PlayerOwner = p;

            if( InstanceID == null || InstanceID == "")
                InstanceID = GenerateInstanceID(ContentID);
        }
    }
    public virtual void RemoveOwner(){}


    public void SetContentID(string contentID)
    {
        ContentID = contentID;
        OnContentIDSet();
    }


    protected virtual void OnContentIDSet(){ }

    public virtual BaseData Save()
    {
        return new BaseData(){ ContentID = ContentID, InstanceID = InstanceID };
    }

    public virtual bool Load(BaseData data)
    {
        ContentID = data.ContentID;
        InstanceID = data.InstanceID;
        return true;
    }  

    public virtual C ToClient<C>() where C : BaseInfo
    {
        C info = System.Activator.CreateInstance<C>();
        info.ContentID = ContentID;
        info.InstanceID = InstanceID;
        return info;
    }


    protected virtual void Update() { }

    string GenerateInstanceID( string baseID)
    {
        if (PlayerOwner == null)
            Debug.LogError("Can't generate instance id without giving a player that is owned the item");

        string dateTime = System.DateTime.Now.Ticks.ToString();
        return baseID + "" + PlayerOwner.Username + "" + dateTime + "" +InstanceIDSuffix;
    }



    public virtual bool IsTypeof( System.Type type)
    {
        return GetType() == type || GetType().IsInstanceOfType(type);
    }
}