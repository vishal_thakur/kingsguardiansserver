﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCurrencies 
{
    Dictionary<Currency, int> _storages;
    Dictionary<Currency, int> _materials;

    Player _player;

    public bool UpdateClient { get { return _lastChanged > _lastClientUpdate;}}
    float _lastChanged = -1;
    float _lastClientUpdate = -1;

    public PlayerCurrencies(Player player)
    {
        _player = player;
        _materials = new Dictionary<Currency, int>();
        _storages = new Dictionary<Currency, int>();

		foreach (var m in Settings.STARTING_MATERIALS)
		{
			if (_materials.ContainsKey(m.Key))
				_materials[m.Key] = m.Value;
			else
				_materials.Add(m.Key, m.Value);
		}
    }

    /// <summary>
    /// Get the specified material.
    /// </summary>
    /// <param name="material">Material.</param>
    public int Get(Currency material)
    {
        if (_materials.ContainsKey(material))
            return _materials[material];
        return 0;
    }

    #region Storage

    /// <summary>
    /// Refreshs the storage.
    /// </summary>
    public void RefreshStorage()
    {
        _storages.Clear();
        foreach (var b in _player.Buildings.Items)
        {
            if (b.Category == BuildingCategory.Resource_mine || b.Category == BuildingCategory.Resource_storageOnly)
            {
                if (_storages.ContainsKey(b.Material))
                    _storages[b.Material] += b.Storage;
                else 
                    _storages.Add(b.Material, b.Storage);
            }    
        }

        _lastChanged = Time.time;
    }

    /// <summary>
    /// Gets the storage.
    /// Zero means there is no maximum set.
    /// </summary>
    /// <returns>The storage.</returns>
    /// <param name="material">Material.</param>
    public int GetStorage(Currency material)
    {  
        if (_storages.ContainsKey(material))
            return _storages[material];
        return 0;
    }
    #endregion

    #region ADD
    /// <summary>
    /// Add the specified amount of material. If the total number of material exceed the maximum storage
    /// the rest will be returned by the method.
    /// </summary>
    /// <param name="material">Material.</param>
    /// <param name="amount">Amount.</param>
    public int Add( Currency material, int amount, bool noStorageCheck = false )
    {
        int total = Get(material);
        int storage = GetStorage(material);
        int rest = 0;

        if (!noStorageCheck && storage > 0 && storage < total + amount )
            rest = total + amount - storage;
         
        amount -= rest;

        if (_materials.ContainsKey(material))
            _materials[material] += amount;
        else
            _materials.Add(material, amount);

        _lastChanged = Time.time;

        return rest;
    }

    /// <summary>
    /// Add the specified amount of materials. If the total number of materials exceed the maximum storage
    /// the rest will be returned by the method.
    /// </summary>
    /// <param name="materials">Materials.</param>
    public List<SMaterial> Add(List<SMaterial> materials)
    {
        for (int i = 0; i < materials.Count; i++)
            materials[i].Amount = Add(materials[i].Material, materials[i].Amount);
        materials.RemoveAll(x => x.Amount == 0);

        _lastChanged = Time.time;

        return materials;
    }
    #endregion



    #region HAS
    public bool Has(Currency material, int amount)
    {
        return Get(material) >= amount;
    }

    public bool Has(List<SMaterial> materials)
    {
        foreach (var m in materials)
            if (!Has(m.Material, m.Amount)) return false;
        
        return true;
    }
    #endregion


    #region SUBTRACT
    public bool Subtract(Currency material, int amount)
    {
        if (!Has(material, amount)) return false; 

        _materials[material] -= amount;

        _lastChanged = Time.time;
        return true;     
    }

    public bool Subtract(List<SMaterial> materials)
    {
        Debug.Log("Try to subtract materials");
        if (!Has(materials)) return false;

        foreach (var m in materials)
        {
            Debug.Log(" - " + m.Amount + " " + m.Material+"\n");
            _materials[m.Material] -= m.Amount;
        }

        _lastChanged = Time.time;
        return true;
    }
    #endregion



    public List<SMaterial> Save()
    {
        List<SMaterial> list = new List<SMaterial>();
        foreach (var p in _materials)
            list.Add(new SMaterial(){ Material = p.Key, Amount = p.Value });

        _lastClientUpdate = Time.time;

        return list;
    }

    public void Load(List<SMaterial> list)
    {
        Add(list);
    }
}
