﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StatHolder 
{
    public List<SCharacterStat> BaseStats { get; private set; }

    Character _character;
    Dictionary<CharacterStats, Stat> _stats = new Dictionary<CharacterStats, Stat>();

    public StatHolder(Character character)
    {
        _character = character;
        foreach (var stat in (CharacterStats[])Enum.GetValues(typeof(CharacterStats)))
            _stats.Add(stat, new Stat(stat));   
    }

    public int GetStat(CharacterStats stat)
    {
        if (_stats.ContainsKey(stat))
            return _stats[stat].Value;
        return 0;
    }


    /// <summary>
    /// Adds an upgrade to a single stat.
    /// </summary>
    /// <param name="stat">Stat.</param>
    /// <param name="level">Level.</param>
    /// <param name="amount">Amount.</param>
    public void AddUpgrade(CharacterStats stat, int amount)
    {
        if (_stats.ContainsKey(stat) && _character.Level > _stats[stat].Upgrades)
            _stats[stat].AddModifier(ModifierType.Upgrade, "upgrade on " + _stats[stat].Upgrades+1, amount);
    }     



    /// <summary>
    /// Adds certain amount of equipment modifiers to a given stats.
    /// </summary>
    /// <param name="stats">Stats.</param>
    /// <param name="equipmentID">Equipment ID.</param>
    public void AddEquipmentModifier( List<SCharacterStatMod> stats, string equipmentID)
    {
        foreach (var p in stats)
        {
            if (_stats.ContainsKey(p.Stat))
                _stats[p.Stat].AddModifier(ModifierType.Equipment, equipmentID, p.Value);
        }
    }

    /// <summary>
    /// Removes all the equipment modifier with the given id.
    /// </summary>
    /// <param name="equipmentID">Equipment ID.</param>
    public void RemoveEquipmentModifier( string equipmentID )
    {
        foreach (var p in _stats)
            p.Value.RemoveModifier(ModifierType.Equipment, equipmentID);
    }

    /// <summary>
    /// Adds magic modifiers to the given stats.
    /// </summary>
    /// <param name="stats">Stats.</param>
    /// <param name="cardID">Card I.</param>
    public void AddMagicModifier( CharacterStats stat, float mod, string cardID)
    {
        if (_stats.ContainsKey(stat))
            _stats[stat].AddModifier(ModifierType.Magic, cardID, mod);
    }

    /// <summary>
    /// Adds magic modifiers to the given stats.
    /// </summary>
    /// <param name="stats">Stats.</param>
    /// <param name="cardID">Card I.</param>
    public void AddMagicModifier( List<SCharacterStat> stats, string cardID)
    {
        foreach (var p in stats)
        {
            if (_stats.ContainsKey(p.Stat))
                _stats[p.Stat].AddModifier(ModifierType.Magic, cardID, p.Value);
       }
    }
  
    /// <summary>
    /// Removes all the magic modifier with the given id.
    /// </summary>
    /// <param name="cardID">Card ID.</param>
    public void RemoveMagicModifier(string cardID)
    {
        foreach (var p in _stats)
            p.Value.RemoveModifier(ModifierType.Magic, cardID);
    }

    /// <summary>
    /// Removes all magic modifiers from all stats.
    /// </summary>
    public void RemoveMagicModifier()
    {
        foreach (var p in _stats)
            p.Value.RemoveModifier(ModifierType.Magic);
    }


    public void ReadBaseStats(List<SCharacterStat> stats)
    {
        BaseStats = new List<SCharacterStat>();
        foreach (var s in stats)
        {
            if (_stats.ContainsKey(s.Stat))
            {
                _stats[s.Stat].SetBaseValue(s.Value);
                BaseStats.Add(new SCharacterStat(){ Stat = s.Stat, Value = s.Value });
            }
        }
    }


    public List<StatUpgradeData> Save()
    {
        var list = new List<StatUpgradeData>();
        foreach (var p in _stats)
            list.Add(new StatUpgradeData(){StatEnum = (int)p.Value.Name, Upgrades = p.Value.SerializeUpgrades() });
        return list;
    }

    public void Load(List<StatUpgradeData> data)
    {
        foreach (var d in data)
        {
            var stat = (CharacterStats)d.StatEnum;
            if (_stats.ContainsKey(stat))
                _stats[stat].DeserializeUpgrades(d.Upgrades);
        }
    }

    public List<SCharacterStat> ToClient()
    {
        var list = new List<SCharacterStat>();
        foreach (var p in _stats)
            list.Add(new SCharacterStat(){Stat = p.Value.Name, Value = p.Value.Value });
        return list;
    }
	
}
