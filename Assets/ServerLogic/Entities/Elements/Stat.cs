﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum ModifierType : int { Upgrade = 0, Equipment = 1, Magic = 2 }

public class Stat 
{
    public CharacterStats Name { get; private set; }
    public int Value{
        get 
        {
            int value = _baseValue;
            for (int i = 0; i < _modifiers.Length; i++)
                _modifiers[i].Apply(ref value);
            return value; 
        }
    } 

    public int Upgrades { get { return _modifiers[(int)ModifierType.Upgrade].Count; }}

    int _baseValue = 0;
    Modifier[] _modifiers;

    public Stat(CharacterStats stat) 
    { 
        Name = stat; 
        _modifiers = new Modifier[Enum.GetNames(typeof(ModifierType)).Length];
        for (int i = 0; i < _modifiers.Length; i++)
        {
            if (_modifiers[i] == null)
                _modifiers[i] = new Modifier();
            _modifiers[i].Type = (ModifierType)i;
        }
    }
    public void SetBaseValue(int value) { _baseValue = value; }

    public void AddModifier(ModifierType type, string id, float amount)
    {
        _modifiers[(int)type].Add(id, amount);
    }

    public void RemoveModifier(ModifierType type, string id)
    {
        _modifiers[(int)type].Remove(id);
    }

    public void RemoveModifier(ModifierType type, List<string> ids)
    {
        foreach( var id in ids)
            _modifiers[(int)type].Remove(id);
    }

    public void RemoveModifier(ModifierType type)
    {
        _modifiers[(int)type].Clear();
    }

    class Modifier
    {
        public ModifierType Type;
        public int Count{ get { return _modifiers.Count; }}

        Dictionary<string, float> _modifiers = new Dictionary<string, float>();

        public void Apply(ref int value)
        {
            float multiplier = 0;
            foreach (var p in _modifiers)
            {
                if (p.Value < 1 && p.Value > multiplier)
                    multiplier = p.Value;

                else if (p.Value != 1)
                    value += (int)p.Value;
            }

            if(multiplier != 0)
                value = Mathf.RoundToInt((float)value * (1f + multiplier));
        }

        public void Add( string id, float amount)
        {
            if (!_modifiers.ContainsKey(id))
                _modifiers.Add(id, amount);      
        }

        public void Remove(string id)
        {
            if (!_modifiers.ContainsKey(id))
                _modifiers.Remove(id);
        }

        public void Clear() { _modifiers.Clear(); }

        public List<StatModData> GetModifiers()
        {
            List<StatModData> list = new List<StatModData>();
            foreach (var p in _modifiers)
                list.Add(new StatModData(){ ID = p.Key, Value = Mathf.RoundToInt(p.Value) });
            return list;
        }
    }

    public List<StatModData> SerializeUpgrades()
    {
        return _modifiers[(int)ModifierType.Upgrade].GetModifiers();
    }

    public void DeserializeUpgrades(List<StatModData> data)
    {
        _modifiers[(int)ModifierType.Upgrade].Clear();
        foreach (var d in data)
            _modifiers[(int)ModifierType.Upgrade].Add(d.ID, d.Value);
    }

}   