﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Inventory <T> where T : GameItem, new()
{
    public bool IgnoreStacks = false;
    public int MaxSlot { get; set; }                // 0 : unlimited
    public List<T> Items { get; private set; }
    public bool FreeSlot { get { return MaxSlot == 0 || MaxSlot > Items.Count; } }

    IItemOwner _owner;
    List<int> _stacks = new List<int>();

    public Inventory(IItemOwner owner)
    {
        MaxSlot = 0;
        Items = new List<T>(); 
        _owner = owner;
    }

    public T GetByInstanceID(string instanceID)
    {
        T item = Items.Find(x => x.InstanceID == instanceID);
        return item;
    }

    public T Get(string contentID)
    {
        T item = Items.Find(x => x.ContentID == contentID);
        return item;
    }

    public List<T> Get(List<System.Type> type)
    {
        List<T> list = new List<T>();
        foreach (var t in type)
        {
            list.AddRange(Get(t));
        }
        return list;
    }

    public List<T> Get(System.Type type)
    {
        List<T> list = new List<T>();
        foreach (var item in Items)
        {
            if (item != null && item.IsTypeof(type))
                list.Add(item);
        }
        return list;
    }

    public int Stacks(string contentID)
    {
        return Stacks(GetIndex(contentID));
    }

    public int Stacks(int slotIndex)
    {
        if (slotIndex >= 0 && slotIndex < _stacks.Count)
            return _stacks[slotIndex];
        return 0;
    }

    public void Add (T item )
    {
        Add(item, -1);
    }

    public void Add(T item, int slot, int stacks = 1)
    {          
        if (item == null /*|| item.ContentID == null*/ || stacks < -1)
            return;

        var stackable = item as IStackable;

        if (stackable != null)
        {
            for (int cnt = 0; cnt < Items.Count; cnt++)
            {
                if (Items[cnt] != null && Items[cnt].ContentID == item.ContentID)
                {
                    Debug.Log("Adding " + item.ContentID + " (" + stacks + "). Total stacks: "+_stacks[cnt] +" of "+ stackable.MaxStacks );

                    if (IgnoreStacks || stackable.MaxStacks >= _stacks[cnt] + stacks)
                    {
                        _stacks[cnt] += stacks;
                        stacks = 0;
                    }
                    else
                    {
                        int addingStacks = stackable.MaxStacks - _stacks[cnt];
                        _stacks[cnt] += addingStacks;
                        stacks -= addingStacks;   
                    }

                }
            }
        }
               
        if (stacks > 0 && (MaxSlot == 0 || Items.FindAll(x => x != null).Count < MaxSlot))
        {
            if (slot < 0 || slot >= Items.Count || Items[slot] == null)
                slot = GetIndexFirstEmpty(); 
           
            item.SetOwner(_owner);

            if (slot < 0)
            {
                Items.Add(item);
                _stacks.Add(stacks);
            }
            else
            {
                Items[slot] = item;
                _stacks[slot] = stacks;
            }

            if (Items.Count != _stacks.Count)
                Debug.LogError("Elbaaasztaaam az inventoryt! Különböző az items count és a stacks count!");
        }
    }

    public void Remove(int slotIndex)
    {
        if (slotIndex >= 0 && slotIndex < Items.Count)
        {
            _stacks[slotIndex]--;
            if (_stacks[slotIndex] == 0)
            {
                Items[slotIndex].RemoveOwner();
                if (IgnoreStacks)
                {
                    Items.RemoveAt(slotIndex);
                    _stacks.RemoveAt(slotIndex);
                }
                else
                    Items[slotIndex] = null;
            }
        }
    }

	public void RemoveByInstanceID(string instanceID)
	{
		Remove(GetIndexByInstanceID(instanceID));    
	}

    public void Remove(string contentID)
    {
        Remove(GetIndex(contentID));    
    }

    public void Remove(IEnumerable<string> items)
    {
        foreach (var item in items)
            Remove(item);
    }

    public void Clear()
    {
        Items.Clear();
        _stacks.Clear();
    }
       
    int GetIndexFirstEmpty()
    {
        for (int cnt = 0; cnt < Items.Count; cnt++)
        {
            if (Items[cnt] == null)
                return cnt;
        }  
        return -1;
    }

    int GetIndex(string contentID)
    {
        for (int cnt = 0; cnt < Items.Count; cnt++)
        {
            if (Items[cnt].ContentID == contentID)
                return cnt;
        }  
        return -1;
    }

	int GetIndexByInstanceID(string instanceID)
	{
		for (int cnt = 0; cnt < Items.Count; cnt++)
		{
			if (Items[cnt].InstanceID == instanceID)
				return cnt;
		}  
		return -1;
	}

    public delegate void OnChanged();
    public OnChanged onListChanged;




    public List<D> Save<D>() where D : BaseData
    {
        if (typeof(D) == typeof(ItemData) && typeof(T) == typeof(Building) ||
            typeof(D) == typeof(BuildingData) && typeof(T) != typeof(Building))
            Debug.LogError("Invalid data type for serialization.\nItem type: " + typeof(T).ToString() + ", and the data type: " + typeof(D).ToString());

        List<D> list = new List<D>();
        for (int cnt = 0; cnt < Items.Count; cnt++)
        {
            if (Items[cnt] != null)
            {
                var i = (D)Items[cnt].Save();
                if (typeof(D) == typeof(ItemData))
                    (i as ItemData).Stacks = _stacks[cnt];
                list.Add(i);
            }
            else
                list.Add(null);
        }     
        return list;
    }

    public void Load<D>(List<D> list) where D : BaseData
    {
        if (typeof(D) == typeof(ItemData) && typeof(T) == typeof(Building) ||
            typeof(D) == typeof(BuildingData) && typeof(T) != typeof(Building))
            Debug.LogError("Invalid data type for serialization.\nItem type: " + typeof(T).ToString() + ", and the data type: " + typeof(D).ToString());

        Clear();

        if (typeof(D) == typeof(ItemData) )
        {
            foreach (var i in list)
            { 
                if (i == null)
                {
                    Add(null);
                    continue;
                }                            

                var itemData = i as ItemData;
                switch (itemData.Category)
                {
                    case ItemCategory.Defence:
                        var defence = new DefenseEquipment();
                        defence.SetOwner(_owner);
                        defence.Load(i); 
                        Add(defence as T, -1, itemData.Stacks);                                       
                        break;
                    case ItemCategory.Weapon:
                        var weapon = new WeaponEquipment();
                        weapon.SetOwner(_owner);
                        weapon.Load(i);
                        Add(weapon as T, -1, itemData.Stacks);
                        break;
                    default:
                        var card = new CombatCard();
                        card.SetOwner(_owner);
                        card.Load(i);
                        Add(card as T, -1, itemData.Stacks);
                        break;
                }                   
            }   
        }
        else
        {
            foreach (var i in list)
            {        
				if (i == null)
					continue;
				
                var item = new T();
                item.SetOwner(_owner);
                item.Load(i);               
                Add(item);

            }   
        }       
    }

    public List<C> ToClient<C>() where C : BaseInfo
    {
        var list = new List<C>();
        bool stacking = typeof(C) == typeof(ItemInfo) || typeof(C).IsSubclassOf(typeof(ItemInfo));

        for (int cnt = 0; cnt < Items.Count; cnt++)
        {
            if (Items[cnt] != null)
            {
                var i = Items[cnt].ToClient<C>();
                if (stacking)
                    (i as ItemInfo).Stacks = _stacks[cnt];
                list.Add(i);
            }
            else
                list.Add(null);                          
        }     
        return list;
    }
}
