﻿using UnityEngine;
using System.Collections;

public class Worker : GameItem<GameItemContent>
{
    public WorkerType Type
	{
		get { return _type; }
		set { _type = value; SetContentID(_type.ToString()); }
	}
    public bool Free { get { return ActiveWork == null; } }
    public Work ActiveWork { get; private set; }


	protected override string InstanceIDSuffix 
	{
		get 
		{ 
			if (PlayerOwner == null)
				return "_none";
			if (PlayerOwner.WorkerNumber == 0)
				return "_1";
			return "_" + (PlayerOwner.WorkerNumber + 1);

		} 
	}


	WorkerType _type = WorkerType.Worker;

	public Worker() : base()
	{
		SetContentID(_type.ToString());
	}


    public void AssignWork(Building building, float duration, Works work)
    {
        ActiveWork = new Work(building, duration, work);
        building.WorkerID = InstanceID;
    }

    public void Cancel()
    {
        ActiveWork.Cancel();
        ActiveWork = null;
    }

    protected override void Update()
    {
        if (ActiveWork == null) return;

        if (ActiveWork.TimeLeft == 0)
        {
            if (ActiveWork.Finish())
            {
                Building b = ActiveWork.Building;
                ActiveWork = null;

				//If player is connected then Intute him ingame about upgrade completition  
				if (PlayerOwner.IsConnected)
					TownHandler.WorkerJobDone (PlayerOwner, b, this);
				else {//If player is not connected then send him push notification Instead
					FirebaseNotificationManager.Instance.SendPushNotification(PlayerOwner , "Hey " + PlayerOwner.Username ,b.Category.ToString() +" Upgrade Complete");
					Debug.LogError ("kam to poora hai pr bande na jane kahan gand marvaye rahe bsdk !\t" + PlayerOwner.Username + "\t" + b.Category.ToString ());
				}
            }
        }
    }


    new public WorkerData Save()
    {
        WorkerData data = new WorkerData();
        data.ContentID = ContentID;
        data.InstanceID = InstanceID;
        data.Type = Type;

        if (ActiveWork != null)
            data.BuildingWorkingOn = ActiveWork.Serialize();
        return data;
    }

    public void Load(WorkerData data)
    {
        base.Load(data);
        Type = data.Type;
        if (data.BuildingWorkingOn != null)
        {
            ActiveWork = new Work(PlayerOwner);
            ActiveWork.Deserialize(data.BuildingWorkingOn);
            if (ActiveWork.Building == null)
                ActiveWork = null;
            else
                ActiveWork.Building.WorkerID = InstanceID;
        }
    }

    public override C ToClient<C>()
    {
        if( !Utils.IsInherited( typeof(C), typeof(WorkerInfo)))
            return null;

        WorkerInfo info = base.ToClient<C>() as WorkerInfo;
        info.Type = Type;
        if (ActiveWork != null)
        {
            info.ActiveWork = ActiveWork.ToClient();
            info.BuildingInstanceID = ActiveWork.Building.InstanceID;
        }
        return info as C;
    }
}
