﻿using UnityEngine;
using System.Collections.Generic;

public class Character : GameItem<CharacterContent>, ILevelable, IDamageable, IItemOwner
{
    #region IItemOwner implementation
    public string Identifier{ get { return InstanceID; }}
    #endregion

    
    #region Battle
    public BattleControll ControledBy { get; set; }
    public float Readiness { get; set; }
    public BattleManager Manager;
    #endregion

    #region Raid
    public float AttackSpeed = 0.3f;
    public float MovementSpeed { get { return Data.MovementSpeed * 5f; } }    
    public int AttackRange { get { return Data.AttackRange; } } 
    #endregion



    public int Shield { get; set; }
    public int CurrentHP { get; private set; }
    public float Rage { get; private set; } 
    public int Exp { get; private set; }

    public int Level { get { return Calculators.CalculateLevel(Exp); } }
    public int MaxHP { get { return Stats.GetStat(CharacterStats.Health); }}
    public bool Dead { get { return CurrentHP == 0; } }

    public bool IsStunned { get { return ActiveStatuses.Contains(Status.Stun); } }

    public StatHolder Stats;
    public WeaponEquipment Weapon;
    public Inventory<DefenseEquipment> Defences;
    public Inventory<CombatCard> Inventory;

    public List<Status> ActiveStatuses = new List<Status>();

    public Character (string contentID)
    {      
        SetContentID(contentID);

        Defences = new Inventory<DefenseEquipment>(this);
        Defences.MaxSlot = 3;
        Inventory = new Inventory<CombatCard>(this);
        Stats = new StatHolder(this);
        Rage = 0;    

        Stats.ReadBaseStats(Data.BaseStats);
    }

    public bool Equip(Item item, int slot = -1)
    {
        bool added = false; 

        if (Utils.IsInherited(item.GetType(), typeof(CombatCard)) && Inventory.FreeSlot)
        {
            Inventory.Add((CombatCard)item, slot);
            added = true;
        }
        else if (Utils.IsInherited(item.GetType(), typeof(DefenseEquipment)) && Defences.FreeSlot)
        {
            Defences.Add((DefenseEquipment)item, slot);
            added = true;
        }
        else if (Utils.IsInherited(item.GetType(), typeof(WeaponEquipment)) && Weapon == null)
        {
            Weapon = (WeaponEquipment)item;
            added = true;
        }       

        if (added == true)
            item.Use(this);

        return added;
    }

    public Item Unequip(string itemID)
    {
        Item item;
        if (Weapon != null && Weapon.InstanceID == itemID)
        {
            item = Weapon;
            Weapon = null;
        }
        else
        {
            item = Defences.Get(itemID);
            if (item != null)
                Defences.Remove(itemID);
            else
            {
                item = Inventory.Get(itemID);
                if (item != null)
                    Inventory.Remove(itemID);
            }
        }

        if (item != null)
            item.Use(this);

        return item;
    }

    public float ElementDefence( Element element)
    {
        int amount = 0;
        var def = Defences.Items.FindAll( x => x.Data.Element == element);
        foreach (var d in def)
        {
            if (amount < d.Data.Amount)
                amount = d.Data.Amount;
        }

        if (Weapon != null)
        {
            foreach (var d in Weapon.Data.DefenceBonuses)
            {
                if (d.Element == element)
                    amount += d.Amount;
            }
        }           

        return (float)amount / 100f;
    }

    public void TakeDamage(Element element, int amount)
    {
        if (element != Element.Untyped && element != Element.All)
        {
            float defense = ElementDefence(element);
            if (defense > 0f)
                amount = Mathf.RoundToInt((float)amount * (1f - defense));
        }
        AdjustHP(-amount);
    }

    public void AdjustHP(int amount)
    {
        int absorbedAmount = 0;
        if (Shield > 0 && amount < 0)           // Aware: the amount is negative if its damage!
        {
            int s = Shield;
            Shield += amount;
            amount += s;      

            if (Shield >= 0)
                absorbedAmount = s - Shield;

            if (Shield < 0) Shield = 0;
            if (amount < 0) amount = 0;       

            if (Shield == 0)
                Manager.BuffTracker.RemoveBuff(this, typeof(ShieldEffect));
        }          

        CurrentHP = Mathf.Clamp(CurrentHP + amount, 0,  MaxHP);

        if(CurrentHP == 0)
        {
            Stats.RemoveMagicModifier();
            Rage = 0f;
        }

       if ( amount < 0)
            Rage += ((float)amount / ((float)MaxHP) * 3);


       // BATTLE MESSAGE
       if (Manager != null)
        {            
            if( absorbedAmount > 0)
                Manager.SetMessage("Absorbed (" + absorbedAmount + ")", CommandMessageColor.Yellow);
            else if (amount < 0)
                Manager.SetMessage((-amount).ToString(), CommandMessageColor.Red);
            else if (amount > 0)
                Manager.SetMessage(amount.ToString(), CommandMessageColor.Green);
        }
    }

    public int UseRage(int dmg)
    {
        int damage = Mathf.RoundToInt( dmg * (1 + Rage));
        Rage = 0;
        return damage;
    }

    public void AddExp(int xp)
    {
        Exp += xp;
    }

    public void AddStatus(Status s)
    {
        if (!ActiveStatuses.Contains(s))
        {
            ActiveStatuses.Add(s);
            Manager.SetMessage(s.ToString(), CommandMessageColor.White);
        }
    }

    public void RemoveStatus(Status s)
    {
        if (ActiveStatuses.Contains(s))
            ActiveStatuses.Remove(s);
    }


    new public CharacterData Save()
    {
        CharacterData data = new CharacterData();       
		data.ContentID = ContentID;
        data.InstanceID = InstanceID;
		data.Exp = Exp;
        data.HP = CurrentHP;

        if( Weapon != null)
            data.Weapon = (ItemData)Weapon.Save();
        
        data.Defenses = Defences.Save<ItemData>();
        data.Inventory = Inventory.Save<ItemData>();
        data.StatUpgrades = Stats.Save();

        return data;
    }

    public void Load(CharacterData data)
    {
        InstanceID = data.InstanceID;
        Exp = data.Exp;
        CurrentHP = data.HP;       
        if (data.Weapon != null)
        {
            Weapon = new WeaponEquipment();
            Weapon.Load(data.Weapon);
        }
        Defences.Load<ItemData>(data.Defenses);
        Inventory.Load<ItemData>(data.Inventory);
        Stats.Load(data.StatUpgrades);

        CurrentHP = MaxHP;   
    }


    public static Character Create(string contentID)
    {
        Character c = new Character(contentID);

        string nativeCard = c.Data.NativeCardContentID;
        if (nativeCard != "")
        {
            var item = ServerLogic.Instance.GameContent.GetContent<CombatCardContent>(nativeCard);
            if (item != null)
            {
                var card = new CombatCard();
                card.SetContentID(nativeCard);
                c.Equip(card);
            }              
        }
        return c;
    }

    public override C ToClient<C>()
    {
        if( !Utils.IsInherited( typeof(C), typeof(CharacterInfo)))
            return null;

        int lvl = Level;

        CharacterInfo info = base.ToClient<C>() as CharacterInfo;
        info.CurrentHP = CurrentHP;
        info.MaxHP = MaxHP;
        info.Shield = Shield;
        info.Rage = Rage;
        info.Exp = Exp;
        info.Level = lvl;
        info.ExpStart = Calculators.TotalExperienceTo(lvl);
        info.ExpEnd = Calculators.TotalExperienceTo(lvl + 1);

        info.MaxItems = Inventory.MaxSlot;
        info.Inventory = Inventory.ToClient<CombatCardInfo>();
        info.MaxDefences = Defences.MaxSlot;
        info.Defences = Defences.ToClient<DefenceInfo>();

        if( Weapon != null)
            info.Weapon = Weapon.ToClient<WeaponInfo>();
      
        info.Stats = Stats.ToClient();       

        return info as C;
    }
}
