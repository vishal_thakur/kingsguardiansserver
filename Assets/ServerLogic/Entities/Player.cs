﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class Player : ILevelable, IItemOwner
{
    public string Identifier { get { return Username; } }

    public int ConnectionID = -1;
    public bool IsConnected { get { return ConnectionID != -1; }}

    public string Username { get; private set; }
    public string Avatar { get; private set; }
    public string Password { get; set; }
    public string DeviceID { get; set; }
    public string FacebookToken { get; set; }
    public string GoogleToken { get; set; }

	//Admin Control
	public bool IsBanned{get;set;}

	//Settings
	public bool IsPushNotificationsEnabled{get;set;}

	//GCM Firebase Push Notification Token
	public string GCMFirebaseToken { get; set; }

    public int Exp { get; private set; }
    public int Level { get { return Calculators.CalculateLevel(Exp); } }
    public int Medals { get; set; }

    public int CastleLevel {
        get 
        { 
            var castle = Buildings.Get("Castle");
            return (castle == null) ? 0 : castle.Upgrade;
        } 
    }
    public Inventory<Item> Inventory { get; private set; }
    public Inventory<Building> Buildings { get; private set; }
    public PlayerCurrencies Materials { get; private set; }

    public List<Character> Characters { get; private set; }
    public List<string> Squad { get; private set; }

	public int WorkerNumber { get { return _workers.Count + Warlocks.Count; } }

    public List<Worker> Workers 
	{ 
		get
		{ 
			if (_workers == null)
			{
				_workers = new List<Worker>();
				var worker = new Worker();
				worker.Type = WorkerType.Worker;
				worker.SetOwner(this);
				Debug.Log(Username + " - Add worker ("+WorkerNumber+"): " + worker.InstanceID);
				_workers.Add(worker);
			}
				
			return _workers;
		}
		private set { _workers = value; }
	}
	List<Worker> _workers;

	public List<Warlock> Warlocks { get; private set; }

	List<RemainsInfo> _raidRemains = new List<RemainsInfo>();


	int _maxWorkers = 1;
	int _maxWarlocks = 0;
	int _lastCastleUpgrade = 0;

    public Player ()
    {
		Reset();
    }  

	public void Reset()
	{
		Avatar = "none";
		Inventory = new Inventory<Item>(this);
		Inventory.IgnoreStacks = true;
		Buildings = new Inventory<Building>(this);

		Materials = new PlayerCurrencies(this);
		Buildings.onListChanged = Materials.RefreshStorage;

		Characters = new List<Character>();
		Squad = new List<string>();
		for (int i = 0; i < Settings.SQUAD_SIZE; i++)
			Squad.Add("");

		Workers = null;
		Warlocks = new List<Warlock>();

		Medals = 0;
	}

    public Character GetSquad(string instanceID)
    {
        var c = Characters.Find(x => x.InstanceID == instanceID);
        return c;
    }

    public void SetSquad(Character c, int slot)
    {
        if (slot >= Settings.SQUAD_SIZE)
        {
            Debug.LogError("Squad slot (" + slot + ") is out of squad size (" + Settings.SQUAD_SIZE + ")");
            return;
        }
        if (slot >= Squad.Count)
        {
            Debug.LogError("Squad slot (" + slot + ") is out of squad list (" + Squad.Count + ")");
            return;
        }

        Squad[slot] = c.InstanceID;
    }

    public void HealSquad()
    {
        foreach (var c in Squad)
            GetSquad(c).AdjustHP(GetSquad(c).MaxHP);
    }


    public void SetUsername(string username){ Username = username; } 
    public void AddExp(int xp) { Exp += xp; }

    public void AddCharacter(Character c)
    {
        c.SetOwner(this);
        c.AdjustHP(c.MaxHP + 100);
        Characters.Add(c);
    }

    public void AddWorker(WorkerType workertype, int etherCost)
    {
    	// check max worker number
		if (_lastCastleUpgrade != CastleLevel)
		{
			var castle = Buildings.Get("Castle");

			_maxWorkers = 0;
			_maxWarlocks = 0;

			for (int i = 1; i <= CastleLevel; i++)
			{
				var u = castle.Data.GetUpgrade(i);
				if (u.WorkerPrice > -1)
					_maxWorkers++;
				if (u.WarlockPrice > -1)
					_maxWarlocks++;
			}
			_lastCastleUpgrade = CastleLevel;
		}

		if (workertype == WorkerType.Worker && _workers.Count >= _maxWorkers)
			return;
		if (workertype == WorkerType.Warlock && Warlocks.Count >= _maxWarlocks)
			return;

		if( etherCost > 0)
            Materials.Subtract(Currency.Ether, etherCost);

		if (workertype == WorkerType.Worker)
		{
			Worker w = new Worker();
			w.Type = workertype;
			w.SetOwner(this);
			Workers.Add(w);
		}
		else
			Warlocks.Add(new Warlock(){ ID = Username + "_Warlock_"+Warlocks.Count });
    }

	public void AttackWarlock(string buildingID)
	{
		var warlock = Warlocks.Find(x => x.Building == "" || x.Building == null);
		warlock.Building = buildingID;
	}

	public void DetachWarlock(string buildingID)
	{
		var warlock = Warlocks.Find(x => x.Building == buildingID);
		warlock.Building = "";
	}

	public void DestroyWarlock(List<IRaidUnit> destroyedBuildings)
	{
		foreach (var b in destroyedBuildings)
			DestroyWarlock(b.InstanceID);	
	}

	public void DestroyWarlock(string buildingID)
	{
		var warlock = Warlocks.Find(x => x.Building == buildingID);
		if (warlock != null)
			Warlocks.Remove(warlock);
	}

    public int BuildingCount( string contentID)
    {
        var list = Buildings.Items.FindAll(x => x.ContentID == contentID);
        return list.Count;
    }


	public void AddRemain(IRaidUnit unit)
	{
//		_raidRemains.Add(new RemainsInfo(){ InstanceID = unit.InstanceID, ContentID = unit.ContentID, Position = unit.Position.vector3 });
		_raidRemains.Add(new RemainsInfo(){ InstanceID = unit.InstanceID, ContentID = unit.ContentID, x = unit.Position.vector3.x, y = unit.Position.vector3.y , z = unit.Position.vector3.z });
	}

	public bool CollectRemain(string id)
	{
		var remain = _raidRemains.Find(x => x.InstanceID == id);
		if (remain != null)
		{
			_raidRemains.Remove(remain);
			Materials.Add(Currency.Gold, 100);
			return true;
		}
		return false;
	}

    public PlayerData Save()
    {
        PlayerData data = new PlayerData();
        data.Username = Username;
        data.Password = Password;
		data.IsBanned = IsBanned;
		data.IsPushNotificationsEnabled = IsPushNotificationsEnabled;
        data.FacebookToken = FacebookToken;
        data.GoogleToken = GoogleToken;
        data.DeviceID = DeviceID;
		data.GCMFirebaseToken = GCMFirebaseToken;

        data.Avatar = Avatar;
        data.Exp = Exp;
        data.Level = Level;
        data.Medals = Medals;
        data.Inventory = Inventory.Save<ItemData>();
        data.Buildings = Buildings.Save<BuildingData>();

        data.Materials = Materials.Save();

		data.Remains = _raidRemains;

        string s = Username + " Saved inventory:\n";
        foreach (var i in data.Inventory)
            s += "-" + i.ContentID+"\n";
        Debug.Log(s);

        foreach (var c in Characters)
            data.Characters.Add(c.Save());

        data.Squad = Squad;

        foreach (var w in Workers)
            data.Workers.Add(w.Save());

		data.Warlocks = Warlocks;

        data.GridTileSize = GridSystem.GridHandler.Instance.TileSize.x + "," +GridSystem.GridHandler.Instance.TileSize.y;

        return data;
    }

    public void Load(PlayerData data)
    {
        Username = data.Username;
        Password = data.Password;
		IsBanned = data.IsBanned;
		IsPushNotificationsEnabled = data.IsPushNotificationsEnabled;
        FacebookToken = data.FacebookToken;
        GoogleToken = data.GoogleToken;
        DeviceID = data.DeviceID;
		GCMFirebaseToken = data.GCMFirebaseToken;

        Avatar = data.Avatar;
        if (Avatar == null)
            Avatar = "none";
        Exp = data.Exp;

        Medals = data.Medals;

        if(data.GridTileSize == "" || data.GridTileSize != (GridSystem.GridHandler.Instance.TileSize.x + "," +GridSystem.GridHandler.Instance.TileSize.y))         
        {
            foreach (var b in data.Buildings)
                b.Position = GridSystem.GridHandler.Instance.Center.ToString(); 
        } 

        Inventory.Load(data.Inventory);
        Buildings.Load(data.Buildings);
        Materials.Load(data.Materials);
        Materials.RefreshStorage();

		_raidRemains = data.Remains;

        foreach (var cd in data.Characters)
        {
            Character c = new Character(cd.ContentID);
            c.Load(cd);
			c.SetOwner(this);
            Characters.Add(c);
        }

        Squad = data.Squad;

		Workers.Clear();
        foreach (var wd in data.Workers)   
			AddWorker(wd.Type, 0);

		if(data.Warlocks != null)
			Warlocks = data.Warlocks;
    }







    public PlayerInfo AccountToClient(bool forPlayerList = false)
    {
        PlayerInfo info = new PlayerInfo();

        // BASE DATA
        info.Username = Username;
        info.DeviceID = DeviceID;
        info.FacebookToken = FacebookToken;
        info.GoogleToken = GoogleToken;
		info.IsBanned = IsBanned;
		info.IsPushNotificationsEnabled = IsPushNotificationsEnabled;
		info.GCMFirebaseToken = GCMFirebaseToken;
        int lvl = Level;

        info.AvatarID = Avatar;
        info.Exp = Exp;
        info.Level = lvl;
        info.ExpStart = Calculators.TotalExperienceTo(lvl);
        info.ExpEnd = Calculators.TotalExperienceTo(lvl + 1);

        info.MaxBuildings = Buildings.MaxSlot;
        info.MaxItems = Inventory.MaxSlot;

        if (forPlayerList)
        {
            info.Squad = new List<SquadInfo>();
            foreach (var q in Squad)
            {
                var c = GetSquad(q);

                if (c != null)
                    info.Squad.Add(new SquadInfo()
                        { 
                            ContentID = c.ContentID, 
                            InstanceID = c.InstanceID, 
                            Level = c.Level, 
                            MaxHP = c.MaxHP, 
                            HP = c.CurrentHP,
                            Readiness = c.Readiness
                        });
                else
                    info.Squad.Add(null);
            }
        }

        info.GridTileSize = GridSystem.GridHandler.Instance.TileSize.ToString();

		info.Remains = _raidRemains;
        return info;
    }

    public List<BuildingInfo> BuildingsToClient(string instanceID = "")
    {
        return Buildings.ToClient<BuildingInfo>();
    }

    public List<ItemInfo> InventoryToClient(string instanceID = "")
    {
        return Inventory.ToClient<ItemInfo>();
    }

    public List<SMaterial> MaterialsToClient()
    {
        return Materials.Save();
    }

    public List<CharacterInfo> CharactersToClient(string instanceID = "")
    {
        var characters = new List<CharacterInfo>();
        foreach (var c in Characters)
        {
            if( instanceID == "" || instanceID == c.InstanceID)
                characters.Add(c.ToClient<CharacterInfo>());
        }
        return characters;
    }

    public List<string> SquadToClient()
    {
        return Squad;
    }

    public List<WorkerInfo> WorkersToClient(string instanceID = "")
    {
        List<WorkerInfo> workers = new List<WorkerInfo>();
        foreach (var w in Workers)
        {
            if( instanceID == "" || instanceID == w.InstanceID)
                workers.Add(w.ToClient<WorkerInfo>());
        }
        return workers;
    }
}

