﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CombatCard : Item
{
    public override ItemCategory Category { get { return Data.Category; }  }

    new public CombatCardContent Data { get { return ServerLogic.Instance.GameContent.GetContent<CombatCardContent>(ContentID); } }

    public void Use(Character target, BuffTracker bufftracker)
    {
        base.Use(target);

        foreach (var effect in Data.Effects)
        {
            if (Utils.IsInherited(effect.GetType(), typeof(BuffBase)))
                bufftracker.AddBuff((BuffBase)effect, CharacterOwner, target, Data.Duration);
            else
                effect.Apply(target, CharacterOwner, Data.Duration);  
        }
    }

    public override bool IsTypeof(System.Type type)
    {
        if (base.IsTypeof(type))
            return true;

        return Data.Effects.Find(x => x.GetType() == type || x.GetType().IsInstanceOfType(type)) != null;
    }
}
