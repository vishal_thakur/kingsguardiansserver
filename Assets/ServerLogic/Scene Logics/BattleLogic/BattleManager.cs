﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum BattleControll { Player, AI }
public enum BattleStatus { Playing, Win, Lose, Forfeit }

public class BattleManager 
{
    public BattleStatus CurrentStatus { get; private set; }

    public BuffTracker BuffTracker = new BuffTracker();
    public Character CurrentCharacter { get { return (_currentCharacter == -1 || _currentCharacter > _characters.Count) ? null : _characters[_currentCharacter]; } }   
    public List<CommandResult> Commands = new List<CommandResult>();

    public Player Opponent { get { return _enemy; }}

    AI _ai;
    Player _player;
    Player _enemy;
    List<Character> _characters = new List<Character>();
    int _currentCharacter = -1;

    List<ActionLog> _log = new List<ActionLog>();
    string _message = "";
    CommandMessageColor _messageColor = CommandMessageColor.White;

    /// <summary>
    /// Initializes a new instance of the <see cref="BattleManager"/> class.
    /// </summary>
    /// <param name="player">Player.</param>
    /// <param name="enemy">Enemy.</param>
    public BattleManager( Player player, Player enemy )
    {
        _player = player;
        _enemy = enemy;
        _ai = new AI(this);

        // Create copy from the player squad.
        foreach (var ps in _player.Squad)
        {
            var c = _player.GetSquad(ps);
            if (c != null)
            {
                var copy = new Character(c.ContentID);
                copy.Load(c.Save());
                     
                copy.ControledBy = BattleControll.Player;
                copy.Manager = this;
                _characters.Add(copy);
                _ai.Enemies.Add(copy);
            }   
       }

        // Create copy from the enemy squad.
        foreach (var es in _enemy.Squad)
        {
            var c = _enemy.GetSquad(es);
            if (c != null)
            {
                var copy = new Character(c.ContentID);
                copy.Load(c.Save());

                copy.ControledBy = BattleControll.AI;
                copy.Manager = this;

                _characters.Add(copy);
                _ai.Allies.Add(copy);
            }
        }

        NextTurn();
    }

    /// <summary>
    /// Sets the message.
    /// </summary>
    /// <param name="msg">Message.</param>
    /// <param name="color">Color.</param>
    public void SetMessage(string msg, CommandMessageColor color )
    {
        _message = msg;
        _messageColor = color;
    }

    /// <summary>
    /// Destroys this instance.
    /// </summary>
    public void Destroy()
    {
        // Update player characters.
        foreach (var c in _characters)
        {
            if (c.ControledBy == BattleControll.Player)
            {                
                var pc = _player.GetSquad(c.InstanceID);
                c.AdjustHP(c.MaxHP);    // REMOVE: heal player squad after battle.
                pc.Load(c.Save());
            }
        }     
    }



    /// <summary>
    /// Proceed the player's action.
    /// </summary>
    /// <param name="casterID">Caster ID.</param>
    /// <param name="targetID">Target ID.</param>
    /// <param name="cardID">Card ID.</param>
    public void PlayerAction(string casterID, string targetID, string cardID)
    {
        Commands.Clear();

        // Get the caster and the target
        var caster = _characters.Find(x => x.InstanceID == casterID);
        var target = _characters.Find(x => x.InstanceID == targetID);
        if (caster == null || target == null)
        {
            Debug.LogError("Player action is invalid: " + ((caster == null) ? "Character is null ("+casterID+")":"") + " " + ((target == null) ? "Character is null ("+targetID+")":""));
            return;
        }

        // Get the card
        CombatCard card = null;
        if (cardID != "BasicAttack" && cardID != "RageAttack")
        {
            card = caster.Inventory.Get(cardID);
            if (card == null || caster.Inventory.Stacks(cardID) == 0)
            {
                Debug.LogError("The card is invalid " + cardID);
                return;
            }

            if ((card.Data.Target == TargetType.Foe) != (target.ControledBy == BattleControll.AI))
            {
                Debug.LogError("The target is invalid type - needs" + card.Data.Target);
                return;
            }  
        }

        // Use the card.
        UseCard(caster, target, card, true, cardID == "RageAttack");

        _maxsteps = 20;
        NextTurn();
    }

    /// <summary>
    /// Uses the card.
    /// </summary>
    /// <param name="caster">Caster.</param>
    /// <param name="target">Target.</param>
    /// <param name="card">Card.</param>
    /// <param name="useStack">If set to <c>true</c> decreases the card actual stacks, otherwise it just tracks virtualy.</param>
    /// <param name="rage">If set to <c>true</c> uses rage attack.</param>
    public void UseCard(Character caster, Character target, CombatCard card, bool useStack, bool rage)
    {
        // Confused caster effect
        if (caster.ActiveStatuses.Contains(Status.Confused))
        {
            // random target,
            target = _characters[Random.Range(0, _characters.Count)];

            // random card
            int index = Random.Range(0, Mathf.RoundToInt(caster.Inventory.Items.Count * 1.5f));
            card = (index < caster.Inventory.Items.Count) ? caster.Inventory.Items[index] : null;         
        }       

        string cardID = "";

        // Basic or Rage Attack
        if (card == null)
        {
            cardID = (rage) ? "RageAttack" : "BasicAttack";
            BasicAttack.Use(caster, target, rage);

            // Double attack effect
            if (caster.ActiveStatuses.Contains(Status.DoubleAttack))
            {
                Debug.LogWarning("Double Attack: " + cardID);
                // Remove status, to prevent infinite attack.
                caster.RemoveStatus(Status.DoubleAttack);
                // Use this attack again with the same parameters.
                UseCard(caster, target, card, useStack, rage);


            }
        }
        // Combat Card
        else
        {
            cardID = card.ContentID;
            card.Use(target, BuffTracker);

            if (useStack) caster.Inventory.Remove(cardID);
            else _ai.RemoveStack(caster, card);
        }    

        // Update Combat Log, but do not contain more than 5 entry.
        if (_log.Count > 5)
            _log.RemoveRange(0, _log.Count - 5);
        _log.Add(new ActionLog(){ CharacterID = caster.InstanceID, CardID = cardID, TargetID = target.InstanceID});

        // Create Command Result from this action.
        CommandResult command = new CommandResult();
        command.Caster = caster.InstanceID;
        command.Target = target.InstanceID;
        command.UsedCardContentID = cardID; 

        command.TargetHP = target.CurrentHP;
        command.TargetShield = target.Shield;
        if( target.ControledBy == BattleControll.Player )
            command.Stats = target.Stats.ToClient();       

        command.Buffs = BuffTracker.ToClient();
        command.Log = _log;            
        command.TurnOrder = GetTurnOrder();

        if (_message != "")
        {
            command.Message = _message;
            command.MessageType = _messageColor;
        }
        _message = "";
       
        Commands.Add(command);
    }

    /// <summary>
    /// Gets the turn order.
    /// </summary>
    /// <returns>The turn order.</returns>
    public List<TurnOrder> GetTurnOrder()
    {
        List<TurnOrder> list = new List<TurnOrder>();
        foreach (var c in _characters)
            list.Add(new TurnOrder(){ CharacterID = c.InstanceID, Readiness = c.Readiness });
        return list;
    }
 

    /// <summary>
    /// Forces to win the match.
    /// Test function.
    /// </summary>
    public void ForceWin()
    {
        CurrentStatus = BattleStatus.Win;
        NextTurn();
    }



    int _maxsteps = 10;

    void NextTurn()
    {
        UpdateStatus();

        _maxsteps--;
        if (_maxsteps <= 0)
        {
            Debug.LogWarning("Stack overflow: " + _maxsteps + " - " + CurrentStatus);
            return;
        }

        if (CurrentStatus == BattleStatus.Playing)
        {          
            // Calculate next character
            float nextReach = 100;
            for (int i = 0; i < _characters.Count; i++)
            {
                float reach = Mathf.Max(0, (100f - _characters[i].Readiness) / _characters[i].Stats.GetStat(CharacterStats.Speed));

                if (reach < nextReach && !_characters[i].Dead)
                {
                    nextReach = reach;
                    _currentCharacter = i;
                }
            }

            // Increase readiness for each character
            foreach (Character c in _characters)
                c.Readiness += c.Stats.GetStat(CharacterStats.Speed) * nextReach;

            // If the next character is valid.
            if (_currentCharacter > -1)
            {
                Debug.LogWarning("Battle Message: The turn is on " + CurrentCharacter.InstanceID);

                // Handle bufftracker next turn
                BuffTracker.NextTurn(CurrentCharacter);

                // Reset it's readiness to zero.
                _characters[_currentCharacter].Readiness = 0;

                // If the character is stunned, skip it's turn
                if (CurrentCharacter.IsStunned)
                    NextTurn();                      

                // If the current character is AI driven.
                if (CurrentCharacter.ControledBy == BattleControll.AI)
                {
                    _ai.Act(CurrentCharacter);
                    NextTurn();
                }

                // If the current is player commanded, don't do anyhting, this will make the server send back the response.  
            }  
        }
        else
            CalculateBattleResult();        
    }


    void UpdateStatus()
    {
        if (CurrentStatus != BattleStatus.Playing) return;

        bool won = _characters.Find(x => x.ControledBy == BattleControll.AI && !x.Dead) == null;
        bool lost = _characters.Find(x => x.ControledBy == BattleControll.Player && !x.Dead) == null;

        if (won)
            CurrentStatus = BattleStatus.Win;
        else if (lost)
            CurrentStatus = BattleStatus.Lose;
        else
            CurrentStatus = BattleStatus.Playing;
    }

    void CalculateBattleResult()
    {
        int gold = Mathf.RoundToInt((float)Settings.BATTLE_GOLD_REWARD * ((CurrentStatus == BattleStatus.Win) ? 1 : 0.5f));
        _player.Materials.Add(Currency.Gold, gold);

        int xp = Mathf.RoundToInt((float)Settings.BATTLE_XP_REWARD * ((CurrentStatus == BattleStatus.Win) ? 1 : 0.5f));
        _player.AddExp(xp);

        foreach (var ps in _player.Squad)
        {
            var c = _player.GetSquad(ps);
            if (c == null)
                continue;

            c.AddExp(xp);
            if (CurrentStatus == BattleStatus.Win)
                c.AdjustHP(c.MaxHP);
            c.Defences.Clear();
        }
    }


 
   

}




