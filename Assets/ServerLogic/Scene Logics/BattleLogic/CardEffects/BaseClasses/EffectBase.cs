﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class EffectBase : IEffect
{   
    public float Chance = 1f;
    public virtual string Description { get {return "none"; }}

    public EffectBase( List<Parameter> data )
    {
        foreach (var f in GetType().GetFields())
        {
            if (f.IsPublic && !f.IsLiteral)
            {
                string key = f.Name;
                var variable = data.Find(x => x.Key == key);
                if( variable != null)
                    f.SetValue(this, variable.Value);
            }
        }
    }

    public virtual void Apply (Character target, Character caster, int duration)
    {
       
    }

    public virtual bool Test(Character caster, Character target)
    {
        if (Chance == 1) return true;        
        return Random.Range(0, 1) <= Chance;
    }
}
