using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BuffBase : EffectBase
{
    public virtual string ID { get { return ""; }}

    public BuffBase(List<Parameter> data) : base(data){}

    public virtual void Remove(Character target, Character caster){ }
    public virtual void NextTurn(Character target, Character caster){ }
}

