﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class DispelEffect : EffectBase
{
    public override string Description
    {
        get
        {
            return "Removes a buff from the target.";
        }
    }

    public DispelTarget BuffType;

    public DispelEffect( List<Parameter> data ) : base (data){}

    public override void Apply (Character target, Character caster, int duration)
    {    
        switch (BuffType)
        {
            case DispelTarget.Buff:
                target.Manager.BuffTracker.RemoveBuff(target, typeof(StatModificationEffect));
                break;

            case DispelTarget.Poison:
                target.Manager.BuffTracker.RemoveBuff(target, typeof(DOTEffect)); 
                break;
        }                
    }
} 
  
public enum DispelTarget
{
    Buff,
    Poison,
    Stun,
    Fire, 
    Freeze,
    Confuse
}
