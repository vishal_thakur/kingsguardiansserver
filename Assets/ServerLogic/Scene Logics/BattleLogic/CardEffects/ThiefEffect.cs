using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ThiefEffect : EffectBase
{
    public override string Description
    {
        get
        {
            return "Steals a random card from the target";
        }
    }


    public ThiefEffect(List<Parameter> data) : base(data){ }


    public override void Apply(Character target, Character caster, int duration)
    {
        var card = target.Inventory.Items[Random.Range(0, target.Inventory.Items.Count)];

        target.Inventory.Remove(card.InstanceID);
        caster.Inventory.Add(card);
    }
      
}


