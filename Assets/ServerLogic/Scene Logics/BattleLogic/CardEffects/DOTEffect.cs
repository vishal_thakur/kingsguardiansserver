﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DOTEffect : BuffBase
{
    public override string ID { get { return (Element == Element.Untyped) ? "Poison" : Element.ToString(); } }

    public override string Description
    {
        get
        {
            return "Deals " + Amount + "% Health points of "+Element+" damage in every turn." ;
        }
    }

    public Element Element;
    public int Amount = 0;
    public bool FixedAmount = false;

    public DOTEffect( List<Parameter> data ) : base (data){}

    public override void NextTurn(Character target, Character caster)
    {
        int amount = Amount;
        if (!FixedAmount)
            amount = Mathf.Abs(Mathf.RoundToInt(target.MaxHP * Amount/100));  
        
        target.TakeDamage(Element, amount);
    }
}
