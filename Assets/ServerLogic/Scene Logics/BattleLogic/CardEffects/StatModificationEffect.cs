using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class StatModificationEffect : BuffBase
{
    public override string ID { get { return Stat.ToString(); } }

    public override string Description
    {
        get
        {
            return "+" + Mathf.RoundToInt(Multiplier*100) + "% " + Stat;
        }
    }

    public CharacterStats Stat;
    public float Multiplier;

    public StatModificationEffect(List<Parameter> data) : base(data){ }

    public override void Apply(Character target, Character caster, int duration)
    {
        target.Stats.AddMagicModifier(Stat, Multiplier, ID);
    }  

    public override void Remove (Character target, Character caster)
	{
        target.Stats.RemoveMagicModifier(ID);
	}
}

