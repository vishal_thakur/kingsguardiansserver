using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class StatusEffect : BuffBase
{
    public override string ID { get { return Status.ToString(); } }

    public override string Description
    {
        get
        {
            return "Makes the target " + Status;
        }
    }

    public Status Status;

    public StatusEffect(List<Parameter> data) : base(data){ }

    public override bool Test(Character caster, Character target)
    {
        return !target.ActiveStatuses.Contains(Status);
    }

    public override void Apply(Character target, Character caster, int duration)
    {
        target.AddStatus(Status);
    }

    public override void Remove(Character target, Character caster)
    {
        target.RemoveStatus(Status);
    }
      
}

public enum Status
{
    Concealment,
    Miss,
    Confused,
    Stun,
    DoubleAttack

}
