using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ShieldEffect : BuffBase
{
    public override string ID { get { return "Shield"; } }

    public override string Description
    {
        get
        {
            return "Creates a shield that absorbs damage equals to " + Amount + "% of Max Health";
        }
    }

    public int Amount;

    public ShieldEffect(List<Parameter> data) : base(data){ }

    public override void Apply(Character target, Character caster, int duration)
    {
        target.Shield = Mathf.Abs(Mathf.RoundToInt(target.MaxHP * Amount/100));     
    }  

    public override void Remove (Character target, Character caster)
	{
        target.Shield = 0;
	}
}

