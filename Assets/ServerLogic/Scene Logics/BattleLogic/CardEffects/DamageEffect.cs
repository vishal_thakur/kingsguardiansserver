﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class DamageEffect : EffectBase
{
    public override string Description
    {
        get
        {
            if (Elemental == Element.Untyped)
                return "Does " + Amount + "% of damage.";
           return "Does " + Amount + " " + Elemental + " damage";
        }
    }

    public Element Elemental;
	public int Amount = 0;
    public bool DamageOverTime = false;

    public DamageEffect( List<Parameter> data ) : base (data){}

    public override void Apply (Character target, Character caster, int duration)
	{
        if (Elemental == Element.All) return;

        int amount = (Elemental == Element.Untyped) ? 
            Mathf.RoundToInt(target.MaxHP * ((float)Amount / 100)) :
            Amount + caster.Stats.GetStat(CharacterStats.MagicAttack);
        
		target.TakeDamage(Elemental, Mathf.RoundToInt(amount));

        if (DamageOverTime)
        {
            DOTEffect dot = new DOTEffect(new List<Parameter>());
            dot.Element = Elemental;
            dot.Chance = 1;
            dot.FixedAmount = true;
            dot.Amount = Mathf.RoundToInt( (float)amount * 0.2f);

            target.Manager.BuffTracker.AddBuff(dot, caster, target, duration);
        }
	}
}

