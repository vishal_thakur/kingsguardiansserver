﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HOTEffect : BuffBase
{
    public override string ID { get { return "HOT"; } }
    
    public override string Description
    {
        get
        {
            return "Heals " + Amount + "% Health points in every turn" ;
        }
    }

    public int Amount = 0;

    public HOTEffect( List<Parameter> data ) : base (data){}

    public override void NextTurn(Character target, Character caster)
    {
        int amount = Mathf.Abs(Mathf.RoundToInt(target.MaxHP * Amount/100));            
        target.AdjustHP(amount);
    }
}

