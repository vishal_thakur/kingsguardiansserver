﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class HealEffect : EffectBase
{
    public override string Description
    {
        get
        {
            return "Heals " + Amount + "% Health points";
        }
    }

    public int Amount = 0;

    public HealEffect( List<Parameter> data ) : base (data){}

    public override void Apply (Character target, Character caster, int duration)
    {      
        int amount = Mathf.Abs(Mathf.RoundToInt(target.MaxHP * Amount/100));            
        target.AdjustHP(amount);
    }
} 
    
