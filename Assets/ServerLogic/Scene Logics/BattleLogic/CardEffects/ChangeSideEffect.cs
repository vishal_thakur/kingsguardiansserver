using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ChangeSideEffect : BuffBase
{
    public override string Description
    {
        get
        {
            return "Controls enemy";
        }
    }


    public ChangeSideEffect(List<Parameter> data) : base(data){ }

    public override void Apply(Character target, Character caster, int duration)
    {
        target.ControledBy = caster.ControledBy;
    }
 
    public override void Remove(Character target, Character caster)
    {
        target.ControledBy = (target.ControledBy == BattleControll.AI) ? BattleControll.Player : BattleControll.AI;
    } 
}


