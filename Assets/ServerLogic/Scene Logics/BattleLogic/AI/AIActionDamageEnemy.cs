﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIActionDamageEnemy : AIAction
{    
    protected override List<System.Type> Effects { get { return new  List<System.Type>(){
                typeof(DamageEffect),
                typeof(DOTEffect)
            }; } }

    public AIActionDamageEnemy(AI ai) : base(ai){}

    // Get the strongest ally
    protected override Character GetTarget()
    {       
        var livingEnemies = ai.Enemies.FindAll( x => !x.Dead );
        if (livingEnemies.Count > 0)
            return livingEnemies [Random.Range (0, livingEnemies.Count)];
        return null;
    }

    protected override CombatCard SelectCard(List<CombatCard> cards, Inventory<CombatCard> inventory)
    {
        return cards[Random.Range(0, cards.Count)];
    }
}
