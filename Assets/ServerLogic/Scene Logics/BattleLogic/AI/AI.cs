﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI 
{
    public List<Character> Allies = new List<Character>();
    public List<Character> Enemies = new List<Character>();

    public Character Me { get; private set; }
    public BattleManager Manager { get; private set; }

    List<AIAction> _actions = new List<AIAction>();

    Dictionary<string, Dictionary<string, int>> _cardStacks = new Dictionary<string, Dictionary<string, int>>();

    public AI ( BattleManager manager)
    {
        Manager = manager; 
        _actions.Add(new AIActionProtectAlly(this));
        _actions.Add(new AIActionDebuffEnemy(this));
        _actions.Add(new AIActionBuffAlly(this));
        _actions.Add(new AIActionDamageEnemy(this));
    }

    public void Act(Character me)
    {
        Me = me;

        bool basicAttack = true;
        foreach (var action in _actions)
        {
            if (action.Activate())
            {
                basicAttack = false; 
                break;
            }
        }

        if (basicAttack)
        {
            var livingEnemies = Enemies.FindAll( x => !x.Dead );
            if (livingEnemies.Count == 0)
                return;
            
            int cnt = Random.Range(0, livingEnemies.Count);
            if (cnt >= livingEnemies.Count)
            {
                string s = "";
                foreach (var e in Enemies)
                    s += "-"+e.ContentID + " " + e.CurrentHP + " is dead? " + e.Dead + "\n"; 
                Debug.LogWarning(Me.ContentID + " want to use basic attack, but there is only " + livingEnemies.Count + " out of " + Enemies.Count + " enemies\n"+s);
            }
            var target = livingEnemies [cnt];

            bool rage = Me.Rage > 0.5 && Random.Range(0, 1) > 0.5f;

            Manager.UseCard(me, target, null, false, rage);     
        }        
    }


    public int GetStacks (string contentID)
    {
        if (!_cardStacks.ContainsKey(Me.InstanceID))
            _cardStacks.Add(Me.InstanceID, new Dictionary<string, int>());

        if(!_cardStacks[Me.InstanceID].ContainsKey(contentID))
            _cardStacks[Me.InstanceID].Add(contentID, Me.Inventory.Stacks(contentID));

        return _cardStacks[Me.InstanceID][contentID];
    }

    public void RemoveStack(Character character, CombatCard card)
    {
        _cardStacks[character.InstanceID][card.ContentID]--;
    }
}
