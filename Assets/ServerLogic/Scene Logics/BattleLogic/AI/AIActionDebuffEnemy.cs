﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIActionDebuffEnemy : AIAction
{    
    protected override List<System.Type> Effects { get { return new  List<System.Type>(){
                typeof(DispelEffect),
                typeof(StatusEffect),               
            }; } }

    public AIActionDebuffEnemy(AI ai) : base(ai){}

    // Get the strongest enemy character
    protected override Character GetTarget()
    {       
        Character strongestMelee = null;
        int dmgPerTurn = 0;

        foreach(Character c in ai.Enemies)
        {
            if( !c.Dead && !c.IsStunned)
            {
                int eAttack = c.Stats.GetStat(CharacterStats.Attack);
                int turns = c.Stats.GetStat(CharacterStats.Speed);
                int dpt = eAttack * turns;
                if(dpt > dmgPerTurn)
                {
                    dmgPerTurn = dpt;
                    strongestMelee = c;
                }
            }
        }
        return strongestMelee;
    }


    protected override CombatCard SelectCard(List<CombatCard> cards, Inventory<CombatCard> inventory)
    {
        cards.RemoveAll(x => x.Data.Target == TargetType.Friend);
        int buffCount = target.Manager.BuffTracker.BuffCount(target, new List<System.Type>(){ typeof(StatModificationEffect)}); 

        CombatCard card = null;
        if( buffCount > 0 )
            card = cards.Find( x => x != null && x.Data.Effects.Find(e => e.GetType() == typeof(DispelEffect)) != null);

        if( card == null)
            card = cards.Find( x => x != null && x.Data.Effects.Find(e => e.GetType() == typeof(StatusEffect)) != null);

        return card;
    }
}
