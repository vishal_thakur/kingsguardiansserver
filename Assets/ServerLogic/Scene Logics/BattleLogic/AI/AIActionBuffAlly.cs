﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIActionBuffAlly : AIAction
{    
    protected override List<System.Type> Effects { get { return new  List<System.Type>(){
                typeof(StatModificationEffect)
            }; } }

    public AIActionBuffAlly(AI ai) : base(ai){}

    // Get the strongest ally
    protected override Character GetTarget()
    {       
        Character strongestMelee = null;
        int dmgPerTurn = 0;

        foreach(Character c in ai.Allies)
        {
            if( !c.Dead && !c.IsStunned)
            {
                int eAttack = c.Stats.GetStat(CharacterStats.Attack);
                int turns = c.Stats.GetStat(CharacterStats.Speed);
                int dpt = eAttack * turns;
                if(dpt > dmgPerTurn)
                {
                    dmgPerTurn = dpt;
                    strongestMelee = c;
                }
            }
        }
        return strongestMelee;
    }


    protected override CombatCard SelectCard(List<CombatCard> cards, Inventory<CombatCard> inventory)
    {
        CharacterStats[] stats = new CharacterStats[]{ CharacterStats.Attack, CharacterStats.Speed };                
        foreach( CharacterStats stat in stats)
        {
            var c = cards.Find( x => x.Data.Effects.Find ( y => ((StatModificationEffect)y).Stat == stat) != null );
            if( c != null)
                return c;
        }               
        return null;
    }
}
