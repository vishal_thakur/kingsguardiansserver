﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIAction 
{
    protected virtual List<System.Type> Effects { get { return new List<System.Type>(); }}

    protected Character target;
    protected AI ai;

    public AIAction(AI ai)
    {
        this.ai = ai;
    }

    public bool Activate()
    {
        target = GetTarget();
        if (target == null) return false;

        var buffs = Effects.FindAll(x => x.GetType().IsInstanceOfType(typeof(BuffBase)));
        if (buffs.Count > 0)
            buffs.RemoveAll(x => ai.Manager.BuffTracker.HasBuff(target, x)); 
  
        var cards = ai.Me.Inventory.Get(Effects); 
        cards.RemoveAll(x => ai.GetStacks(x.ContentID) == 0);
        if (cards.Count == 0) return false;
       

        CombatCard card = SelectCard(cards, ai.Me.Inventory);
        if (card == null) return false;

        ai.Manager.UseCard(ai.Me, target, card, false, false);    
        return true;
    }	


    protected virtual CombatCard SelectCard(List<CombatCard> cards, Inventory<CombatCard> inventory)
    {
        return null;
    }
   
    protected virtual Character GetTarget()
    {
        return null;
    }


}
