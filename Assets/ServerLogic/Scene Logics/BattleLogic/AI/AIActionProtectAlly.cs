﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIActionProtectAlly : AIAction
{  
    protected override List<System.Type> Effects { get { return new  List<System.Type>(){
                typeof(HealEffect), 
                typeof(HOTEffect),
                typeof(StatusEffect),
                typeof(ShieldEffect),
                typeof(DispelEffect)
            }; } }

    public AIActionProtectAlly(AI ai) : base(ai){}

    // Get the most wounded character
    protected override Character GetTarget()
    {
        Character target = null;
        float percentage = 1;

        foreach(Character c in ai.Allies)
        {
            if( !c.Dead )
            {
                float p = c.CurrentHP / c.MaxHP;
                if(p < percentage)
                {                    
                    target = c;
                    percentage = p;
                }
            }
        }
        return target;
    }

    protected override CombatCard SelectCard(List<CombatCard> cards, Inventory<CombatCard> inventory)
    {    
        cards.RemoveAll(x => x.Data.Target == TargetType.Foe);

        int wounds = Mathf.RoundToInt(100f * (float)target.CurrentHP / (float)target.MaxHP);
        float bestHeal = 0;
        float bestHot = 0;

        CombatCard heal = null;
        CombatCard hot = null;
        CombatCard shield = null;
        CombatCard status = null;
  
        foreach (var c in cards)
        {
            if (c == null) continue;

            if (target.Dead != c.Data.UseOnDead)
                continue;                

            // HEAL
            if( c.IsTypeof(typeof(HealEffect)))
            {
                var healEffect = (HealEffect)c.Data.Effects.Find(x => x.GetType() == typeof(HealEffect));
                if (healEffect != null && healEffect.Amount < wounds && healEffect.Amount > bestHeal)
                {
                    heal = c;
                    bestHeal = healEffect.Amount;
                }
            }

            // HOT
            if( c.IsTypeof(typeof(HOTEffect)))
            {
                var hotEffect = (HOTEffect)c.Data.Effects.Find(x => x.GetType() == typeof(HOTEffect));
                if (hotEffect != null && hotEffect.Amount > bestHot)
                {
                    hot = c;
                    bestHot = hotEffect.Amount;
                }
            }

            if( c.IsTypeof(typeof(ShieldEffect)) )
                shield = c;

            if( c.IsTypeof(typeof(StatusEffect)) )
                status = c;

        }

        if (heal != null)
            return heal;
        if (hot != null)
            return hot;
        if ( shield != null)
            return shield;
        if ( status != null)
            return status;
        return null;
    }
}
