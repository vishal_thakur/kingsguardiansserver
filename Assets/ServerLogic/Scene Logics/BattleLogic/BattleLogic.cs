﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleLogic
{
    public static ErrorInfo Start( Player player, SimpleMessage request )
    {
        if( ServerLogic.Instance.ActiveBattles.ContainsKey(player.Username))
            return new ErrorInfo(){ Code = ErrorCode.AlreadyInBattle };

        string enemyUsername = request.GetValue(ParameterCode.Username);


        Player enemy = ServerLogic.Instance.PlayerDatabase.GetPlayer(enemyUsername);  

		//Send Attack PushNotification 
		FirebaseNotificationManager.Instance.SendPushNotification(enemy,  "Hey " + enemyUsername  , "Your Village is under Attack by " + player.Username +" !!!");

        if( enemy == null)
            return new ErrorInfo(){ Code = ErrorCode.EnemyDoesNotExists, ContentID = enemyUsername };
       
        var battle = new BattleManager(player, enemy);
        ServerLogic.Instance.ActiveBattles.Add(player.Username, battle);

        NetworkManager.SendMessage(player, RequestCode.StartBattle, new BattleMessage()
            {
                NextPlayerCharacter = (battle.CurrentCharacter != null) ? battle.CurrentCharacter.InstanceID : "",
                CommandResults = battle.Commands,
                TurnOrder = (battle.Commands.Count == 0 ) ? battle.GetTurnOrder() : new List<TurnOrder>(),
            });
        return null;
    }

    public static ErrorInfo OnPlayerRequest( Player player, SimpleMessage request )
    {
        if(!ServerLogic.Instance.ActiveBattles.ContainsKey(player.Username))
            return new ErrorInfo(){ Code = ErrorCode.PlayerNotInBattle };

        var battle = ServerLogic.Instance.ActiveBattles[player.Username];
        battle.PlayerAction( request.GetValue(ParameterCode.CharacterInstanceID), 
                                    request.GetValue(ParameterCode.TargetCharacterInstanceID),
                                    request.GetValue(ParameterCode.ContentID)
        );
        var code = RequestCode.TurnRequest;

        var msg = new BattleMessage();
        msg.NextPlayerCharacter = (battle.CurrentCharacter != null) ? battle.CurrentCharacter.InstanceID : "";
        msg.CommandResults = battle.Commands;

        if (battle.CurrentStatus != BattleStatus.Playing)
        {
            msg.BattleEnd = new BattleEndMessage();           
            msg.BattleEnd.Win = battle.CurrentStatus == BattleStatus.Win;          

            battle.Destroy();        

            msg.BattleEnd.Account = player.AccountToClient();
            msg.BattleEnd.Materials = player.MaterialsToClient();
            msg.BattleEnd.Characters = player.CharactersToClient();

            code = RequestCode.BattleEnd;           
        }

        NetworkManager.SendMessage(player, code, msg);     

        if(code == RequestCode.BattleEnd)
            ServerLogic.Instance.FinishBattle(player.Username);       
        return null;
    }





    public static ErrorInfo OnPlayerForceWin( Player player )
    {
        if(!ServerLogic.Instance.ActiveBattles.ContainsKey(player.Username))
            return new ErrorInfo(){ Code = ErrorCode.PlayerNotInBattle };

        var battle = ServerLogic.Instance.ActiveBattles[player.Username];
        battle.ForceWin();

        var code = RequestCode.TurnRequest;
        var msg = new BattleMessage();

        if (battle.CurrentStatus != BattleStatus.Playing)
        {
            msg.BattleEnd = new BattleEndMessage();           
            msg.BattleEnd.Win = battle.CurrentStatus == BattleStatus.Win;          

            battle.Destroy();        

            msg.BattleEnd.Account = player.AccountToClient();
            msg.BattleEnd.Materials = player.MaterialsToClient();
            msg.BattleEnd.Characters = player.CharactersToClient();

            code = RequestCode.BattleEnd;           
        }

        NetworkManager.SendMessage(player, code, msg);     

        if(code == RequestCode.BattleEnd)
            ServerLogic.Instance.FinishBattle(player.Username);       
        return null;
    }
}
