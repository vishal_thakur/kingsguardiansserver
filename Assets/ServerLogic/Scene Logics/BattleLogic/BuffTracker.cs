﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuffTracker 
{
    List<BuffNode> _activeBuffs = new List<BuffNode>();

    class BuffNode
    {        
        public readonly BuffBase Buff;
        public readonly Character Caster;
        public readonly Character Target;
        public int Rounds;

        public BuffNode(BuffBase buff, Character caster, Character target, int duration)
        {
            Buff = buff;
            Caster = caster;
            Target = target;
            Rounds = duration;
        }
    }

    public void AddBuff(BuffBase buff, Character caster, Character target, int duration)
    {
        if (buff.Test(caster, target))
        {
            buff.Apply(target, caster, duration);
            _activeBuffs.Add(new BuffNode(buff, caster, target, duration));
        }    
    }

    public void RemoveBuff(Character caster, string ID)
    {
        foreach (var buff in _activeBuffs)
        {
            if (buff.Caster == caster && buff.Buff.ID == ID)
                buff.Rounds = -1;
        }
    }

    public void RemoveBuff(Character target, System.Type type)
    {
        if (!type.IsInstanceOfType(typeof(BuffBase)))
            return;

        foreach (var buff in _activeBuffs)
        {
            if (buff.Target == target && buff.Buff.GetType() == type)
                buff.Rounds = -1;
        }   
    }

    public int BuffCount()
    {
        return _activeBuffs.Count;
    }

    public int BuffCount (Character character, List<System.Type> types)
    {
        return _activeBuffs.FindAll(x => x.Target == character && types.Contains(x.Buff.GetType())).Count;
    }

    public bool HasBuff(Character character, System.Type type)
    {
        var b = _activeBuffs.Find(x => x.Target == character && x.Buff.GetType() == type);
        return b != null;
    }


    public void NextTurn(Character currentCharacter)
    {
        _activeBuffs.RemoveAll(x => x.Rounds < 0);
        foreach (var b in _activeBuffs)
        {
            if (b.Caster == currentCharacter)
            {
                b.Buff.NextTurn(b.Target, b.Caster);
                if( b.Rounds > 0)
                    b.Rounds--;
            }
        }
    }

    public void Clear()
    {
        _activeBuffs.Clear();
    }

    public List<CharacterBuffs> ToClient()
    {
        List<CharacterBuffs> list = new List<CharacterBuffs>();
        foreach (var b in _activeBuffs)
            list.Add(new CharacterBuffs(){ CharacterID = b.Target.InstanceID, ID = b.Buff.ID, Rounds = b.Rounds }); 
        return list;
    }

}
