﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class TownLogic 
{    	
    #region Worker functions
	public static ErrorInfo Construct( Player player, WallConstructRequest request )
	{
		List<string> removethese = new List<string>();
		List<WallResponse.Wall> walls = new List<WallResponse.Wall>();

		foreach (var w in request.Buildings)
		{
			// Get the wall fromt the catalog
			var wall = ServerLogic.Instance.GameContent.GetContent<BuildingContent>(w.ContentID);
			if (wall == null)
				return new ErrorInfo(){ Code = ErrorCode.MissingGameContent, ContentID = w.ContentID, ItemInstanceID = w.InstanceID };

			// Check materials
			if (!player.Materials.Subtract(wall.GetUpgrade(1).Cost))
				removethese.Add(w.InstanceID);
			else
			{
				// Create the building instance
				Building instance = new Building();
				instance.SetContentID(w.ContentID);
				instance.SetOwner(player);
				instance.Replace(w.Position, w.Rotation);
				instance.Active = true;

				player.Buildings.Add(instance);

				walls.Add(new WallResponse.Wall(){ TemporaryInstanceID = w.InstanceID, Building = instance.ToClient<BuildingInfo>() });
			}
		}

		NetworkManager.SendMessage(player, RequestCode.ConstructWall, new WallResponse()
			{
				Walls = walls,
				RemoveThese = removethese,
				Materials = player.Materials.Save(),
			});

		return null;
	}

    public static ErrorInfo Construct( Player player, ConstructRequest request )
    {        
		// Get building fromt the catalog
		var building = ServerLogic.Instance.GameContent.GetContent<BuildingContent>(request.ContentID);
		if (building == null)
			return new ErrorInfo(){ Code = ErrorCode.MissingGameContent, ContentID = request.ContentID, ItemInstanceID = request.InstanceID };
       
		if (request.ContentID != "Castle")
		{      
			// Check building Limits if it's not a castle
			int limit = building.MaxAmount[player.CastleLevel];
			if (player.BuildingCount(request.ContentID) == limit)
				return new ErrorInfo(){ Code = ErrorCode.BuildingLimit, ContentID = request.ContentID, ItemInstanceID = request.InstanceID };
		}

		// Get a free worker
        var worker = player.Workers.Find( x => x.Free);
        if (worker == null)
            return new ErrorInfo(){ Code = ErrorCode.NoFreeWorker, ContentID = request.ContentID, ItemInstanceID = request.InstanceID };

        // Check materials
        if (!player.Materials.Subtract(building.GetUpgrade(1).Cost))
            return new ErrorInfo(){ Code = ErrorCode.NotEnoughMaterial, ContentID = request.ContentID, ItemInstanceID = request.InstanceID };
      
        // Create the building instance
        Building instance = new Building();
        instance.SetContentID(request.ContentID);
        instance.SetOwner(player);
        instance.Replace(request.Position, request.Rotation);

        worker.AssignWork(instance, instance.Data.GetUpgrade(1).UpgradeTime, Works.Construction); 
        
        player.Buildings.Add(instance);

        NetworkManager.SendMessage(player, RequestCode.Construct, new BuildingResponse()
            {
                TemporaryInstanceID = request.InstanceID,
                Building =  instance.ToClient<BuildingInfo>(),
                Materials = player.Materials.Save(),
                Worker = (worker != null) ? worker.ToClient<WorkerInfo>() : null
            });

        return null;
    }

	public static ErrorInfo Move(Player player, WallConstructRequest request )
	{
		List<WallResponse.Wall> walls = new List<WallResponse.Wall>();

		foreach (var w in request.Buildings)
		{
			Building building = player.Buildings.GetByInstanceID(w.InstanceID);
			if( building == null )
				return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ContentID = w.ContentID, ItemInstanceID = w.InstanceID };

			building.Replace(w.Position, w.Rotation);

			walls.Add(new WallResponse.Wall(){ TemporaryInstanceID = w.InstanceID, Building = building.ToClient<BuildingInfo>() });
		}

		NetworkManager.SendMessage(player, RequestCode.ReplaceWall, new WallResponse()
			{
				Walls = walls,
				RemoveThese = new List<string>(),
				Materials = player.Materials.Save(),
			});

		return null;
	}


    public static ErrorInfo Move(Player player, ConstructRequest request )
    {
        // Get building fromt the catalog
        Building building = player.Buildings.GetByInstanceID(request.InstanceID);
        if( building == null )
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ContentID = request.ContentID, ItemInstanceID = request.InstanceID };

        building.Replace(request.Position, request.Rotation);
        NetworkManager.SendMessage(player, RequestCode.Replace, new BuildingResponse()
            {
                Building =  building.ToClient<BuildingInfo>(),
            });
        return null;
    }

    public static ErrorInfo Upgrade(Player player, SimpleMessage request)
    {
        // Get a free worker
        var worker = player.Workers.Find( x => x.Free);
        if (worker == null) 
            return new ErrorInfo(){ Code = ErrorCode.NoFreeWorker, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

        // Get building fromt the catalog
        Building building = player.Buildings.GetByInstanceID(request.GetValue(ParameterCode.InstanceID));
        if (building == null)
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

        // Check if the building have more upgrade
        if (building.Data.Upgrades.Length <= building.Upgrade)
            return new ErrorInfo(){ Code = ErrorCode.BuildingLimit, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

        // Check materials
        if (!player.Materials.Subtract( building.Data.GetUpgrade(building.Upgrade + 1).Cost))
            return new ErrorInfo(){ Code = ErrorCode.NotEnoughMaterial, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };
        
        worker.AssignWork(building, building.Data.GetUpgrade(building.Upgrade + 1).UpgradeTime, Works.Upgrade);

        NetworkManager.SendMessage(player, RequestCode.Upgrade, new BuildingResponse()
            {
                Building =  building.ToClient<BuildingInfo>(),
                Materials = player.Materials.Save(),
                Worker = worker.ToClient<WorkerInfo>()
            });
        return null;
    }

    public static ErrorInfo Repair (Player player, SimpleMessage request)
    {
        // Get a free worker
        var worker = player.Workers.Find( x => x.Free);
        if (worker == null) 
            return new ErrorInfo(){ Code = ErrorCode.NoFreeWorker, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

        // Get building fromt the player buildings
        Building building = player.Buildings.GetByInstanceID(request.GetValue(ParameterCode.InstanceID));
        if (building == null)
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

        // Check materials
        if (!player.Materials.Subtract(building.Data.RepairCost(building.Upgrade + 1)))
            return new ErrorInfo(){ Code = ErrorCode.NotEnoughMaterial, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };
     
        player.Materials.Subtract(building.Data.RepairCost(building.Upgrade + 1));
        worker.AssignWork(building, 30f, Works.Repair);

        NetworkManager.SendMessage(player, RequestCode.Repair, new BuildingResponse()
            {
                Building =  building.ToClient<BuildingInfo>(),
                Materials = player.Materials.Save(),
                Worker = worker.ToClient<WorkerInfo>()
            });
        return null;
    }  

	public static ErrorInfo AttachWarlock(Player player, SimpleMessage request)
	{
		// Check if there is a free worker
		var warlock = player.Warlocks.Find( x => x.Building == "" || x.Building == null);
		if (warlock == null) 
			return new ErrorInfo(){ Code = ErrorCode.NoFreeWarlock, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

		// Get building
		Building building = player.Buildings.GetByInstanceID(request.GetValue(ParameterCode.InstanceID));
		if (building == null)
			return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

		player.AttackWarlock(building.InstanceID);

		NetworkManager.SendMessage(player, RequestCode.AttachWarlock, new PlayerMessage()
			{                
				Warlocks = player.Warlocks
			});
		return null;
	}

	public static ErrorInfo DetachWarlock(Player player, SimpleMessage request)
	{
		// Get building
		Building building = player.Buildings.GetByInstanceID(request.GetValue(ParameterCode.InstanceID));
		if (building == null)
			return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

		player.DetachWarlock(building.InstanceID);

		NetworkManager.SendMessage(player, RequestCode.DetachWarlock, new PlayerMessage()
			{                
				Warlocks = player.Warlocks
			});
		return null;
	}
    #endregion

    #region Building functions
    public static ErrorInfo CreateWorker (Player player, RequestCode code)
    {
        WorkerType worker = (code == RequestCode.CreateWorker) ? WorkerType.Worker : WorkerType.Warlock;
        var castle = player.Buildings.Get("Castle");

        if (castle == null)
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ContentID = "Castle" };

        // Get the total number of worker or warlock that the player has
        int number = (worker == WorkerType.Worker) ? player.Workers.Count : player.Warlocks.Count;

        int cost = -1;

        // Get the price of the next worker/warlock
        for (int i = 0; i < castle.Upgrade; i++)
        {
            var c = (worker == WorkerType.Warlock) ? castle.Data.GetUpgrade(i).WarlockPrice : castle.Data.GetUpgrade(i).WorkerPrice;
            if (c >= 0 && number == 0)
            {
                cost = c;
                break;
            }
            number--;
        }
             

        // Check materials
        if (cost > 0 && !player.Materials.Has(Currency.Ether, cost))
            return new ErrorInfo(){ Code = ErrorCode.NotEnoughMaterial, ContentID = worker.ToString() };
        player.AddWorker(worker, cost);
                

        NetworkManager.SendMessage(player, code, new PlayerMessage()
            {                
                Materials = player.Materials.Save(),
                Workers = player.WorkersToClient(),
				Warlocks = player.Warlocks
            });

        return null;
    }

    public static ErrorInfo Mine(Player player, SimpleMessage request)
    {
        // Get building from the catalog
        Building building = player.Buildings.GetByInstanceID(request.GetValue(ParameterCode.InstanceID));
        if (building == null)
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ContentID = request.GetValue(ParameterCode.ContentID), ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

        if (building.Category != BuildingCategory.Resource_mine)
            return new ErrorInfo(){ Code = ErrorCode.WrongItemType, ContentID = request.GetValue(ParameterCode.ContentID), ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

        // Check materials
        if (!player.Materials.Has(building.MiningPrices))
            return new ErrorInfo(){ Code = ErrorCode.NotEnoughMaterial, ContentID = request.GetValue(ParameterCode.ContentID), ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };
        
        building.Create();

        NetworkManager.SendMessage(player, RequestCode.Mine, new BuildingResponse()
            {
                Building =  building.ToClient<BuildingInfo>(),
                Materials = player.Materials.Save(),
            });
        return null;
    }

    public static ErrorInfo Craft(Player player, SimpleMessage request)
    {        
        // Get building fromt the catalog
        Building building = player.Buildings.GetByInstanceID(request.GetValue(ParameterCode.InstanceID));
        if (building == null)
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ContentID = request.GetValue(ParameterCode.ContentID), ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

        if (building.Category != BuildingCategory.CardCreator && building.Category != BuildingCategory.CharacterCreator)
            return new ErrorInfo(){ Code = ErrorCode.WrongItemType, ContentID = request.GetValue(ParameterCode.ContentID), ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

        GameItemContent item = null;
        if (building.Category == BuildingCategory.CardCreator)
            item = ServerLogic.Instance.GameContent.GetContent<ItemContent>(request.GetValue(ParameterCode.ContentID));
        else if (building.Category == BuildingCategory.CharacterCreator)
            item = ServerLogic.Instance.GameContent.GetContent<CharacterContent>(request.GetValue(ParameterCode.ContentID));
        
        if (item == null)
            return new ErrorInfo(){ Code = ErrorCode.MissingGameContent, ContentID = request.GetValue(ParameterCode.ContentID), ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };     

        if( Utils.IsInherited(item.GetType(), typeof(ItemContent)) && ((ItemContent)item).BuildingLevelReq > building.Upgrade)
            return new ErrorInfo(){ Code = ErrorCode.LockedItem, ContentID = request.GetValue(ParameterCode.ContentID), ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };     

        // Check materials
        if (!player.Materials.Has(item.Price))
            return new ErrorInfo(){ Code = ErrorCode.NotEnoughMaterial, ContentID = request.GetValue(ParameterCode.ContentID), ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };
        
        building.Create(item);

        if(item.CraftTime > 0)
            NetworkManager.SendMessage(player, RequestCode.Craft, new BuildingResponse()
                {
                    Building = building.ToClient<BuildingInfo>(),
                    Materials = player.Materials.Save(),
                });
        return null;
    }

    public static ErrorInfo Cancel(Player player, SimpleMessage request, RequestCode code)
    {
        if (code == RequestCode.CancelWorker)
        {
            var w = player.Workers.Find(x => x.InstanceID == request.GetValue(ParameterCode.InstanceID));
            if (w == null) 
                return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };
            
            var building = w.ActiveWork.Building;
            if( building == null)
                return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };
            
            w.Cancel();

            NetworkManager.SendMessage(player, RequestCode.CancelWorker, new BuildingResponse()
                {
                    Building =  building.ToClient<BuildingInfo>(),
                    Materials = player.Materials.Save(),
                    Worker = w.ToClient<WorkerInfo>()
                });

        }
        else if ( code == RequestCode.CancelBuilding)
        {
            Building building = player.Buildings.Get(request.GetValue(ParameterCode.InstanceID));
            if( building == null)
                return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };

            if (building.Category != BuildingCategory.Resource_mine && building.Category != BuildingCategory.CardCreator && 
                building.Category != BuildingCategory.CharacterCreator)
                return new ErrorInfo(){ Code = ErrorCode.WrongItemType, ItemInstanceID = request.GetValue(ParameterCode.InstanceID) };
            
            building.Cancel();

            NetworkManager.SendMessage(player, RequestCode.CancelWorker, new BuildingResponse()
                {
                    Building =  building.ToClient<BuildingInfo>(),
                    Materials = player.Materials.Save(),
                });   
        }

        return null;
    }
    #endregion

    #region Character Functions
    public static ErrorInfo EquipItem(Player player, SimpleMessage request, RequestCode code )
    {
        Character c = player.Characters.Find(x => x.InstanceID == request.GetValue(ParameterCode.CharacterInstanceID));
        if( c == null )
            return new ErrorInfo(){ Code = ErrorCode.UnknownError, ItemInstanceID = request.GetValue(ParameterCode.CharacterInstanceID) };

        ErrorInfo error = null;
        if (code == RequestCode.Equip)
            error = Equip(player, c, request.GetValue(ParameterCode.ContentID), int.Parse(request.GetValue(ParameterCode.ItemSlot)));
        else
            error = Unequip(player, c, request.GetValue(ParameterCode.ContentID));

        if (error != null)
            return error;
        
        NetworkManager.SendMessage(player, code, new PlayerMessage(){
            Inventory = player.InventoryToClient(),
            Characters = player.CharactersToClient(c.InstanceID),
        });
        return null;
    }

    static ErrorInfo Equip(Player player, Character c, string contentID, int slot)
    {
        Debug.Log("Equip item " + contentID + " " + c.ContentID + " slot: " + slot);

        var item = player.Inventory.Get(contentID);
        if (item == null)
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ContentID = contentID };

        if (c.Equip(item, slot))
            player.Inventory.Remove(contentID);      
        else
            return new ErrorInfo(){ Code = ErrorCode.UnknownError, ContentID = contentID };
        return null;
    }

    static ErrorInfo Unequip(Player player, Character c, string contentID)
    {
        Item item = c.Unequip(contentID);
        if (item == null)
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ContentID = contentID };

        player.Inventory.Add(item);
        return null;
    }  


    public static ErrorInfo UpgradeCharacter(Player player, SimpleMessage request)
    {
        Character c = player.Characters.Find(x => x.InstanceID == request.GetValue(ParameterCode.CharacterInstanceID));
        if( c == null )
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = request.GetValue(ParameterCode.CharacterInstanceID) };

        CharacterStats stat = (CharacterStats)System.Enum.Parse(typeof(CharacterStats), request.GetValue(ParameterCode.CharacterStat));
        c.Stats.AddUpgrade(stat, 10);

        NetworkManager.SendMessage(player, RequestCode.UpgradeCharacter, new CharacterResponse()
            {
                Character = c.ToClient<CharacterInfo>()
            });
        return null;
    }

    public static ErrorInfo ChangeSquad(Player player, SimpleMessage request)
    {
        int slot = int.Parse(request.GetValue(ParameterCode.SquadSlot));

        Character c = player.Characters.Find(x => x.InstanceID == request.GetValue(ParameterCode.CharacterInstanceID));
        if( c == null )
            return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = request.GetValue(ParameterCode.CharacterInstanceID) };

        if( slot >= Settings.SQUAD_SIZE)
            return new ErrorInfo(){ Code = ErrorCode.UnknownError, ItemInstanceID = request.GetValue(ParameterCode.CharacterInstanceID) };

        if(player.Squad.Find( x => x != null && x == c.InstanceID) != null)
            return new ErrorInfo(){ Code = ErrorCode.AlreadyExists, ItemInstanceID = request.GetValue(ParameterCode.CharacterInstanceID) };
        
        player.SetSquad(c, slot);

        NetworkManager.SendMessage(player, RequestCode.ChangeSquad, new PlayerMessage(){ Squad = player.SquadToClient() });
        return null;
    }


    #endregion


	public static ErrorInfo Collect(Player player, SimpleMessage request)
	{
		bool isBuilding = request.GetValue(ParameterCode.BoolValue) == "true";
		string id = request.GetValue(ParameterCode.InstanceID);

		if (isBuilding)
		{
			Building building = player.Buildings.GetByInstanceID(request.GetValue(ParameterCode.InstanceID));
			if (building == null)
				return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned, ItemInstanceID = id };
			if(building.Category != BuildingCategory.Resource_generator)
				return new ErrorInfo(){ Code = ErrorCode.WrongItemType, ItemInstanceID = id };
			if(!building.Collect())
				return new ErrorInfo(){ Code = ErrorCode.UnknownError, ItemInstanceID = id };

			NetworkManager.SendMessage(player, RequestCode.Mine, new BuildingResponse()
				{
					Building = building.ToClient<BuildingInfo>(),
					Materials = player.MaterialsToClient()
				});
		}
		else
		{
			if (!player.CollectRemain(id))
				return new ErrorInfo(){ Code = ErrorCode.UnknownError, ItemInstanceID = id };

			NetworkManager.SendMessage(player, RequestCode.Collect, new MaterialUpdateMessage()
				{
					Materials = player.MaterialsToClient()
				});
		}
		return null;
	}



    public static ErrorInfo GetOpponents(Player player)
    {
        List<PlayerInfo> list = new List<PlayerInfo>();
        foreach (var p in ServerLogic.Instance.PlayerDatabase.GetPlayers())
        {
            if( p.Username != player.Username)
                list.Add(p.AccountToClient(true));
        }

        NetworkManager.SendMessage(player, RequestCode.GetOpponents, new PlayerListResponse()
            {
                Players = list
            });

        return null;
    }


    public static ErrorInfo GetOtherPlayer(Player player, SimpleMessage message)
    {
        Player p = ServerLogic.Instance.PlayerDatabase.GetPlayer(message.GetValue(ParameterCode.Username));
        if (p == null)
            return new ErrorInfo(){ Code = ErrorCode.InvalidUsernameOrPassword, ContentID = message.GetValue(ParameterCode.Username)};

        NetworkManager.SendMessage(player, RequestCode.GetOtherPlayer, new PlayerMessage()
            {
                Account = p.AccountToClient(),
                Buildings = p.BuildingsToClient(),
                Characters = p.CharactersToClient(),
                Inventory = p.InventoryToClient(),
                Materials = p.MaterialsToClient(),
                Squad = p.SquadToClient(),
                Workers = p.WorkersToClient(),
				Warlocks = p.Warlocks
            });      
        return null;
    }
}
