﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public enum Works : int { Construction, Upgrade, Repair }

public class Work : BaseProcess
{
    public Works Type { get; private set; }
    public Building Building { get; private set; }

    public Work( Player owner) : base (owner, null, 0f, 0){}

    public Work( Building building, float duration, Works work) : base (building.PlayerOwner, building.Price, duration, 0)
    {
        Building = building;
        Type = work;
        Building.Active = false;
    }

    protected override void OnCancel()
    {
        if (Type == Works.Construction)
        {
            Player p = Building.PlayerOwner;
            p.Buildings.Remove(Building.InstanceID);
        } 

        Building.Active = true;
    }

    protected override void OnFinished()
    {
        Building.Active = true;

        if (Type == Works.Repair)
            Building.AdjustHP(Building.MaxHP - Building.CurrentHP);
        if (Type == Works.Upgrade)
            Building.AddUpgrade(); 
        if (Type == Works.Construction)
            Building.OnConstructed();
    }

    public override QueueData Serialize()
    {
        var data = base.Serialize();
        data.ItemContentID = Building.InstanceID;
        data.EnumData = (int)Type;
        return data;
    }

    public override void Deserialize(QueueData data)
    {
        base.Deserialize(data);
        Type = (Works)data.EnumData;
        Building = player.Buildings.Get(data.ItemContentID);      
    }

    public override QueueInfo ToClient()
    {
        var data = base.ToClient();
        data.ItemContentID = Building.InstanceID;
        data.EnumData = (int)Type;
        return data;
    }

    public override string ToString()
    {
        return "Work: " + Building.InstanceID + " [" + Type + "] (" + TimeLeft + " seconds)";
    }
}
