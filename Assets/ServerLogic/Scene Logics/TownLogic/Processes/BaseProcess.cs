﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Globalization;

public class BaseProcess
{
    public float TimeLeft
    {
        get
        {
            var now = DateTime.Now.Ticks;
            long elapsedTicks = now - _start.Ticks;
            TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
            var total = elapsedSpan.TotalSeconds;
            var x = duration - (float)total;
            return Mathf.Max( 0,x );
        }
    }

    protected Player player;
    protected int amount;  

    protected float duration { get {  return _duration * Settings.CreationTimeScale; }}

    List<SMaterial> _costs = new List<SMaterial>();
    DateTime _start;
    float _duration;

    public BaseProcess( Player player, List<SMaterial> costs, float duration, int amount)
    {
        if (duration > -1)
            Start(duration);
        this.amount = amount;
        _costs = costs;
        this.player = player;
    }

    public void Start(float duration)
    {
        _start = DateTime.Now;
        _duration = duration;
        OnStarted();
    }

    public bool Finish()
    {
        if (!CheckFinish())
            return false;
		
        OnFinished();
        return true;
    } 

    public void Cancel()
    {
        player.Materials.Add(_costs);
        OnCancel();
    }

    protected virtual bool CheckFinish(){ return true; }

    protected virtual void OnStarted(){}
    protected virtual void OnCancel(){}
    protected virtual void OnFinished(){}




    public virtual QueueData Serialize()
    {
        var data = new QueueData();
        data.StartTime = _start.ToString(ServerLogic.DATE_TIME_FORMAT);
        data.Duration = _duration;
        data.Costs = _costs;
        data.Amount = amount;
        return data;
    }

    public virtual void Deserialize(QueueData data)
    {
        _start = DateTime.Parse(data.StartTime);
        _duration = data.Duration;
         _costs = data.Costs;
        amount = data.Amount;
    }

    public virtual QueueInfo ToClient()
    {
        var data = new QueueInfo();
        data.StartTime = _start.ToString(ServerLogic.DATE_TIME_FORMAT);
        data.Duration = duration;
        data.Costs = _costs;
        data.Amount = amount;

    //    Debug.LogFormat("Queue to Client: {0} amount, {1} duration", amount, duration); 

        return data;
    }

    /// <summary>
    /// Returns a <see cref="System.String"/> that represents the current <see cref="BaseProcess"/>.
    /// For testing.
    /// </summary>
    /// <returns>A <see cref="System.String"/> that represents the current <see cref="BaseProcess"/>.</returns>
    public virtual string ToString()
    {
        return "Base Process: "+TimeLeft+" seconds";
    }
}
