﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CraftProcess : BaseProcess
{
    public GameItemContent Item { get; private set; }
    public int ItemCat { get; private set; }   // -1 means Character

    public CraftProcess( Player player, GameItemContent item, int amount, float duration = -1) 
        : base (player, (item != null) ? item.Price : new List<SMaterial>() , duration, amount)
    {
        if (item == null)
            return;
        Item = item;

        if (item.GetType() == typeof(ItemContent) || item.GetType().IsSubclassOf(typeof(ItemContent)))
        {
            ItemCat = (int)((ItemContent)item).Category;
        }
        else if (item.GetType() == typeof(CharacterContent))
            ItemCat = -1;
    }

    protected override bool CheckFinish()
    {     
        if (ItemCat == -1) return true;     
        return player.Inventory.FreeSlot;
    }

    protected override void OnFinished()
    {
        if (ItemCat == -1)
            player.AddCharacter(Character.Create(Item.ContentID));
        else
        {
            ItemCategory c = (ItemCategory)ItemCat;
            switch (c)
            {
                case ItemCategory.Defence: 
                    DefenseEquipment d = new DefenseEquipment();
                    d.SetContentID(Item.ContentID);
                    player.Inventory.Add(d);
                    break;

                case ItemCategory.Weapon: 
                    WeaponEquipment w = new WeaponEquipment();
                    w.SetContentID(Item.ContentID);
                    player.Inventory.Add(w);
                    break;

                default :
                    CombatCard cc = new CombatCard();
                    cc.SetContentID(Item.ContentID);
                    player.Inventory.Add(cc);
                    break;
            }
        }     
    }






    public override QueueData Serialize()
    {
        var data = base.Serialize();
        data.ItemContentID = Item.ContentID;
        data.EnumData = ItemCat;
        return data;
    }

    public override void Deserialize(QueueData data)
    {
        base.Deserialize(data);

        Item = ServerLogic.Instance.GameContent.GetContent<ItemContent>(data.ItemContentID);

     //   if (ServerLogic.Instance.GameContent.Items.ContainsKey(data.ItemContentID))
      //      Item = ServerLogic.Instance.GameContent.Items[data.ItemContentID];
        ItemCat = data.EnumData;
    }

    public override QueueInfo ToClient()
    {
        var data = base.ToClient();
        data.ItemContentID = Item.ContentID;
        data.EnumData = ItemCat;
        return data;
    }


    public override string ToString()
    {
        return "Craft Process: "+ Item.ContentID + " ("+TimeLeft+" seconds)";
    }

}