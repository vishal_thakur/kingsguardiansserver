﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MineProcess : BaseProcess
{
    Building _building;

    public MineProcess( Player player, Building building,  List<SMaterial> costs, int amount, float duration = -1) 
        : base (player, costs, duration, amount)
    {
        _building = building;
    }

    protected override bool CheckFinish()
    {               
        return _building.StoredMaterials + amount <= _building.Storage;
    }

    protected override void OnFinished()
    {
        player.Materials.Add(_building.Material, amount);
    }


    public override string ToString()
    {
        return "MineProcess: " + amount + " " + _building.Material + " ("+TimeLeft+" seconds)";
    }

}