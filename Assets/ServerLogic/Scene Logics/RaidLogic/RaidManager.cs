﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GridSystem;

public class RaidManager
{	
	public int AbilityPoint { get; set; }
	public int DestroyedBuildingCount { get { return Units.FindAll( x => x.UnitType == UnitType.Building && x.Destroyed).Count;}}
   	public Player Player { get { return _player; } }

	List<IRaidUnit> Units = new List<IRaidUnit>();

	Player _player;
    Player _enemy;  
	Outcome _outcome;

	List<IRaidUnit> _queueToSpawn = new List<IRaidUnit>();
	List<string> _spawnedCharacters = new List<string>();

	bool _firstEventSent;

    public RaidManager( Player player, Player enemy)
    {        
        _player = player;
        _enemy = enemy;  
		_outcome = new Outcome(Time.time, _player.Characters.Count);

		AbilityPoint = 5000;

        foreach (var b in enemy.Buildings.Items)
        {
            Building copy = new Building();
            copy.Load(b.Save());   

			if (b.Category == BuildingCategory.Defense)
                Units.Add(new DefenseUnit(copy, this));
			else
				Units.Add(new BuildingUnit(copy, this));
			
			GridHandler.Instance.AddObstacle(b.ContentID, Footprint.GetPoints(b.Position, b.Size));
        }
        ServerLogic.Instance.UpdateDelegates += Update;        
    }

    void Update()
    {
		if (!_firstEventSent && _player != null)
		{
			SimpleMessage msg = new SimpleMessage();
			msg.Add(ParameterCode.AbilityPoints, AbilityPoint.ToString());
			NetworkManager.SendMessage(_player, EventCode.AbilityPointUpdate, msg);   
			_firstEventSent = true;
		}


		if (_outcome == null || _outcome.Finished) return;
		if (_outcome.IsFinished(Units))
			_outcome.CalculateReward(_player, _enemy, Units);          

		else
        {     
			if (_queueToSpawn.Count > 0)
			{
				foreach(var u in _queueToSpawn)
					Units.Add(u);
				_queueToSpawn.Clear();
			}

            // Call Character and Defense Building updates
			foreach (var p in Units)
			{
				if(Utils.IsInherited(p.GetType(), typeof(RaidUnit)))
					((RaidUnit)p).Update();
			}

            // Remove the destroyed buildings from the obstacle list!
			foreach (var b in Units.FindAll( x => x.UnitType == UnitType.Building && x.Destroyed))
			{
				Building building = null;
				if (b.GetType() == typeof(BuildingUnit))
					building = ((BuildingUnit)b).Building;
				else
					building = ((DefenseUnit)b).Building;

				GridHandler.Instance.RemoveObstacle(Footprint.GetPoints(building.Position, building.Size));       
			 }
        }
    }


	public List<IRaidUnit> UnitsInRange (List<UnitType> enemyUnitTypes, bool friend, Point center, int range, bool landTarget, bool airTarget, List<string> exludedInstanceIDs = null)
    {
		if (exludedInstanceIDs == null) exludedInstanceIDs = new List<string>();

		List<IRaidUnit> list = new List<IRaidUnit>();
		foreach (var unit in Units)
        {    
			// If we can't target this type of unit
			if (enemyUnitTypes.Contains(unit.UnitType) == friend)
				continue;
			// Check if the unit type is one that we want to target
			if(unit.AirUnit && !airTarget || !unit.AirUnit && !landTarget)
				continue;

			if (unit.ContentID == "Landmine")
				continue;

			if (!exludedInstanceIDs.Contains(unit.InstanceID) && IsUnitTargetable(unit))
			{
				var d = GridHandler.ManhattanDistance(unit.Position, center);
				if (d <= range)
					list.Add(unit);              
			}
		}      
        return list;
    }


	public bool IsUnitTargetable(string instanceID)
	{
		return IsUnitTargetable(GetUnit(instanceID));
	}

	public bool IsUnitTargetable(IRaidUnit unit)
	{
		return unit != null && !unit.Destroyed;
	}



	#region Character functions
    public bool IsCharacterSpawned(string instanceID) 
	{
		return _spawnedCharacters.Contains(instanceID);
	}

	public T Spawn<T>(Character c, Point point) where T : RaidUnit
	{
		if (typeof(T) == typeof(CharacterUnit))
		{
			if(IsCharacterSpawned(c.InstanceID)) return null;   
			_spawnedCharacters.Add(c.InstanceID);
		}

		Character copy = new Character(c.ContentID);
		copy.Load(c.Save());
		copy.SetOwner(_player);

		var unit = (T)System.Activator.CreateInstance(typeof(T), new object[]{ _player, copy, this, point });
		_queueToSpawn.Add(unit);
		return unit;
	}

	public void SetTarget(string unitInstanceID, string targetInstanceID)
	{
		var unit = GetCharacter(unitInstanceID);
		var target = GetUnit(targetInstanceID);
		unit.HandleTarget(target);   
	}

	public bool OnDestinationReached(string unitInstanceID)
	{
		var unit = GetCharacter(unitInstanceID);
		return unit.DestinationArrivalAllowed;
	}

	public void StartAttack(string unitInstanceID)
	{
		Debug.Log("RaidManager Start Attack");
		var unit = GetCharacter(unitInstanceID);
		unit.StartAttack();
	}

	public bool IsCharacterBlocked(string unitInstanceID)
	{
		var unit = GetCharacter(unitInstanceID);
		if( unit != null)
			return unit.MagicBlocked;
		return true;
	}

	public ActiveAbility GetAbility(string unitInstanceID, int ability)
	{
		var unit = GetCharacter(unitInstanceID);
		if( unit != null)
			return unit.GetAbility(ability);        
		return null;
	}

	public bool EnoughPoint(ActiveAbility ability)
	{
		return AbilityPoint >= ability.Cost;
	}

	public void ActivateAbility(string unitInstanceID, ActiveAbility ability)
	{
		var unit = GetCharacter(unitInstanceID);
		if( unit != null)
			unit.ActivateAbility(ability);   

		var msg = new SimpleMessage();
		msg.Add(ParameterCode.AbilityPoints, AbilityPoint.ToString());
		NetworkManager.SendMessage(Player, EventCode.AbilityPointUpdate, msg );
	}

	public IRaidUnit GetUnit(string instanceID)
	{
		return Units.Find(x => x.InstanceID == instanceID);
	}

	CharacterUnit GetCharacter(string instanceID)
	{
		if (IsCharacterSpawned(instanceID))
			return (CharacterUnit)GetUnit(instanceID);
		return null;
	}
	#endregion



  

   

  
    public void Destroy()
    {
        // Save the enemies building status.
		foreach (var u in Units)
        {
			if (u.UnitType == UnitType.Building)
			{
				Building tempBuilding = null;
				if( u.GetType() == typeof(BuildingUnit))
					tempBuilding = ((BuildingUnit)u).Building;
				else if( u.GetType() == typeof(DefenseUnit))
					tempBuilding = ((DefenseUnit)u).Building;

				if (tempBuilding != null)
				{
					var building = _enemy.Buildings.GetByInstanceID(u.InstanceID);
					if (building.CurrentHP > tempBuilding.CurrentHP)
						building.Load(tempBuilding.Save());
				}
			}

			if (u.UnitType == UnitType.Character)
			{
				if (u.GetType() == typeof(CharacterUnit))
				{
					var tempCharacter = ((CharacterUnit)u).Character;
					var character = _player.Characters.Find(x => x.InstanceID == u.InstanceID);
					character.Load(tempCharacter.Save());
				}
			}         
        }

        ServerLogic.Instance.UpdateDelegates -= Update;
    }



	public bool Finished { get { return _outcome.Finished; } }
//	public string GetAllRewards() { return _outcome.GetAllRewards(_player, _enemy); }
	public string GetAllRewards() { return _outcome.GetAllRewards(_player, _enemy); }
   


	public void SendSummonSpawn(SummonedUnit summon)
	{
		CustomCharacterSpawn r = new CustomCharacterSpawn();
		r.Character = summon.Character.ToClient<CharacterInfo>();;
		r.Position = summon.Position;
		NetworkManager.SendMessage(_player, EventCode.CustomCharacterSpawn, r);     
	}
}
