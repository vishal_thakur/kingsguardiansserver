﻿using UnityEngine;
using System.Collections;
using GridSystem;

public class BuildingUnit : IRaidUnit
{
	public readonly Building Building;
	readonly RaidManager _manager;

	public BuildingUnit(Building building, RaidManager manager)
	{
		Building = building;
		if (manager.Player.Warlocks.Find(x => x.Building == building.InstanceID) != null)
			Building.AdjustShield(building.MaxHP);
		_manager = manager;
	}


	#region IRaidUnit implementation
	public string ContentID{ get { return (Building != null) ? Building.ContentID : "unknown"; }}
	public string InstanceID { get { return (Building != null) ? Building.InstanceID : "unknown"; }}
	public UnitType UnitType { get { return UnitType.Building; } }
	public Point Position { get { return (Building != null) ? Building.Position : Point.zero; }}
	public int MaxHP { get { return (Building != null) ? Building.MaxHP : 0; }}
	public int CurrHP { get { return (Building != null) ? Building.CurrentHP : 0; }}
	public bool Destroyed { get { return (Building != null) ? Building.CurrentHP == 0 : false; }}
	public bool AirUnit { get { return false; } }

	public Point GetClosestTile(Point position)
	{
		if (Building == null) return null;		
		return Footprint.ClosestCorner(position, Building.Position, Building.Size);
	}

	public void Heal(int amount)
	{
		if (Building == null) return;
		Building.AdjustHP(amount);
		DamageTakenMessage(true, -amount, Element.Untyped);
	}

	public void TakeDamage(int amount, Element e = Element.Untyped)
	{
		if (Building == null) return;
		Building.AdjustHP(-amount);
		DamageTakenMessage(true, amount, e);

	}
	#endregion


	protected void DamageTakenMessage(bool hit, int amount, Element e)
	{
		var msg = new RaidDamageTakenMessage() {
			InstanceID = InstanceID,
			AttackResult = (hit) ? AttackResult.Hit : AttackResult.Miss ,
			Damage = amount,
			DamageType = e,
			CurrentHP = CurrHP,
			CurrentShield = Building.Shield
		};
		NetworkManager.SendMessage(_manager.Player, EventCode.DamageTaken, msg);
	}




}
