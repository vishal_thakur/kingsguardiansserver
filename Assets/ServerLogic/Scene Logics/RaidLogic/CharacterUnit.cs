﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;
using System;

public class CharacterUnit : CharacterUnitBase
{
	public bool MagicBlocked { get; set; }

	protected override List<UnitType> EnemyUnits { get { return new List<UnitType>(){ UnitType.Building, UnitType.BreedingPit }; } }


	Player _player;
	IRaidUnit _originalTarget;

	public CharacterUnit(Player player, Character character, RaidManager manager, Point position) : base (character, manager, position)
	{
		_player = player;  	
		ChangeState(State.Idle);
	}

	public void HandleTarget(IRaidUnit target)
	{
		_originalTarget = target;
		SetTarget(target);
	}

	protected override IRaidUnit SearchTarget()
	{
		if (_originalTarget != null && manager.IsUnitTargetable(_originalTarget))
			return _originalTarget;
		else
		{
			if (_originalTarget != null)
				_originalTarget = null;
			return base.SearchTarget();
		}
	}


	public ActiveAbility GetAbility(int ability)
	{
		if (Character.Data.ActiveAbilities.Count < ability || ability < 0)
			return null;
		
		return Character.Data.ActiveAbilities[ability];
	}

	public void ActivateAbility(ActiveAbility a)
    {		
		manager.AbilityPoint -= a.Cost;
		
		if (a.Target == TargetType.Self)
			AddTrait(a.Name, a.Trait, a.Duration);
		else
		{
			Aura aura = null;
			switch (a.AuraType)
			{
				case AuraType.HPAdjust:
					aura = new HPAdjustAura(a.Amount, this, a.Duration, a.Radius);
					break;
				case AuraType.TraitGranting: 
					aura = new TraitGrantingAura(a.Trait, this, a.Duration, a.Radius);
					break;
				case AuraType.Combined:
					aura = new CombinedAura(a.Amount, a.Trait, this, a.Duration, a.Radius);
					break;
			}

			if(aura != null)
				aura.Targets = a.Target;
			SetAura(aura);
		}

		int ability = Character.Data.ActiveAbilities.IndexOf(a);
		ServerLogic.Instance.StartCoroutine(AbilityDeactivated(_player, Character.InstanceID, ability, a.Duration));
    }

	IEnumerator AbilityDeactivated(Player p, string instanceID, int ability, float delayTime)
	{
		yield return new WaitForSeconds(delayTime);
		RaidLogic.SendAbilityEvent(_player, Character.InstanceID, ability, false);
	}
    
  }
