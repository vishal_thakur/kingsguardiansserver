﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;
using System;

public class SummonedUnit : CharacterUnitBase
{
    Player _player;
    
    /// <summary>
    /// Initializes a new instance of the <see cref="CharacterAI"/> class.
    /// </summary>
    /// <param name="character">Character.</param>
    /// <param name="enemy">Enemy.</param>
    /// <param name="position">Position.</param>
	public SummonedUnit(Player player, Character character, RaidManager manager, Point position) : base (character, manager, position)
    {
        _player = player;  	
		ChangeState(State.Ready);
    }



	protected override void OnUpdate()
	{
		// Prevent to do aura and trait update
	}


  
  }
