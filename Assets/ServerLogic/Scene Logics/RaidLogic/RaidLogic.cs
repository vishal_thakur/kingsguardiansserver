﻿using UnityEngine;
using System.Collections;
using GridSystem;
using UnityEngine.Networking.NetworkSystem;

public class RaidLogic
{
    public static ErrorInfo Start( Player player, SimpleMessage msg )
    {
        if( ServerLogic.Instance.ActiveRaids.ContainsKey(player.Username))
            return new ErrorInfo(){ Code = ErrorCode.AlreadyInRaid };

        string enemyUsername = msg.GetValue(ParameterCode.Username);
        Player enemy = ServerLogic.Instance.PlayerDatabase.GetPlayer(enemyUsername);  
        if( enemy == null)
            return new ErrorInfo(){ Code = ErrorCode.EnemyDoesNotExists, ContentID = enemyUsername };

		var raid = new RaidManager(player, enemy);
        ServerLogic.Instance.ActiveRaids.Add(player.Username, raid);
        return null;
    }

    public static ErrorInfo SpawnCharacter(Player player, SimpleMessage msg)
    {
        if(!ServerLogic.Instance.ActiveRaids.ContainsKey(player.Username))
            return new ErrorInfo(){ Code = ErrorCode.PlayerNotInRaid };

        Character c = player.Characters.Find(x => x.InstanceID == msg.GetValue(ParameterCode.CharacterInstanceID));
        if( c == null ) return new ErrorInfo(){ Code = ErrorCode.ItemNotOwned };
        if( c.Dead) return new ErrorInfo(){ Code = ErrorCode.CharacterIsDead }; 

        Point point = Point.Parse(msg.GetValue(ParameterCode.Position));

        var raid = ServerLogic.Instance.ActiveRaids[player.Username];
        if( raid.Spawn<CharacterUnit>(c, point) == null)
            return new ErrorInfo(){ Code = ErrorCode.CharacterAlreadySpawned };         

        NetworkManager.SendMessage(player, RequestCode.SpawnCharacter, msg);     
        return null;
    }

    public static ErrorInfo TargetBuilding(Player player, SimpleMessage msg)
    {
        if(!ServerLogic.Instance.ActiveRaids.ContainsKey(player.Username))
            return new ErrorInfo(){ Code = ErrorCode.PlayerNotInRaid };

        var raid = ServerLogic.Instance.ActiveRaids[player.Username];

        string characterID = msg.GetValue(ParameterCode.CharacterInstanceID);
        string buildingID = msg.GetValue(ParameterCode.InstanceID);

        if (!raid.IsCharacterSpawned(characterID))
            return new ErrorInfo(){ Code = ErrorCode.CharacterNotSpawned };  
        
        if (!raid.IsUnitTargetable(buildingID))
            return new ErrorInfo(){ Code = ErrorCode.InvalidTarget }; 

		raid.SetTarget(characterID, buildingID); 
        return null;       
    }

	public static ErrorInfo OnDestinationReached(Player player, SimpleMessage msg)
	{
		if(!ServerLogic.Instance.ActiveRaids.ContainsKey(player.Username))
			return new ErrorInfo(){ Code = ErrorCode.PlayerNotInRaid };

		var raid = ServerLogic.Instance.ActiveRaids[player.Username];
		string characterID = msg.GetValue(ParameterCode.CharacterInstanceID);

		if (!raid.IsCharacterSpawned(characterID))
			return new ErrorInfo(){ Code = ErrorCode.CharacterNotSpawned };  

		if (!raid.OnDestinationReached(characterID))
			return null;

		raid.StartAttack(characterID);

		NetworkManager.SendMessage(player, RequestCode.DestinationReached, msg); 
		return null;
	}

	public static ErrorInfo ActivateRaidAbility(Player player, SimpleMessage msg)
    {
        if(!ServerLogic.Instance.ActiveRaids.ContainsKey(player.Username))
            return new ErrorInfo(){ Code = ErrorCode.PlayerNotInRaid };

        var raid = ServerLogic.Instance.ActiveRaids[player.Username];

        string characterID = msg.GetValue(ParameterCode.CharacterInstanceID);
        int ability = int.Parse(msg.GetValue(ParameterCode.AbilityNumber));
        if (!raid.IsCharacterSpawned(characterID)) return new ErrorInfo(){ Code = ErrorCode.CharacterNotSpawned };  

		// Magic Block
		if(raid.IsCharacterBlocked(characterID))  return new ErrorInfo(){ Code = ErrorCode.CantUseAbility };  
		var a = raid.GetAbility(characterID, ability);
		if( a == null ) return new ErrorInfo(){ Code = ErrorCode.CantUseAbility };  
		if(!raid.EnoughPoint(a))  return new ErrorInfo(){ Code = ErrorCode.NotEnoughActionPoint };  

		raid.ActivateAbility(characterID, a);

		msg.Add(ParameterCode.Action, "activate");
        NetworkManager.SendMessage(player, RequestCode.ActivateRaidAbility, msg);     
        return null;       
    }

	public static void SendAbilityEvent(Player player, string instanceID, int ability, bool activate)
	{
		var msg = new SimpleMessage();
		msg.Add(ParameterCode.CharacterInstanceID, instanceID);
		msg.Add(ParameterCode.AbilityNumber, ability.ToString());
		msg.Add(ParameterCode.Action, (activate) ? "activate" : "");

		NetworkManager.SendMessage(player, EventCode.RaidAbilityEvent, msg);     
	}


    public static ErrorInfo GetAllRewards(Player player)
    {
        if(!ServerLogic.Instance.ActiveRaids.ContainsKey(player.Username))
            return new ErrorInfo(){ Code = ErrorCode.PlayerNotInRaid };

        var raid = ServerLogic.Instance.ActiveRaids[player.Username];
        if(raid == null)
            return new ErrorInfo(){ Code = ErrorCode.PlayerNotInRaid };  

        if (!raid.Finished)
            return new ErrorInfo(){ Code = ErrorCode.UnknownError };

        SimpleMessage m = new SimpleMessage();
        string reward = raid.GetAllRewards();       
        if (reward != "")
            m.Add(ParameterCode.StringValue, reward);
             
        NetworkManager.SendMessage(player, RequestCode.GetAllRewards,  m);     
//		Debug.LogError ("Thi MF >>\t" + reward);
        return null;       
    }


    public static void RaidFinished(Player player)
    {
        if (!ServerLogic.Instance.ActiveRaids.ContainsKey(player.Username))
        {
            Debug.Log("The user isnt in raid");
            return;
        }

        var raid = ServerLogic.Instance.ActiveRaids[player.Username];
        if(raid == null)
        {
            Debug.Log("the raid doesnt exists.");
            return;
        }  

        int cnt = raid.DestroyedBuildingCount;
        SimpleMessage m = new SimpleMessage();
        m.Add(ParameterCode.BuildingCount, cnt.ToString());

        NetworkManager.SendMessage(player, EventCode.RaidFinished, m);
    }


   // ServerLogic.Instance.FinishRaid(player.Username);       
}
