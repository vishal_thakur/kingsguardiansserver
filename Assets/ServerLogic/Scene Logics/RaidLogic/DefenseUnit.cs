﻿ using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;

public class DefenseUnit : RaidUnit
{
	public readonly Building Building;

	public override string ContentID{ get { return (Building != null) ? Building.ContentID : "unknown"; }}
	public override string InstanceID { get { return (Building != null) ? Building.InstanceID : "unknown"; }}
	public override UnitType UnitType { get { return UnitType.Building; } }
	public override Point Position { get {return (Building != null) ? Building.Position : Point.zero; }}
	public override int MaxHP { get { return (Building != null) ? Building.MaxHP : 0; }}
	public override int CurrHP { get { return (Building != null) ? Building.CurrentHP : 0; }}

	protected override int AttackRange { get { return (Building != null) ? Building.GetUpgrade().Range : 0; } }
	protected override float AttackSpeed { get { return (Building != null) ? Building.GetUpgrade().AttackSpeed : 0f; } }

	protected override List<UnitType> EnemyUnits { get { return new List<UnitType>(){ UnitType.Character }; } }

	bool _targetLandUnit;
	bool _targetAirUnit;


	public DefenseUnit(Building building, RaidManager manager) : base (manager)
	{
		Building = building;
		_targetLandUnit = building.GetUpgrade().LandUnit;
		_targetAirUnit = building.GetUpgrade().FlyingUnit;
	}


	protected override bool AdjustHP(int amount, out bool hit, Element damageType = Element.Untyped)
	{
		hit = true;
		if (Building == null) return false;
		Building.AdjustHP(amount);
		return true;
	}

	public override Point GetClosestTile(Point position)
	{
		if (Building == null) return null;		
		return Footprint.ClosestCorner(position, Building.Position, Building.Size);
	}

	protected override IRaidUnit SearchTarget()
	{
		var list = manager.UnitsInRange(EnemyUnits, false, Position, AttackRange, _targetLandUnit, _targetAirUnit);
		int distance = AttackRange + 1;
		foreach (var u in list)
		{
			var d = GridHandler.ManhattanDistance(u.Position, Position);

			Log("Search target - " + u.InstanceID + " (" + d + "), range: " + distance);

			if (d < distance)
			{
				distance = d;
				return u;
			}           
		}   
		return null;
	}

	protected override void OnAttack(ref Element damageType, ref int damage)
	{
		damage = Building.GetUpgrade().Damage;
		damageType = Element.Untyped;

		// If the building only attacks once destroy it after the attack.
		if (AttackSpeed == 0) TakeDamage(Building.CurrentHP, Element.Untyped);
	}

	protected override void OnStateStarted(State state)
	{
		if (state == State.Moving || state == State.Idle)
			ChangeState(State.Ready);
	}
}
