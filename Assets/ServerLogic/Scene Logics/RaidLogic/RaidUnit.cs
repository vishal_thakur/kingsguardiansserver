﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;

/// <summary>
/// This class is the base class for the all unit in the raid (defense buildings, player characters, and breeding pit characters)
/// Basic buildings is not derived from this class, that comes from IRaidUnit interface only.
/// </summary>
public class RaidUnit : IRaidUnit
{
	public IRaidUnit TargetUnit { get; private set; }
	public Point DestinationTile { get; private set; }

	public enum State { Destroyed, Idle, Moving, Ready, Attack }
	public State CurrentState { get; private set; }

	protected readonly RaidManager manager;

	float _lastAttackTime = -1000f;


	public RaidUnit(RaidManager manager) 
	{ 
		this.manager = manager; 
		ChangeState(State.Ready);
	}

	public void ChangeState(State state)
	{
		OnStateEnded(CurrentState);
		CurrentState = state;
		OnStateStarted(state);
	}

	public void StartAttack() 
	{
		ChangeState(State.Attack); 
	}
	public void StopAttack() { ChangeState(State.Ready); }


	public void Update()
	{
		if (CurrentState == State.Destroyed)
			return;

		switch (CurrentState)
		{
			case State.Idle: break;
			case State.Moving: break;
				
			case State.Ready: SetTarget(SearchTarget()); break;

			case State.Attack:
				if (TargetUnit == null) StopAttack();
				else
				{			
					int distance = GridHandler.ManhattanDistance(DestinationTile, Position);  
					if (distance <= AttackRange)
					{	
						if (_lastAttackTime + AttackSpeed <= Time.time)
						{
							Element damageType = Element.Untyped;
							int damage = 1;

							OnAttack(ref damageType, ref damage);
							TargetUnit.TakeDamage(damage, damageType);

							// Splash Damage
							if (DamageRadius > 0)
							{
								var list = manager.UnitsInRange(EnemyUnits, false, TargetUnit.Position, DamageRadius, TargetLandUnit, TargetAirUnit, new List<string>(){ TargetUnit.InstanceID });
								foreach (var c in list)
									c.TakeDamage(damage, damageType);
							}

							_lastAttackTime = Time.time;

							if (TargetUnit.Destroyed) 
								OnTargetDestroyed();
						}
					}
					else
						OnTargetOutofRange();
				}
				break;
		}

		OnUpdate();
	}



	#region Protected methods
	protected void SetTarget(IRaidUnit target)
	{
		if (manager.IsUnitTargetable(target))
		{
			TargetUnit = target;

			if (UnitType == UnitType.Building)
				DestinationTile = TargetUnit.Position;
			else
			{
				Point closestPoint = TargetUnit.GetClosestTile(Position);

				if (AttackRange >= GridHandler.ManhattanDistance(closestPoint, Position))
					DestinationTile = Position;
				else
				{
					Point direction = (TargetUnit.Position - Position).normalized;
					DestinationTile = closestPoint - direction * AttackRange;
				}
			}

			ChangeState(State.Attack);
			SetTargetEvent();
		}
	}

	protected void ClearTarget()
	{
		TargetUnit = null;
		DestinationTile = Position;
	}
	#endregion



	#region virtual Parameters
	public virtual bool AirUnit { get { return false; } }

	protected virtual int AttackRange { get {return 1; } }
	protected virtual float AttackSpeed { get {return 1f; } }
	protected virtual int DamageRadius { get { return 0; } }
	protected virtual bool TargetLandUnit { get { return true; }}
	protected virtual bool TargetAirUnit { get { return true; }}

	protected virtual List<UnitType> EnemyUnits { get { return new List<UnitType>(); }}



	#endregion



	#region Virtual Methods
	protected virtual void OnStateEnded(State state){}
	protected virtual void OnStateStarted(State state){}

	protected virtual IRaidUnit SearchTarget(){ return null; }

	protected virtual void OnTargetDestroyed(){ StopAttack(); }
	protected virtual void OnTargetOutofRange(){ StopAttack(); }

	protected virtual void OnAttack(ref Element damageType, ref int damage){}
	protected virtual bool AdjustHP(int amount, out bool hit, Element damageType = Element.Untyped)
	{ 
		hit = true;
		Log("Adjust HP is not implemented (" + amount + ")"); 
		return false;
	}

	protected virtual void OnUpdate(){	}
	#endregion



	#region IRaidUnit implementation
	public virtual string ContentID { get { return "unknown"; }}
	public virtual string InstanceID { get { return "unknown"; }}
	public virtual Point Position { get { return Point.zero; }}
	public virtual int MaxHP { get { return 0; }}
	public virtual int CurrHP { get { return 0; }}

	public virtual UnitType UnitType { get { return UnitType.Building; } }

	public virtual Point GetClosestTile(Point position) { return Position; }


	public bool Destroyed { get { return CurrentState == State.Destroyed; }}
	public void Heal(int amount) 
	{ 
		bool hit = true;
		if(AdjustHP(amount, out hit))
			DamageTakenMessage(hit, -amount, Element.Untyped);
	}
	public void TakeDamage(int amount, Element e = Element.Untyped)
	{
		bool hit = true;
		if (AdjustHP(-amount, out hit, e))
		{
			if (CurrHP == 0) ChangeState(State.Destroyed);
			DamageTakenMessage(hit, amount, e);
		}
	}
	#endregion


	protected void DamageTakenMessage(bool hit, int amount, Element e)
	{
		var msg = new RaidDamageTakenMessage() {
			InstanceID = InstanceID,
			AttackResult = (hit) ? AttackResult.Hit : AttackResult.Miss ,
			Damage = amount,
			DamageType = e,
			CurrentHP = CurrHP
		};

		NetworkManager.SendMessage(manager.Player, EventCode.DamageTaken, msg );
	}


	protected void SetTargetEvent()
	{
		var msg = new SimpleMessage();
		msg.Add(ParameterCode.CharacterInstanceID, InstanceID);
		msg.Add(ParameterCode.InstanceID, TargetUnit.InstanceID);
		msg.Add(ParameterCode.Position, DestinationTile.vector3.ToString());
		msg.Add(ParameterCode.LookAtPosition, TargetUnit.Position.vector3.ToString());

		NetworkManager.SendMessage(manager.Player, EventCode.TargetUnit, msg); 
	}

	protected void CustomEvent(string eventName)
	{
		var msg = new SimpleMessage();
		msg.Add(ParameterCode.InstanceID, InstanceID);
		msg.Add(ParameterCode.EventName, eventName);
		NetworkManager.SendMessage(manager.Player, EventCode.CustomRaidEvent, msg);
	}

	protected void Log(string msg)
	{
		//if(ContentID == "Landmine")
		if(UnitType == UnitType.Character)
			Debug.Log("[" + InstanceID + "]: " + msg);
	}




	/*

	public virtual bool ChangeFlag { get; set; }
	public RaidAction CurrentAction { get; protected set; }
	public Vector3 LookTarget { get { return (TargetTile != null) ? TargetTile.vector3 : Vector3.zero; }}
	public virtual bool AirUnit { get { return false; } }
	protected virtual Vector3 wordPosition { get { return Position.vector3; }}





	public void Update()
	{
		if (ReturnUpdate())
			OnUpdateReturned();
		else
		{
			if (TargetUnit != null && (!manager.IsUnitTargetable(TargetUnit) || !HasValidTarget()))
			{
				TargetUnit = null;
				ChangeAction(RaidAction.Idle);
			}
			else if (TargetUnit == null)
				FindTarget();

			else if (TargetUnit != null)
				Act();

			OnUpdate();
		}

		UpdateClient();

		if (CurrentAction == RaidAction.Attack)
			ChangeAction(RaidAction.Idle);
	}


	

	
	protected void UpdateClient()
	{
		if (_lastPosition != wordPosition || _lastLookAt != LookTarget || _lastAction != CurrentAction)
		{
			Debug.Log(InstanceID + " updates client " + Position + ", " + LookTarget + ", " + CurrentAction);

			var msg = new RaidMovementMessage() {
				InstanceID = InstanceID,
				Position = wordPosition,
				LookAt = LookTarget,
				Action = CurrentAction
			};
			NetworkManager.SendMessage(manager.Player, EventCode.UnitUpdate, msg );

			_lastAction = CurrentAction;
			_lastPosition = wordPosition;
			_lastLookAt = LookTarget;
		}
	}

	
	#endregion


	
*/
}
