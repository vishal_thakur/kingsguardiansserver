﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Outcome
{
	public bool Finished { get; private set; }

	int _totalCharacters;
	float _startTime = -1;

	float[] _rewardAmmounts = new float[]{0.05f, 0.1f, 0.25f, 0.5f, 0.75f };

	List<IRaidUnit> _buildings;
	List<IRaidUnit> _destroyedBuildings;
	int _currentBuilding = 0;


	//All Rewards Collective String
	class Reward {
		public string buildingID = null;
		public Currency currency;
		public int ammount = 0 ;
	}

	List<Reward> AllRewards = new List<Reward>();

	//Reward Ammount for the respective Building
	int rewardAmmount = 0;






	public Outcome(float startTime, int totalCharacters)
	{
		_totalCharacters = totalCharacters;
		_startTime = startTime;
	}

	public bool IsFinished(List<IRaidUnit> participans)
	{
		// Time is out
		if (_startTime + 12 * 60 <= Time.time) return true;

		int characterCount = 0;
		bool allBuildingDestroyed = true;
		bool characterLives = false;


		foreach (var p in participans)
		{
			if (p.UnitType == UnitType.Building && p.ContentID != "Wall" && p.ContentID != "Landmine" && !p.Destroyed)
				allBuildingDestroyed = false;
			else if (p.UnitType == UnitType.Character)
			{
				characterCount++;
				if (!p.Destroyed)
					characterLives = true;
			}
		}
		return characterCount > 0 && (!characterLives || allBuildingDestroyed);
	}

	public void CalculateReward(Player player, Player enemy, List<IRaidUnit> participans)
	{
		//Vishal		_currentBuilding = 0;
		// Get every buildings thats not a wall or a landmine
		_buildings = participans.FindAll(x => x.UnitType == UnitType.Building &&  x.ContentID != "Wall" && x.ContentID != "Landmine");
		// Get the destroyed buildings.
		_destroyedBuildings = _buildings.FindAll(x => x.Destroyed);

		// Calculate % of the buildings are destroyed
		float percentage = (float)_destroyedBuildings.Count / (float)_buildings.Count;
		Debug.LogError ("Destroy Percentage\t" + percentage);
		// Determine if the raid was successful or not
		bool won = percentage >= 0.75f || _destroyedBuildings.Find(x => x.ContentID == "Castle") != null;

		Debug.LogError ("Won ?\t" + percentage);
		// Destroy Warlocks
		if (won) {
			enemy.DestroyWarlock (_destroyedBuildings);

			Debug.LogError ("Enemy warlock destroyed");
		}

		// Add collectibe remains of dead characters
		var deadCharacters = participans.FindAll(x => x.UnitType == UnitType.Character);
		foreach (var c in deadCharacters)
			enemy.AddRemain(c);

		// Calculate the medals awarded or lost
		int medals = Mathf.Clamp(20 + ((won) ? 1 : -1) * (enemy.Level - player.Level), 5, 35);
		// If won multiply the medals with the percentage
		if(won) medals = Mathf.RoundToInt((float)medals * percentage);
		// Add it to the player medals.
		player.Medals += medals;     

		RaidLogic.RaidFinished(player);
		Finished = true;
		Debug.LogError ("Raid Finished!!!");
	}





//	public string GetAllRewards(Player player, Player enemy){
//		//Vishal
//		_currentBuilding++;
//		Debug.LogError ("Get Next Reward\tCurrent Building = " + _currentBuilding + "\tDestroyed Buildings = " + _destroyedBuildings.Count);
//		if (_currentBuilding >= _destroyedBuildings.Count) {
//			Debug.LogError ("Current Build\t" + _currentBuilding + "\t\tDestroyed Buildings" + _destroyedBuildings.Count ) ;
//			return "";
//		}
//
//		var b = ((BuildingUnit)_destroyedBuildings[_currentBuilding]).Building;
//
//		if (b.ContentID == "Castle")
//		{
//			
//			int ether = Mathf.RoundToInt(enemy.Materials.Get(Currency.Ether) * _rewardAmmounts[Random.Range(0, _rewardAmmounts.Length)]);
//			player.Materials.Add(Currency.Ether, ether);
//			Debug.LogError (Currency.Ether + ":" + ether);
//			return Currency.Ether + ":" + ether;
//		}
//		else
//		{
//			switch (b.Category)
//			{
//			case BuildingCategory.Resource_mine: 
//			case BuildingCategory.Resource_generator:
//			case BuildingCategory.Resource_storageOnly:    
//				int mat = Mathf.RoundToInt(enemy.Materials.Get(b.Material) * _rewardAmmounts[Random.Range(0, _rewardAmmounts.Length)]);
//				enemy.Materials.Subtract(b.Material, mat);
//				player.Materials.Add(b.Material, mat);
//				Debug.LogError (b.Material+ ":" + mat);
//				return b.Material + ":" + mat;
//				break;
//				//Vishal			case BuildingCategory.CharacterCreator:
//				//				Debug.LogError ("Gold up your ass!");
//				//Vishal				player.Materials.Add(Currency.Gold, 10);
//				//Vishal				break;
//			case BuildingCategory.CardCreator:
//			case BuildingCategory.Defense:
//				var item = enemy.Inventory.Items.Find(x => x.Category == b.ItemCategory);
//				if (item != null)
//				{
//					enemy.Inventory.Remove(item.ContentID);
//					player.Inventory.Add(item);
//					Debug.LogError (item.ContentID);
//					return item.ContentID;
//				}              
//				break;
//			}
//		}   
//
//		//Vishal		_currentBuilding++;
//		return "";
//	}




	//Vishal
	public string GetAllRewards(Player player, Player enemy)
	{
		//Loop through The destroyed Buildings And Collect rewards from each building
		for(int i=0;i<_destroyedBuildings.Count;i++){
			Building building = ((BuildingUnit)_destroyedBuildings[i]).Building;


			//If the destroyed Building is Castle, then Give Special Reward (What is special reward : A random material reward out of all materials)
			if (building .ContentID == "Castle"){
				int ether = Mathf.RoundToInt(enemy.Materials.Get(Currency.Ether) * _rewardAmmounts[Random.Range(0, _rewardAmmounts.Length)]);
				player.Materials.Add(Currency.Ether, ether);
				AddRewardToAllRewards (building , Currency.Ether , ether);
			}
			//Give Reward As per the Destroyed Building
			else{
				switch (building.Category)
				{
				case BuildingCategory.Resource_mine: 
				case BuildingCategory.Resource_generator:
				case BuildingCategory.Resource_storageOnly:    
					rewardAmmount = Mathf.RoundToInt (enemy.Materials.Get (building.Material) * _rewardAmmounts [Random.Range (0, _rewardAmmounts.Length)]);

					//Substract Material from the Enemy's Being Raided
					enemy.Materials.Subtract (building.Material, rewardAmmount);
					enemy.Save ();

					//Add Material To the Player Who did the raid
					player.Materials.Add(building.Material, rewardAmmount);

					AddRewardToAllRewards (building , building.Material , rewardAmmount);
					break;


				case BuildingCategory.CharacterCreator:
					player.Materials.Add(Currency.Gold, 10);
					AddRewardToAllRewards (building , Currency.Gold , 10);
					break;

				case BuildingCategory.CardCreator:
				case BuildingCategory.Defense:
					var item = enemy.Inventory.Items.Find(x => x.Category == building.ItemCategory);
					if (item != null){
//						enemy.sa
						enemy.Inventory.Remove(item.ContentID);
						enemy.Save ();

						player.Inventory.Add(item);
						Debug.LogError (item.ContentID);
						return item.ContentID;
					}              
					break;
				}
			}   

		}

		//Sample Format:
		//Wood:1233|ether:23213|gold:234324
//		Debug.LogError(AllRewards);
		return GetAllRewardsInOneString();
	}

	void AddRewardToAllRewards(Building building, Currency currency , int rewardAmmount){

		//Create Temp Reward 
		Reward newReward = new Reward();

		//and Fill Params as Provided
		newReward.buildingID = building.ContentID;
		newReward.currency = currency;
		newReward.ammount = rewardAmmount;

		//If this type of reward is already in the list
		bool containsSameRewardAlready = false;

		for (int i=0;i<AllRewards.Count;i++) {
//			Debug.LogError ("Rewardssssss" + AllRewards[i].buildingID.ToString() + AllRewards[i].currency.ToString() + AllRewards[i].ammount.ToString());
			//IF Contains Same Currency Already
			if (newReward.currency == AllRewards[i].currency) {
				//Then Add it to the same currency reward ammount
				containsSameRewardAlready = true;
				AllRewards[i].ammount += rewardAmmount;
			}
		}

		//If does not contains the same currency then add it to the All rewards
		if (!containsSameRewardAlready)
			AllRewards.Add (newReward);
	}


	//Iterates Throught the AllRewards and Returns it in a single string format
	string GetAllRewardsInOneString(){
		string finalString = null;

		for(int i=0;i<AllRewards.Count;i++) {
			//Add Reward Info to the Reward Data String
			//If This is first Reward to be added in Data String then Donot use the seperator char
			if (i == 0) {
				if (AllRewards [i].buildingID == "Castle")
					finalString = AllRewards [i].currency.ToString () + ":" + AllRewards [i].ammount.ToString () + ":Castle";
				else
					finalString = AllRewards [i].currency.ToString () + ":" + AllRewards [i].ammount.ToString ();
			}
		//Else use the seperator char '|' , it is used at the client side to seperate a reward one at a time from the whole reward String
		else {
				if (AllRewards[i].buildingID == "Castle")
					finalString += "|" + AllRewards [i].currency.ToString () + ":" + AllRewards [i].ammount.ToString () + ":Castle";
				else
					finalString += "|" + AllRewards [i].currency.ToString () + ":" + AllRewards [i].ammount.ToString ();
			}
		}

//		Debug.LogError (finalString.ToString());
		return finalString;
	}

}
