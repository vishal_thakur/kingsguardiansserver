﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GridSystem;

public interface IRaidUnit
{
	string ContentID{ get; }
	string InstanceID { get; }

	UnitType UnitType { get; }
	Point Position { get; }

	int MaxHP { get; }
	int CurrHP { get; }
	bool Destroyed { get; }

	bool AirUnit { get;}

	Point GetClosestTile(Point position);

	void Heal(int amount);
	void TakeDamage(int amount, Element e = Element.Untyped);

}
