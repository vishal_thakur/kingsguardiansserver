﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;

public class CharacterUnitBase : RaidUnit
{
	public readonly Character Character;

	public bool DestinationArrivalAllowed 
	{ 
		get 
		{ 
			bool arrived = _destinationReachTime < Time.time;
			if (arrived)
				_position = DestinationTile;
			return arrived;
		}
	}

	public override string ContentID{ get { return (Character != null) ? Character.ContentID : "unknown"; }}
	public override string InstanceID { get { return (Character != null) ? Character.InstanceID : "unknown"; }}
	public override UnitType UnitType { get { return UnitType.Character; } }
	public override Point Position { get { return _position; }}
	public override int MaxHP { get { return (Character != null) ? Character.MaxHP : 0; }}
	public override int CurrHP { get { return (Character != null) ? Character.CurrentHP : 0; }}
	public override bool AirUnit { get { return (Character != null) ? Character.Data.Flying : false; }}

	protected override int AttackRange { get { return (Character != null) ? Character.AttackRange : 0; } }
	protected override float AttackSpeed { get { return (Character != null) ? Character.AttackSpeed : 0f; } }

	Point _position;
	Pathfinder _pathfinder;

	float _destinationReachTime = -1f;

	public CharacterUnitBase(Character character, RaidManager manager, Point position) : base (manager)
	{
		Character = character;		

		_position = position;

		_pathfinder = new Pathfinder();              
		_pathfinder.DefaultObstacleCost = (Character.Data.Flying) ? 1 : 100;
		_pathfinder.SetObstacleCost("Wall", 10);

		// Add passive traits
		_traits = new Trait();
		foreach (var trait in character.Data.Traits)
			_traits.AddTrait("owned", trait);
	}

	protected override IRaidUnit SearchTarget()
	{ 
		float distance = Mathf.Infinity;
		IRaidUnit target = null;

		var unitsInRange = manager.UnitsInRange(EnemyUnits, false, Position, 50, Character.Data.LandTarget, Character.Data.AirTarget);
		foreach (var t in unitsInRange)
		{
			if (manager.IsUnitTargetable(t))
			{
				var d = GridHandler.ManhattanDistance(t.GetClosestTile(Position), Position);
				if (d < distance)
				{
					distance = d;
					target = t;
				}                
			}
		}
		return target; 
	}

	protected override void OnTargetOutofRange()
	{ 
		Log("Target is out of range -> move closer");
		ChangeState(State.Moving);
	}
	protected override void OnTargetDestroyed() 
	{ 
		Log("Target is destroyed -> stop attack");
		StopAttack();
	}


	protected override void OnAttack(ref Element damageType, ref int damage)
	{ 
		damage = Character.Stats.GetStat(CharacterStats.Attack);
		damageType = Element.Untyped;

		// Increase Damage
		if (_traits.HasTrait<DamageIncreaseTrait>())
		{
			var t = _traits.GetTrait<DamageIncreaseTrait>();

			bool sameID = t.VSContentID == "" || t.VSContentID == TargetUnit.ContentID;
			bool sameType = t.VSUnitType == UnitType.None || t.VSUnitType == TargetUnit.UnitType;

			if( sameID && sameType)
				damage = Mathf.RoundToInt((float)damage * t.Multiplier);
		}

		// Explode
		if (_traits.HasTrait<ExplosionTrait>())
		{
			var t = _traits.GetTrait<ExplosionTrait>();
			damage = Mathf.RoundToInt( (float)damage * t.Multiplier);
			Character.AdjustHP(-Character.CurrentHP);
			CustomEvent("Kamikaze");
		}

		// Alternate Damage
		if (_traits.HasTrait<AlternateDamageTrait>())
		{
			var t = _traits.GetTrait<AlternateDamageTrait>();
			damageType = t.DamageType;
		}
	}

	protected override bool AdjustHP(int amount, out bool hit, Element damageType = Element.Untyped)
	{
		hit = true;
		if (Character == null) return false;	

		if (amount < 0)
		{
			if (_traits.HasTrait<MissTrait>())
			{
				var t = _traits.GetTrait<MissTrait>();
				if (UnityEngine.Random.Range(0f, 1f) <= t.Chance)
					hit = false;
			}

			if (hit)
			{
				if (damageType == Element.Untyped && _traits.HasTrait<DamageResistanceTrait>())
				{
					var t = _traits.GetTrait<DamageResistanceTrait>();
					amount = Mathf.RoundToInt( (float)amount * t.Percentage);
				}
			}
		}	

		if(hit) Character.AdjustHP(amount);
		return true;
	}


	protected override void OnUpdate()
	{
		if (_traits.HasTrait<NecromancyTrait>())
		{
			var t = _traits.GetTrait<NecromancyTrait>();

			if (_summons.Count < t.SummonsCount && _lastSummon + 2f <= Time.time)
			{
				Character summon = new Character(ContentID);

				// Summons has half the stats that its masters.
				var baseStats = summon.Stats.BaseStats;
				foreach( var s in baseStats)
					s.Value = Mathf.RoundToInt(s.Value / 2);	
				summon.Stats.ReadBaseStats(baseStats);

				summon.SetOwner(Character.PlayerOwner);

				var summoned = manager.Spawn<SummonedUnit>(summon, Position);
				if (summoned != null)
				{
					_summons.Add(summoned);
					manager.SendSummonSpawn(summoned);
				}
				_lastSummon = Time.time;
			}
		}

		AuraUpdate();
	}

	public override Point GetClosestTile(Point position)
	{
		var dir = new Point(position.x - Position.x, position.y - Position.y).normalized;
		return position + dir;
	}

	protected override void OnStateStarted(State state)
	{
		if (state == State.Moving)
		{
			_pathfinder.Calculate(Position, DestinationTile);
			if (_pathfinder.HasPath)
			{
				float distance = (float)_pathfinder.Length * GridHandler.Instance.TileSize.x; 
				float speed = Character.MovementSpeed / Time.deltaTime;
				_destinationReachTime = Time.time + distance / speed;
				_pathfinder.Reset();
			}
		}
	}


	protected override void OnStateEnded(State state){ base.OnStateEnded(state); }


	#region Traits & Auras
	Aura _aura;
	Trait _traits;
	List<CharacterUnit> _inAura = new List<CharacterUnit>(); 	

	List<SummonedUnit> _summons = new List<SummonedUnit>();
	float _lastSummon = -1f;


	public void AddTrait(string source, TraitBase trait, float duration = -1f)
	{
		if (trait.GetType() == typeof(WallJumpTrait))
		{
			_pathfinder.SetObstacleCost("Wall", 1);
			CustomEvent("WallJumpAdded");
		}

		if (trait.GetType() == typeof(StatIncreaseTrait))
		{
			var sTrait = (StatIncreaseTrait)trait;
			Character.Stats.AddMagicModifier(sTrait.Stat, sTrait.Amount, sTrait.Stat+"_"+sTrait.Amount);
		}

		_traits.AddTrait(source, trait);
		if (duration > -1f)
			ServerLogic.Instance.StartCoroutine(RemoveTrait(source, duration));
	}


	IEnumerator RemoveTrait(string source, float delayTime)
	{
		yield return new WaitForSeconds(delayTime);
		RemoveTrait(source);
	}

	public void RemoveTrait (string source)
	{
		var t = _traits.GetTrait(source);
		if (t != null && t.GetType() == typeof(WallJumpTrait))
		{
			_pathfinder.SetObstacleCost("Wall", 10);
			CustomEvent("WallJumpRemoved");
		}
		if (t.GetType() == typeof(StatIncreaseTrait))
		{
			var sTrait = (StatIncreaseTrait)t;
			Character.Stats.RemoveMagicModifier(sTrait.Stat+"_"+sTrait.Amount);
		}

		_traits.RemoveTrait(source);
	}

	protected void SetAura(Aura aura)
	{
		_aura = aura;
	}


	void AuraUpdate()
	{
		if (_aura == null) return;

		var list = new List<IRaidUnit>();
		switch (_aura.Targets)
		{
			case TargetType.Friend: 
				list = manager.UnitsInRange(EnemyUnits, true, Position, _aura.Range, true, true, new List<string>(){ InstanceID });
				break;
			case TargetType.Foe: 
				list = manager.UnitsInRange(EnemyUnits, false, Position, _aura.Range, true, true);
				break;
		}
		_aura.Update(list);	
	}

	public void FinishAura(Aura a)
	{
		if (_aura == a) _aura = null;
	}
	#endregion
}
