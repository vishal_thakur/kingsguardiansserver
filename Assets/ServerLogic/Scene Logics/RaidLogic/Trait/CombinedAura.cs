﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombinedAura : Aura
{
	float _amount = 0.05f;	//-dmg
	float _lastTick = -1;
	TraitBase _trait;
	List<CharacterUnitBase> _targets = new List<CharacterUnitBase>();

	public CombinedAura(float amount, TraitBase trait, CharacterUnitBase caster, float duration, int range) : base(caster, duration, range)
	{
		_amount = amount;
		_trait = trait;
	}

	protected override void OnUpdate(List<IRaidUnit> inAura)
	{
		bool tick = _lastTick + 1 <= Time.time;

		List<CharacterUnitBase> list = new List<CharacterUnitBase>();
		foreach (var u in inAura)
		{
			if (u.UnitType == UnitType.Character || u.UnitType == UnitType.BreedingPit)
			{
				var c = (CharacterUnitBase)u;
				c.AddTrait(caster.InstanceID + "Aura", _trait);
				list.Add(c);
			}

			if (tick)
			{
				int hp = Mathf.RoundToInt((float)u.MaxHP * _amount);				
				if (hp > 0) u.Heal(hp);
				else u.TakeDamage(-hp);			
			}
		}

		if(tick)
			_lastTick = Time.time;

		_targets = list;
		base.OnUpdate(inAura);
	}

	protected override void OnDeactivate()
	{
		foreach (var t in _targets)
			t.RemoveTrait(caster.InstanceID+"Aura");
		_targets.Clear();
	}
}
