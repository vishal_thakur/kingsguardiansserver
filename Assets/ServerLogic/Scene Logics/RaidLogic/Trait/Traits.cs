﻿using System.Collections.Generic;
using System;

public class Trait
{
	Dictionary<Type, TraitBase> _traits  = new Dictionary<Type, TraitBase>();
	Dictionary<Type, string> _sources = new Dictionary<Type, string>();


	public bool HasTrait(Type t)
	{
		return _traits.ContainsKey(t);
	}

	public bool HasTrait<T>() where T : TraitBase
	{
		return _traits.ContainsKey(typeof(T));
	}

	public T GetTrait<T>() where T : TraitBase
	{
		if (HasTrait<T>())
			return (T)_traits[typeof(T)];
		return null;
	}

	public TraitBase GetTrait(string source)
	{
		foreach (var p in _sources)
		{
			if (p.Value == source)
				return _traits[p.Key];
		}
		return null;
	}

	public void AddTrait(string source, TraitBase trait)
	{
		var t = trait.GetType();
		if (!HasTrait(t))
		{
			_traits.Add(t, trait);
			_sources.Add(t, source);
		}
	}


	public void RemoveTrait(string source)
	{
		List<Type> remove = new List<Type>();
		foreach (var p in _sources)
		{
			if (p.Value == source)
				remove.Add(p.Key);
		}

		foreach(var r in remove)
			RemoveTrait(r);
	}

	public void RemoveTrait<T>() where T : TraitBase
	{
		if (HasTrait<T>())
		{
			_traits.Remove(typeof(T));
			_sources.Remove(typeof(T));
		}
	}

	void RemoveTrait(Type t)
	{
		if (HasTrait(t))
		{
			_traits.Remove(t);
			_sources.Remove(t);
		}
	}




	/// <summary>
	/// Generates trait from trait data.
	/// </summary>
	/// <returns>The trait.</returns>
	/// <param name="t">T.</param>
	public static TraitBase CreateTrait (TraitData t)
	{
		TraitBase trait = null;
		switch (t.Type)
		{
			case TraitType.DamageIncrease: 
				trait = new DamageIncreaseTrait(){ Multiplier = t.FloatParameter, VSUnitType = t.Unit, VSContentID = t.ContentID };
				break;

			case TraitType.DamageResistance: 
				trait = new DamageResistanceTrait(){ Percentage = t.FloatParameter};
				break;

			case TraitType.Miss: 
				trait = new MissTrait(){ Chance = t.FloatParameter, VSUnitType = t.Unit, VSContentID = t.ContentID };
				break;

			case TraitType.Explosion: 
				trait = new ExplosionTrait(){ Multiplier = t.FloatParameter};
				break;

			case TraitType.AlternateDamage: 
				trait = new AlternateDamageTrait(){ DamageType = t.DamageType };
				break;

			case TraitType.WallJump:
				trait = new WallJumpTrait();
				break;

			case TraitType.StatIncrease:
				trait = new StatIncreaseTrait(){ Stat = t.Stat, Amount = t.FloatParameter };
				break;

			case TraitType.Necromancy:
				trait = new NecromancyTrait(){ SummonsCount = t.IntParameter };
				break;

			case TraitType.DOT:
				trait = new DoTTrait() { Damage = t.IntParameter, DamageType = t.DamageType };
				break;

			case TraitType.Stun:
				trait = new StunTrait();
				break;
		}
		return trait;
	}
}

