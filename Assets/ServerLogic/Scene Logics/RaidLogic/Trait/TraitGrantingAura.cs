﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TraitGrantingAura : Aura
{
	TraitBase _trait;
	List<CharacterUnitBase> _targets = new List<CharacterUnitBase>();

	public TraitGrantingAura(TraitBase trait, CharacterUnitBase caster, float duration, int range) : base(caster, duration, range)
	{
		_trait = trait;
	}

	protected override void OnUpdate(List<IRaidUnit> inAura)
	{




		List<CharacterUnitBase> list = new List<CharacterUnitBase>();
		foreach (var u in inAura)
		{
			if (u.UnitType == UnitType.Character)
			{
				var c = (CharacterUnitBase)u;
				c.AddTrait(caster.InstanceID + "Aura", _trait);
				list.Add(c);
			}
		}
		
		foreach (var u in _targets)
		{
			if (!inAura.Contains(u))
				u.RemoveTrait(caster.InstanceID+"Aura");
		}

		_targets = list;
		base.OnUpdate(inAura);
	}

	protected override void OnDeactivate()
	{
		foreach (var t in _targets)
			t.RemoveTrait(caster.InstanceID+"Aura");
		_targets.Clear();
	}
}
