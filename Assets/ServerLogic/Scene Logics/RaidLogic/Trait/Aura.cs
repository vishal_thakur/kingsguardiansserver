﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;





public class Aura
{
	public TargetType Targets;

	public readonly int Range;
	protected CharacterUnitBase caster;
	float _duration;

	public Aura(CharacterUnitBase caster, float duration, int range)
	{
		_duration = duration;
		if (_duration == 0)
			_duration = .1f;
		
		Range = range;
		this.caster = caster;
		OnStart();
	}

	public void Update(List<IRaidUnit> inAura)
	{
		if (_duration == 0)
			Deactivate();
		else
		{
			if(inAura.Count > 0)
				OnUpdate(inAura);
			_duration -= Time.deltaTime;
		}
	}

	public void Deactivate()
	{
		caster.FinishAura(this);
		OnDeactivate();
	}


	protected virtual void OnStart(){}
	protected virtual void OnUpdate(List<IRaidUnit> inAura){}
	protected virtual void OnDeactivate(){}	
}