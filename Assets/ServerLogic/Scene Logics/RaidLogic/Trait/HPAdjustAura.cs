﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HPAdjustAura : Aura
{
	float _amount = 0.05f;	//-dmg
	float _lastTick = -1;

	public HPAdjustAura(float amount, CharacterUnitBase caster, float duration, int range) : base(caster, duration, range)
	{
		_amount = amount;
	}

	protected override void OnUpdate(List<IRaidUnit> inAura)
	{
		if (_lastTick + 1 <= Time.time)
		{
			foreach (var u in inAura)
			{
				int hp = Mathf.RoundToInt((float)u.MaxHP * _amount);				
				if (hp > 0) u.Heal(hp);
				else u.TakeDamage(-hp);
			}
			_lastTick = Time.time;
		}
	}
}
