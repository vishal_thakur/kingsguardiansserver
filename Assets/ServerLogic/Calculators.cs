﻿using UnityEngine;
using System.Collections;

public class Calculators
{
    public static int BASE_LVLUP_XP = 100;
    public static float XP_LEVEL_MULTIPLIER = 1.2f;

    #region Experience Points and Levels
    public static int ExperienceToNextLevel( int currentLevel )
    {
        if (currentLevel == 0) return 0;
        return Mathf.RoundToInt((float)BASE_LVLUP_XP * Mathf.Pow (XP_LEVEL_MULTIPLIER, (float)currentLevel-1));
    }

    public static int TotalExperienceTo( int level)
    {
        int xp = 0;
        for (int i = 1; i <= level; i++)
            xp += ExperienceToNextLevel(i-1);
        return xp;        
    }

    public static int CalculateLevel(int xp)
    {
        int lvl = 1;
        while (xp > 0) 
        {
            xp -= ExperienceToNextLevel(lvl);
            lvl++;
        }
        return lvl;
    }
    #endregion


    public static int BuildingMaxHP (int baseHP, int upgrade)
    {
        return baseHP * (1 + upgrade);
    } 
}
