﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class PlayerDetailsPanel : BasePanel
{
    public int SelectedIndex { get; set; }
    public string SelectedMenu { get; set; }

    public Text Name;
    public GameItemList ItemList;
    public Text ItemCategory;

    public List<GameItem> List { get; private set; }
    
	public BuildingPanel BuildingDetails;
	public CharacterPanel CharacterDetails;
	public MaterialPanel MaterialDetails;
	public Text Details;

    Player _player;

    public override void Refresh()
    {        
        _player = ServerLogic.Instance.PlayerDatabase.GetPlayer(Parent.SelectedIndex);
        ItemList.Panel = this;
		ItemList.Refresh((List != null) ? List.Count : 0);
		Name.text = _player.Username + "("+((_player.IsConnected)?"Connected":"Disconnected | " + "Banned : " + _player.IsBanned)+")";
    }

    public void ChooseMenu(string menu)
    {
        SelectedMenu = menu;     

        if(List != null)
            List.Clear();
        else
            List = new List<GameItem>();
        
        if (menu == "Buildings")
        {           
            foreach (var b in _player.Buildings.Items)
                List.Add(b);            
        }

        else if (menu == "Characters")
        {
            foreach (var b in _player.Characters)
                List.Add(b);            
        }
         
        else if (menu == "Items")
        {
            foreach (var b in _player.Inventory.Items)
                List.Add(b);            
        }

        else if (menu == "Materials")
        {

			var mats = (Currency[])System.Enum.GetValues(typeof(Currency));

			foreach (var m in mats)
			{
				var gi = new GameItem();
				gi.SetContentID(m.ToString() + " ("+_player.Materials.Get(m)+"/"+ _player.Materials.GetStorage(m)+")");
				List.Add(gi);
			}
        }
		else if (menu == "Workers")
		{
			foreach (var w in _player.Workers)
				List.Add(w);
			foreach (var wl in _player.Warlocks)
			{
				var gi = new GameItem();
				gi.SetContentID(wl.ID);
				gi.Note = "Building: " + wl.Building;
				List.Add(gi);
			}
		}

        ItemCategory.text = menu;
        ItemList.Set(List.Count);
    }

    public void SelectItem(int index)
    {
        GameItem item = List[index];

        string itemText = "["+SelectedMenu + "-" +index+"]";

		BuildingDetails.gameObject.SetActive(SelectedMenu == "Buildings");
		CharacterDetails.gameObject.SetActive(SelectedMenu == "Characters");
		MaterialDetails.gameObject.SetActive(SelectedMenu == "Materials");
		Details.gameObject.SetActive(SelectedMenu != "Buildings" && SelectedMenu != "Characters" && SelectedMenu != "Materials");

		if (SelectedMenu == "Buildings")
		{  
			Building b = (Building)item;
			BuildingDetails.Set(b);
		}
		else if (SelectedMenu == "Characters")
		{
			Character c = (Character)item;      
			CharacterDetails.Set(c);
                   
		}
		else if (SelectedMenu == "Items")
		{
			var c = (Item)item;                      
			itemText = "<b>" + c.Data.Name + "</b>\n" +
			"ContentID: " + c.ContentID + "\n" +
			"InstanceID: " + c.InstanceID + "\n" +
			"Category: " + c.Category + "\n" +
			"ItemType: " + c.GetType().ToString() + "\n----------------\n" +
			"Stacks: " + _player.Inventory.Stacks(c.ContentID);
           
		}
		else if (SelectedMenu == "Materials")
			MaterialDetails.Set(_player, item);
		else if (SelectedMenu == "Workers")
		{
			itemText += "\n InstanceID: " + item.InstanceID + ((item.Note != "") ? "\n Note: " + item.Note : ""); 
		}


        Details.text = itemText;
    }

	public void Reset()
	{
		_player.Reset();
	}
}