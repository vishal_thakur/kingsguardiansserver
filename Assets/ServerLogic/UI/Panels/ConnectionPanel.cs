﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class ConnectionPanel  : BasePanel
{
    public Text ConnectedClients;

    public override void Refresh()
    {
        ConnectedClients.text = "<b>Connected Clients: </b>\n";
        foreach (var p in ServerLogic.Instance.ConnectedPlayers.OnlinePlayers)
        {
            ConnectedClients.text += " - " + p.Key.connectionId + ". " + p.Value.Username + " [Connected: "+p.Key.isConnected+", Ready: " + p.Key.isReady+"] \n";
        }

        ConnectedClients.text += "\n\n<b>Battles: </b>\n";
        foreach (var p in ServerLogic.Instance.ActiveBattles)
        {
            ConnectedClients.text += " - " + p.Key + " vs " + p.Value.Opponent.Username;
        }

        ConnectedClients.text += "\n\n<b>Raids: </b>\n";
        foreach (var p in ServerLogic.Instance.ActiveRaids)
        {
            ConnectedClients.text += " - " + p.Key;
        }

    }
	
}
