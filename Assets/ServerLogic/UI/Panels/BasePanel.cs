﻿using UnityEngine;
using System.Collections;

public class BasePanel
{
    public ServerUI Parent { get; set; }

    public GameObject Panel;

    public virtual void Refresh()
    {
        
    }

    public void Toggle(bool b)
    {
        Panel.SetActive(b);
    }
}
