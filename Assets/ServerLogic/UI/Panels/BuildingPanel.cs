﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class BuildingPanel : MonoBehaviour 
{
	public Text ContentID;
	public Text InstanceID;

	public Text HP;
	public Text MaxHP;
	public Text Upgrade;

	// WORKER
	public Text BuildingState;
	public Text WorkTime;
	public Button FinishButton;
	public Text WorkerID;

	// Progress
	public Text ActiveProgress;
	public Text ProgressTime;
	public Button ProgressFinishButton;
	public Text Queue;

	Building _building;
	Worker _worker;
	bool _wasActive = false;
	int _lastQueue = -1;

	public void Set(Building b)
	{
		_building = b;
		ContentID.text = b.ContentID + "(" + b.GetType().ToString() + ", " + b.Category + ")";
		InstanceID.text = b.InstanceID + " (Position: " + b.Position+")";

		MaxHP.text = b.MaxHP.ToString();
		HP.text = b.CurrentHP.ToString();
		Upgrade.text = b.Upgrade.ToString();

		_wasActive = !_building.Active;

		bool progressPart = _building.Category == BuildingCategory.Resource_mine || _building.Category == BuildingCategory.CardCreator;

		ActiveProgress.enabled = progressPart;
		ProgressTime.enabled = progressPart;
		Queue.enabled = progressPart;
		ProgressFinishButton.gameObject.SetActive(progressPart);
	}

	void Update()
	{
		if (_building == null)
			return;

		if (_building.Active != _wasActive)
		{
			WorkTime.enabled = !_building.Active;
			WorkerID.enabled = !_building.Active;
			FinishButton.gameObject.SetActive(!_building.Active);

			if (_building.Active)
				BuildingState.text = "Active";
			else
				_worker = _building.PlayerOwner.Workers.Find(x => x.ActiveWork != null && x.ActiveWork.Building.InstanceID == _building.InstanceID);

			_wasActive = _building.Active;
		}

		if (!_wasActive)
		{
			WorkTime.text = (_worker == null || _worker.ActiveWork == null) ? "??" : _worker.ActiveWork.TimeLeft + " sec";
			WorkerID.text = (_worker == null) ? "undefined (" + _building.WorkerID + ")" : _worker.InstanceID;
			BuildingState.text = (_worker != null && _worker.ActiveWork != null) ? _worker.ActiveWork.Type.ToString() : "Error!";
		}
		else if (_building.Category == BuildingCategory.Resource_mine || _building.Category == BuildingCategory.CardCreator)
		{
			if (_building.Queue != null && _building.Queue.Count != _lastQueue)
			{
				ActiveProgress.enabled = _building.Queue.Count > 0;
				ProgressTime.enabled = _building.Queue.Count > 0;
				Queue.enabled = _building.Queue.Count > 1;
				ProgressFinishButton.gameObject.SetActive(_building.Queue.Count > 0);

				if (_building.Queue.Count > 0)
				{
					ActiveProgress.text = _building.Queue[0].ToString();

					string queue = "";
					for (int i = 1; i < _building.Queue.Count; i++)
					{
						if (queue != "")
							queue += ", ";
						queue += _building.Queue[i].ToString();
					}
					Queue.text = queue;
				}
			}

			if (_building.Queue.Count > 0)
				ProgressTime.text = _building.Queue[0].TimeLeft + " sec";		
		}
		else if (_building.Category == BuildingCategory.Resource_generator)
		{
			BuildingState.text = " Active - Materials: " + _building.StoredMaterials + " of " + _building.Storage;
		}
	}

	public void Repair() { HP.text = MaxHP.text; }
	public void AddUpgrade() { Upgrade.text = (int.Parse(Upgrade.text) + 1).ToString();	}
	public void FinishWork()
	{
		if (_worker == null)
		{
			_building.WorkerID = "";
			_building.Active = true;
		}
		else if (_worker.ActiveWork == null)
			_worker.Cancel();
		else
			_worker.ActiveWork.Finish();
	}

	public void FinishProgress()
	{
		Debug.Log("Try to finish progress");
		if (!_building.Queue[0].Finish())
			_building.Queue[0].Cancel();
	}

	public void Save()
	{
		int hp = int.Parse(HP.text);
		if (_building.CurrentHP != hp)
			_building.AdjustHP(hp - _building.CurrentHP);

		int upgrade = int.Parse(Upgrade.text);
		if (_building.Upgrade != upgrade)
			_building.AddUpgrade(upgrade - _building.Upgrade);

		ServerLogic.Instance.PlayerDatabase.SavePlayer(_building.PlayerOwner.Username);
	}

	public void Remove()
	{		
		_building.PlayerOwner.Buildings.RemoveByInstanceID(_building.InstanceID);
		ServerLogic.Instance.PlayerDatabase.SavePlayer(_building.PlayerOwner.Username);
	}
}
