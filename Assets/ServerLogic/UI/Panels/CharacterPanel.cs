﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class CharacterPanel : MonoBehaviour 
{
	public Text ContentID;
	public Text InstanceID;

	public Text HP;
	public Text MaxHP;
	public Text Level;
	public Text XP;

	Character _character;
	int level = 0;

	public void Set(Character c)
	{
		_character = c;
		ContentID.text = c.Data.Name + "(" +c.ContentID+", "+ c.GetType().ToString() + ")";
		InstanceID.text = c.InstanceID;

		MaxHP.text = c.MaxHP.ToString();
		HP.text = c.CurrentHP.ToString();
		Level.text = c.Level.ToString();
		level = c.Level;
		XP.text = c.Exp + "/" + Calculators.ExperienceToNextLevel(level);
	}

	void Update()
	{

	}

	public void Heal() { HP.text = MaxHP.text; }
	public void LevelChange(int lvl) 
	{ 
		level += lvl;
		Level.text = level.ToString();	

		int curr = (level == _character.Level) ? _character.Exp : Calculators.ExperienceToNextLevel(level - 1)+1;

		XP.text = curr + "/" + Calculators.ExperienceToNextLevel(level);
	}

	public void Save()
	{
		int hp = int.Parse(HP.text);
		if (_character.CurrentHP != hp)
			_character.AdjustHP(hp - _character.CurrentHP);

		int level = int.Parse(Level.text);
		if (_character.Level != level)
		{
			int curr = Calculators.ExperienceToNextLevel(level - 1) + 1;
			_character.AddExp(curr - _character.Exp);
		}

		ServerLogic.Instance.PlayerDatabase.SavePlayer(_character.PlayerOwner.Username);
	}

	public void Remove()
	{		
		_character.PlayerOwner.Buildings.RemoveByInstanceID(_character.InstanceID);
		ServerLogic.Instance.PlayerDatabase.SavePlayer(_character.PlayerOwner.Username);
	}
}
