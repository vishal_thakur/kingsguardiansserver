﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;

public class  MaterialPanel : MonoBehaviour 
{
	public Text Name;
	public Text Current;
	public Text Max;

	public InputField Amount;

	Player _player;
	Currency _currency;

	public void Set(Player player, GameItem material)
	{
		_player = player;

		string mat = material.ContentID.Split(' ')[0];
		_currency = (Currency)Enum.Parse(typeof(Currency), mat);

		Name.text = _currency.ToString();
		Current.text = _player.Materials.Get(_currency).ToString();
		Max.text = _player.Materials.GetStorage(_currency).ToString();
	}

	public void Add()
	{
		_player.Materials.Add(_currency, int.Parse(Amount.text));
		ServerLogic.Instance.PlayerDatabase.SavePlayer(_player.Username);
	}

	public void Subtract()
	{
		_player.Materials.Subtract(_currency, int.Parse(Amount.text));
		ServerLogic.Instance.PlayerDatabase.SavePlayer(_player.Username);
	}
}
