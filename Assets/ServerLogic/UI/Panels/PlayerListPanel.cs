﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class PlayerListPanel : BasePanel
{
    public PlayerList List;

    public override void Refresh()
    {
        var players = ServerLogic.Instance.PlayerDatabase.GetPlayers();
        List.Panel = this;
        List.Set(players.Count);
    }
}