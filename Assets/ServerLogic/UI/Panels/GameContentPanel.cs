﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class GameContentPanel  : BasePanel
{
    public Text GameContent;


    public override void Refresh()
    {
        GameContent.text = "<b>Characters </b>\n";
        foreach (var c in ServerLogic.Instance.GameContent.Characters)
            GameContent.text += "-"+c.Value.Name + " (" + c.Value.ContentID + "): " + c.Value.NativeCardContentID+"\n";

        GameContent.text += "<b>Buildings </b>\n";
        foreach (var b in ServerLogic.Instance.GameContent.Buildings)
            GameContent.text += "-"+b.Value.Name + " (" + b.Value.ContentID + ") ["+b.Value.Category+"] ["+b.Value.ItemCategory+"]: \n";

        GameContent.text += "<b>Items </b>\n";
        foreach (var i in ServerLogic.Instance.GameContent.Items)
        {
            GameContent.text += "-" + i.Value.Name + " (" + i.Value.ContentID + "):\n"; 
        }

        GameContent.text += "<b>Weapons </b>\n";
        foreach (var i in ServerLogic.Instance.GameContent.Weapons)
        {
            GameContent.text += "-" + i.Value.Name + " (" + i.Value.ContentID + "):\n"; 
        }
    }
}
