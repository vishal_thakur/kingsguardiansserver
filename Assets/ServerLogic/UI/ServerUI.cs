﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ServerUI : MonoBehaviour
{
    public Text ServerStatus;

    public GameContentPanel GameContent = new GameContentPanel();
    public ConnectionPanel Connections = new ConnectionPanel();
    public PlayerListPanel Players = new PlayerListPanel();
    public PlayerDetailsPanel PlayerDetails = new PlayerDetailsPanel();

	public Text AssetLastUpdate;

    BasePanel _activePanel;

    public int SelectedIndex { get; set; }

    bool _initalized;

    void Start()
    {
		AssetLastUpdate.text = (PlayerPrefs.HasKey("LastAssetUpdate")) ? PlayerPrefs.GetString("LastAssetUpdate") : "";

        GameContent.Parent = this;
        GameContent.Toggle(true);
        _activePanel = GameContent;

        Connections.Parent = this;
        Connections.Toggle(false);

        Players.Parent = this;
        Players.Toggle(false);

        PlayerDetails.Parent = this;
        PlayerDetails.Toggle(false);

        ServerLogic.Instance.Run();
    }


    public void Refresh()
    {
        if (_activePanel != null)
            _activePanel.Refresh();
    }

    void Update()
    {
        if (ServerLogic.Instance.NetworkManager == null)
            return;

        ServerStatus.text = "<b>Server Status:</b>" + ((ServerLogic.Instance.NetworkManager.Running) ? "Online" : "Offline");
        ServerStatus.text += "\n Local IP: " + Utils.LocalIPAddress() + ", port: "+ NetworkServer.listenPort ;

        if (!_initalized && ServerLogic.Instance.NetworkManager.Running)
        {
            GameContent.Refresh();
            Players.Refresh();
            _initalized = true;
        }

        if( _activePanel == Connections)
            Connections.Refresh();
		
        if( _activePanel == PlayerDetails)
            PlayerDetails.Refresh();
    }

    public void ChangePanel(Button button)
    {
        ChangePanel(button.name);
    }

    public void ChangePanel(string name)
    {
        if(_activePanel != null)
            _activePanel.Toggle(false);
        
        switch (name)
        {
            case "GameContent":  _activePanel = GameContent; break;
            case "Connections":  _activePanel = Connections; break;
            case "Players":  _activePanel = Players; break;
            case "Player":
                _activePanel = PlayerDetails;
                _activePanel.Refresh(); 
                break;
        }

        _activePanel.Toggle(true);
    }

    public void ChooseMenu(Button button)
    {
        PlayerDetails.ChooseMenu(button.name);
    }

	public void RefreshAssetLastUpdate()
	{
		string time = System.DateTime.Now.ToString();
		PlayerPrefs.SetString("LastAssetUpdate", time);
		AssetLastUpdate.text = time; 
	}

	public void ResetPlayer()
	{
		if (_activePanel.GetType() == typeof(PlayerDetailsPanel))
			PlayerDetails.Reset();
	}
}


