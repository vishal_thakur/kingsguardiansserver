﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerSlot : MonoBehaviour, ISlot
{
    public Image Background;
    public Image Icon;
    public Text Name;
    public Text Info;

    public ISlotHandler SlotHandler { get; set; }
    public int Index {get; private set; }

    public void SetIndex(int index)
    {
        Index = index;
        var player = ServerLogic.Instance.PlayerDatabase.GetPlayer(Index);

        Background.enabled = player != null;
        Name.enabled = player != null;
        Info.enabled = player != null;
        Icon.enabled = player != null;

        if (player != null)
        {
            Name.text = player.Username;
        }
    }

    public void Select()
    {
        SlotHandler.Select(this);
    }


  
}
