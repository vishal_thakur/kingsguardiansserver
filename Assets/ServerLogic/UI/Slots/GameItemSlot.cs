﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameItemSlot : MonoBehaviour, ISlot
{
    public Image Background;
    public Text Name;

    public ISlotHandler SlotHandler { get; set; }
    public int Index {get; private set; }

    public void SetIndex(int index)
    {
        Index = index;

        var panel = ((GameItemList)SlotHandler).Panel;
        if (panel != null && panel.List != null && panel.List.Count > Index)
        {
            var item = panel.List[Index];
            if (item != null)
            {
                Background.enabled = true;
                Name.enabled = true;

                Name.text = item.ContentID;
                return;
            }
        }

        Background.enabled = false;
        Name.enabled = false;                

       
    }

    public void Select()
    {
        SlotHandler.Select(this);
    }


  
}
