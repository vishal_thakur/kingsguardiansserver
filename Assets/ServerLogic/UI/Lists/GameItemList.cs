﻿using UnityEngine;
using System.Collections;

public class GameItemList : ListGUI<GameItemSlot>
{
    public PlayerDetailsPanel Panel { get; set; }

    public override void Select(ISlot slot)
    {        
        Panel.SelectItem(slot.Index);
    }
}
