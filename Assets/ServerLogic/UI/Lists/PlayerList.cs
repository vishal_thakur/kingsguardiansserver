﻿using UnityEngine;
using System.Collections;

public class PlayerList : ListGUI<PlayerSlot>
{
    public PlayerListPanel Panel { get; set; }



    public override void Select(ISlot slot)
    {        
        Panel.Parent.SelectedIndex = slot.Index;
        Panel.Parent.ChangePanel("Player");
    }
	
}
