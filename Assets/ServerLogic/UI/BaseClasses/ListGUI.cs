﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class ListGUI<T> : MonoBehaviour, ISlotHandler where T : ISlot
{
    public Text PageLabel;

    protected int page { get; private set; }
    protected int pages { get { return Mathf.CeilToInt(_elements / slots.Count) + 1; } }

    protected List<T> slots;

    int _elements = -1;
    bool _initialized = false;

	void Awake()
	{
        slots = new List<T>();
        slots = GetComponentsInChildren<T>().ToList();
        foreach (var slot  in slots)
            slot.SlotHandler = this;
	}

    void Update()
    {
        if(!_initialized && _elements >= 0)
        {
            SetPage(1);  
            _initialized = true;
        }
    }

    public void Set(int elementCount)
    {		
        _elements = elementCount; 
        SetPage(1);
    }

	public void Refresh(int elementCount)
	{
		if (_elements != elementCount)
		{
			_elements = elementCount; 
			SetPage(page);
		}
	}

    public void SetPage(int p)
    {
        p = Mathf.Clamp(p, 1, pages);
        page = p;
        int sCount = slots.Count;

        for (int i = 0; i < sCount; i++)
            slots[i].SetIndex((p-1) * sCount + i);

        if (PageLabel != null)
            PageLabel.text = page + "/" + pages;
    }

    public void NextPage() { SetPage(page + 1); }
    public void PrevPage() { SetPage(page - 1); }

    public virtual void Select(ISlot slot) 
    {
        Debug.Log("Selected slot's index: " + slot.Index);
    }



}
