﻿
public interface ISlot
{
    ISlotHandler SlotHandler { get; set; }
    int Index { get; }
    void SetIndex(int index);
}
