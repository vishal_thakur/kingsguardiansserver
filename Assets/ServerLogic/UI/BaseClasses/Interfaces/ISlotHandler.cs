﻿using System.Collections;


public interface ISlotHandler
{
    void Select(ISlot slot);
}
