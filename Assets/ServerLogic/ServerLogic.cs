﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class ServerLogic : SingletonMono<ServerLogic>
{
    public static string DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

    public NetworkManager NetworkManager;

    public GameContent GameContent;
    public PlayerDatabase PlayerDatabase;
    public PlayerConnections ConnectedPlayers;

    public Dictionary<string, BattleManager> ActiveBattles = new Dictionary<string, BattleManager>();
    public Dictionary<string, RaidManager> ActiveRaids = new Dictionary<string, RaidManager>();


//	void OnGUI(){
//		for (int i = 0; i < PlayerDatabase._players.Count; i++) {
//			GUILayout.TextField (PlayerDatabase._players[i].GCMFirebaseToken);
//		}
//	}

    void Awake()
    {     
        GameContent = new GameContent();
        ConnectedPlayers = new PlayerConnections();
        PlayerDatabase = new PlayerDatabase();
    }

    public void Run(int port = 4444)
    {
        NetworkManager = new NetworkManager(port);
    }

    void Update()
    {
        if (UpdateDelegates != null)
            UpdateDelegates();
    }

    public void DisconnectPlayer(int connectionID)
    {
        Player p = ConnectedPlayers.GetPlayer(connectionID);
        if (p == null) return;
        PlayerDatabase.SavePlayer(p.Username);
        ConnectedPlayers.PlayerDisconnected(p);

        FinishBattle(p.Username);
        FinishRaid(p.Username);
    }

	void OnApplicationQuit()
	{
		foreach (var connected in ConnectedPlayers.OnlinePlayers)
			DisconnectPlayer(connected.Key.connectionId);

		foreach (var p in PlayerDatabase.GetPlayers())
			PlayerDatabase.SavePlayer(p.Username);
	}

    public void FinishBattle(string username)
    {
        if (ActiveBattles.ContainsKey(username))
        {
            ActiveBattles[username].Destroy();
            ActiveBattles.Remove(username);
        }
    }

    public void FinishRaid(string username)
    {
        if (ActiveRaids.ContainsKey(username))
        {
            ActiveRaids[username].Destroy();
            ActiveRaids.Remove(username);
        }
    }

	public delegate void OnUpdate();
    public OnUpdate UpdateDelegates;
}
