﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PlayerConnections
{
    public Dictionary<NetworkConnection, Player> OnlinePlayers { get{ return _onlinePlayers; } }     
    Dictionary<NetworkConnection, Player> _onlinePlayers = new Dictionary<NetworkConnection, Player>();
    Dictionary<int, NetworkConnection> _connections = new Dictionary<int, NetworkConnection>();

    public void PlayerConnected(NetworkConnection connection, Player player)
    {
        player.ConnectionID = connection.connectionId;
        _onlinePlayers.Add(connection, player);
        _connections.Add(connection.connectionId, connection);
    }

    public void PlayerDisconnected(Player player)
    {
        if (_connections.ContainsKey(player.ConnectionID))
        {
            var con = _connections[player.ConnectionID];
            _onlinePlayers.Remove(con);
            _connections.Remove(player.ConnectionID);
            player.ConnectionID = -1;
        }
    }

    public Player GetPlayer(int connectionID)
    {
        if (_connections.ContainsKey(connectionID))
            return _onlinePlayers[_connections[connectionID]];
        return null;
    }

    public NetworkConnection GetConnectionInfo(int connectionID)
    {
        if (_connections.ContainsKey(connectionID))
            return _connections[connectionID];
        return null;
    }
}
