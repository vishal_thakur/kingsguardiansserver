﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerDatabase : ISaveable
{
    #region ISaveable implementation
    public string FolderName { get { return ""; } }
    public string FileName { get { return "playerlist"; } }
    #endregion

	List<PlayerAccountData> _playerList = new List<PlayerAccountData>();
    List<Player> _players = new List<Player>();

    public PlayerDatabase()
    {
        _players = new List<Player>();

        string path = Persistency.CreatePath(this);
        Debug.Log("Database saving path\n" +path);
		_playerList = Persistency.Load<List<PlayerAccountData>>(path);
        if (_playerList == null)
			_playerList = new List<PlayerAccountData>();

        foreach (PlayerAccountData p in _playerList)
            LoadPlayer(p.Username);
    }

    public List<Player> GetPlayers()
    {
        return _players;
    }


    public Player GetPlayer(string username, string password)
    {
        if (username != "" && password != "")
        {
            var player = GetPlayer(username);
            if (player != null && player.Password == password)
                return player;
        }
        return null;
    }

    public Player GetPlayer(int cnt)
    {
        if (_players.Count > cnt)
            return _players[cnt];
        return null;
    }

    public Player GetPlayer(string username)
    {
        return _players.Find(x => x.Username == username); 
    }

    public Player GetPlayer(string id, LoginMethod method)
    {
        if (id != "")
        {
            Player player = null;

            switch (method)
            {
                case LoginMethod.DeviceID: player = _players.Find(x => x.DeviceID == id); break;
                case LoginMethod.FacebookToken: player = _players.Find(x => x.FacebookToken == id); break;
                case LoginMethod.GoogleToken: player = _players.Find(x => x.GoogleToken == id); break;
                case LoginMethod.Username: player = GetPlayer(id); break;
            }

            if (player != null)
                return player;
        }
        return null;
    }

    public void CreateNewPlayer(Player p)
    {
        if (p == null) return;

        _players.Add(p);

		var account = new PlayerAccountData();
		account.Set(p);
        _playerList.Add(account);

        SaveList();
        SavePlayer(p.Username);
    }

    public void SaveList()
    {
		Persistency.Save<List<PlayerAccountData>>(_playerList, Persistency.CreatePath(this));
    }

    public void SavePlayer(string username)
    {
        var player = GetPlayer(username);
		var account = _playerList.Find( x => x.Username == username);
		account.Set(player);
        Persistency.Save<PlayerData>(player.Save(), Persistency.CreatePath(username, "Player"));
    }

    public void LoadPlayer(string username)
    {
        if (_players.Count > 0 && GetPlayer(username) != null)  return;

        var player = new Player();
		PlayerData p = Persistency.Load<PlayerData>(Persistency.CreatePath(username, "Player"));
		if(p == null)
		{
			var account = _playerList.Find( x => x.Username == username);
			p = new PlayerData();
			p.Username = account.Username;
			p.Avatar = account.Avatar;
			p.Password = account.Password;
			p.DeviceID = account.DeviceID;
			p.FacebookToken = account.FacebookToken;
			p.GoogleToken = account.GoogleToken;
		}	

		player.Load(p);
        _players.Add(player);
    }
}

[System.Serializable]
public class PlayerAccountData
{
	public string Username;
	public string Avatar;
	public string Password;
	public string DeviceID;
	public string FacebookToken;
	public string GoogleToken;

	public void Set(Player p)
	{
		Username = p.Username;
		Avatar = p.Avatar;
		Password = p.Password;
		DeviceID = p.DeviceID;
		FacebookToken = p.FacebookToken;
		GoogleToken = p.GoogleToken;
	}
}
