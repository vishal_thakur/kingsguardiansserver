﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

public class FirebaseNotificationManager : SingletonMono<FirebaseNotificationManager>{

	[Multiline(20)]
	public string rawBodyData;

    string rawBodyDataOrignal;


	List<Player> AllPlayers = new List<Player>();

    private void Start()
    {
        rawBodyDataOrignal = rawBodyData;
    }



    //void OnGUI()
    //{
    //    if (GUILayout.Button("test"))
    //    {
    //        Player newplayer = null;
    //        SendPushNotification(newplayer, "Title Mera Lun", "Salo Gand marao !");
    //    }
    //}


	//Send Push Notification To the specified Player About the Upgrade that just completed !
	public void SendPushNotification(Player player ,  string Title , string Message){
		//Donot Send Push Notif if it is disabled for the respective Player
		if (!player.IsPushNotificationsEnabled && player.IsConnected)
			return;

		//Replace Title
		rawBodyData = rawBodyData.Replace ("thetitle" , Title);
		//Replace Body
		rawBodyData = rawBodyData.Replace ("thebody" , Message);
		//Replace Player GCM Token
		rawBodyData = rawBodyData.Replace ("thereciever" , player.GCMFirebaseToken);

		//Do the POST
		StartCoroutine(POSTNotification (rawBodyData));
	}

//	//Send Push Notification To the specified Player About the Upgrade that just completed !
//	public void SendPushNotification(string playerByUsername,  string Title , string Message){
//		//Grab All Players
//		AllPlayers = ServerLogic.Instance.PlayerDatabase.GetPlayers();
//		Player tempPlayer = null;
//
//		//Find The Specified Player
//		for(int i=0;i<AllPlayers.Count;i++){
//			if (AllPlayers [i].Username == playerByUsername)
//				tempPlayer = AllPlayers [i];
//		}
//
//		//Donot Send Push Notif if it is disabled for the respective Player
//		if (!tempPlayer.IsPushNotificationsEnabled)
//			return;
//
//		//Replace Title
//		rawBodyData = rawBodyData.Replace ("thetitle" , Title);
//		//Replace Body
//		rawBodyData = rawBodyData.Replace ("thebody" , Message);
//		//Replace Player GCM Token
//		rawBodyData = rawBodyData.Replace ("thereciever" , tempPlayer.GCMFirebaseToken);
//
//		//Do the POST
//		StartCoroutine(POSTNotification (rawBodyData));
//	}

	IEnumerator POSTNotification(string jsonStr)
	{
		Hashtable postHeader = new Hashtable();
		postHeader.Add("Content-Type", "application/json");

		// convert json string to byte
		//var formData = System.Text.Encoding.UTF8.GetBytes(jsonStr);


        //WWW www = new WWW("Url", formData, postHeader);
        //StartCoroutine(WaitForRequest(www));
        //return www;


        UnityWebRequest www = new UnityWebRequest("https://gcm-http.googleapis.com/gcm/send", "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonStr);
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("Content-Type", "application/json");

        //Set Authorization Header : Default For now
        www.SetRequestHeader("Authorization", "key=AIzaSyACaMM4CrEjxc8mkDfA4rbVy8d45xSmpOU");

        //DO API CAll thingy
        yield return www.Send();

        //Restore Raw Body Data to be able to send Pushnotifications to others
        rawBodyData = rawBodyDataOrignal;

        if (www.isError)
        {
            //oops
            Debug.Log("Error In REST API CAll" + www.error);
        }
        else
        {
            //Do Stuff now that we have response text 
            //OnApiCallSuccess(www.downloadHandler.text, restApiCall);
            Debug.Log(www.downloadHandler.text);
        }
    }
}
