﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WorkerData : BaseData
{
    public WorkerType Type;
    public QueueData BuildingWorkingOn;
}
