﻿using System.Collections.Generic;

[System.Serializable]
public class PlayerData 
{
    public string GridTileSize = "";

    public string Username;
    public string Password;

    public string DeviceID;
    public string FacebookToken;
    public string GoogleToken;
	public string GCMFirebaseToken;

	public bool IsBanned;

	public bool IsPushNotificationsEnabled;

    public string Avatar = "";
    public int Exp = 0;
    public int Medals = 0;

    /// <summary>
    /// The level is only used between server and client. 
    /// It's not saved, since the server calculates it from exp.
    /// </summary>
    public int Level = 1; 

    public List<ItemData> Inventory = new List<ItemData>();
    public List<BuildingData> Buildings = new List<BuildingData>();
    public List<SMaterial> Materials = new List<SMaterial>();
    public List<CharacterData> Characters = new List<CharacterData>();
    public List<WorkerData> Workers = new List<WorkerData>();

	public List<Warlock> Warlocks = new List<Warlock>();

    public List<string> Squad = new List<string>();

	public List<RemainsInfo> Remains = new List<RemainsInfo>();
}