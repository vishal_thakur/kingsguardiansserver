﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CharacterData : BaseData
{
	public string PlayerOwner;
    public int Exp = 0;
    public int HP = 0;
    public ItemData Weapon;
    public List<ItemData> Defenses = new List<ItemData>();
    public List<ItemData> Inventory = new List<ItemData>();
    public List<StatUpgradeData> StatUpgrades = new List<StatUpgradeData>();

}

[System.Serializable]
public class StatUpgradeData
{
    public int StatEnum;
    public List<StatModData> Upgrades = new List<StatModData>();
}

[System.Serializable]
public class StatModData
{
    public string ID;
    public int Value;
}