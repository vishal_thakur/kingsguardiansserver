﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ItemData : BaseData
{
    public ItemCategory Category;
    public int Stacks = 1;
	
}
