﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class BuildingData : BaseData
{
    public bool Active = true;
    public string Position = "0,0";
    public string Rotation = "";
	public string Size = ""; // REMOVE this (if removed: error with saved data!)
    public int Upgrade = 0;
    public int HP = 0;	

    public int StoredMaterials = 0;
    public List<QueueData> Queue = new List<QueueData>();
    public string LastGeneratorTick = "1990/01/01 00:00:01";
}

[System.Serializable]
public class QueueData
{
    public string StartTime = "19900101000001";
    public float Duration = 0;
    public string ItemContentID = "";
    public List<SMaterial> Costs = new List<SMaterial>();
    public int EnumData;
    public int Amount;
}