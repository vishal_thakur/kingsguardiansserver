﻿public interface ISaveable
{
    string FolderName { get; }
    string FileName { get; }   
}
