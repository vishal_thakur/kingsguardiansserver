﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Persistency
{

    public static void Save<T>(ISaveable data) where T : class
    {
        Save<T>(data, CreatePath(data));
    }

    public static void Save<T>(object data, string path) where T : class
    {
		BinaryFormatter bf = new BinaryFormatter();
        FileStream file = (File.Exists(path)) ? File.Create(path) : File.Open(path, FileMode.OpenOrCreate);        
        bf.Serialize(file, data as T);
        file.Close();
    }
         

    public static T Load<T>(string path) where T : class
    {        
        if(File.Exists(path))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(path, FileMode.Open);

            T data = (T)bf.Deserialize(file);
            file.Close();
            return data;
        }

        return default(T);
    }


    public static string CreatePath(ISaveable data)
    {
        return CreatePath(data.FileName, data.FolderName);
    }

    public static string CreatePath(string filename, string foldername = "")
    {
        string p = Application.persistentDataPath;
        if( foldername != "" ) p += "/"+foldername;

        if (!Directory.Exists(p))
            Directory.CreateDirectory(p);
        
        p += "/"+filename+".txt";
        return p;
    }


}
