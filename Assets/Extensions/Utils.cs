﻿using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

public class Utils 
{
    public static bool IsInherited(System.Type type, System.Type from)
    {
        return type.IsSubclassOf(from) || type == from;
    }

    public static Quaternion StringToRotation(string s)
    {
        string[] rot = s.Split(',');
        return new Quaternion( float.Parse(rot[0]), float.Parse(rot[1]),float.Parse(rot[2]),float.Parse(rot[3]));
    }


    public static int Add(out string log, params int[] list)
    {
        log = "";
        int sum = 0;
        foreach (int i in list)
        {
            if (log != "")
                log += " + ";
            log += i.ToString();
            sum += i;
        }

        log += " = " + sum;
        return sum;
    }


    public static string LocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }

    public static List<SMaterial> CraftCost(GameItemContent gItem)
    {
        if (gItem.GetType() == typeof(BuildingContent))
            return ((BuildingContent)gItem).GetUpgrade(0).Cost;
        return gItem.Price;
    }
}
